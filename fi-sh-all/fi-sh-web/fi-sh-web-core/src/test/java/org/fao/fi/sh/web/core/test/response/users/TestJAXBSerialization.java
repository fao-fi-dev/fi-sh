/**
 * (c) 2015 FAO / UN (project: fi-sh-model-core)
 */
package org.fao.fi.sh.web.core.test.response.users;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.MarshalException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.composite.CoreUserModel;
import org.fao.fi.sh.web.core.response.users.LoginResponse;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2015
 */
public class TestJAXBSerialization {
	@SuppressWarnings("serial")
	@XmlRootElement(name="DummyUserModel")
	static private class DummyUserModel extends CoreUserModel {
		@XmlElement(name="id")
		private String id;
		
		@SuppressWarnings("unused")
		public DummyUserModel() { }
		public DummyUserModel(String id) { super(); this.setId(id); };
		
		@Override
		public String getId() {
			return this.id; // TODO Auto-generated method stub
		}
		
		/* (non-Javadoc)
		 * @see org.fao.fi.sh.model.core.spi.Identified#setId(java.lang.Object)
		 */
		@Override
		public void setId(String id) {
			this.id = id;
		}

		@Override
		public boolean can(String... capabilities) {
			return true; // TODO Auto-generated method stub
		}

		@Override
		public boolean is(String... roles) {
			return true; // TODO Auto-generated method stub
		}

		@Override
		public String[] listCapabilities() {
			return new String[] { "FOO_CAPABILITY" }; // TODO Auto-generated method stub
		}

		@Override
		public String[] listRoles() {
			return new String[] { "FOO_ROLE" }; // TODO Auto-generated method stub
		}
	}
	
	@SuppressWarnings("serial")
	@XmlRootElement(name="DummyLoginResponse")
	static private class DummyLoginResponse extends LoginResponse<DummyUserModel> {	
		@SuppressWarnings("unused")
		public DummyLoginResponse() {
			super(); // TODO Auto-generated constructor block
		}

		public DummyLoginResponse(String token, DummyUserModel user, String message) {
			super(token, user, message);
		}
	}
	
	@Test public void testXMLSerialization() throws Throwable {
		DummyLoginResponse mock = new DummyLoginResponse("abcdef", new DummyUserModel("FOO"), "Foo message");
		
		JAXBContext.newInstance(DummyLoginResponse.class).createMarshaller().marshal(mock, System.out);
	}
	
	@Test public void testXMLDeSerialization() throws Throwable {
		DummyLoginResponse mock = new DummyLoginResponse("abcdef", new DummyUserModel("FOO"), "Foo message");
		
		JAXBContext ctx = JAXBContext.newInstance(DummyLoginResponse.class, DummyUserModel.class);
		
		StringWriter sw = new StringWriter();
		
		ctx.createMarshaller().marshal(mock, sw);

		StringReader sr = new StringReader(sw.toString());

		Assert.assertEquals(DummyLoginResponse.class, ctx.createUnmarshaller().unmarshal(sr).getClass());
	}
	
	@Test(expected=MarshalException.class)
	public void testFailingGenericXMLSerialization() throws Throwable {
		LoginResponse<DummyUserModel> mock = new LoginResponse<DummyUserModel>("abcdef", new DummyUserModel("FOO"), "Foo message");
		
		JAXBContext.newInstance(LoginResponse.class, DummyUserModel.class).createMarshaller().marshal(mock, System.out);
	}
}
