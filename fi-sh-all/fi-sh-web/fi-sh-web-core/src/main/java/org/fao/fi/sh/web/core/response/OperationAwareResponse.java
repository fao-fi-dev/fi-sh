/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
@XmlRootElement(name="operationResponse")
@ApiModel
public class OperationAwareResponse extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3771024676471159908L;
	
	@XmlAttribute(name="operation")
	@ApiModelProperty(value = "The operation that yield this response")
	protected String operation;
	
	/**
	 * Class constructor
	 *
	 */
	public OperationAwareResponse() {
		this.operation = "not set";
	}
	
	/**
	 * Class constructor
	 *
	 * @param operation
	 */
	public OperationAwareResponse(String operation) {
		super();
		this.operation = operation;
	}

	/**
	 * Class constructor
	 *
	 * @param operation
	 * @param message
	 */
	public OperationAwareResponse(String operation, String message) {
		super(message);
		this.operation = operation;
	}

	/**
	 * @return the 'operation' value
	 */
	public String getOperation() {
		return this.operation;
	}

	/**
	 * @param operation the 'operation' value to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	static public OperationAwareResponse forOperation(String operation) {
		return new OperationAwareResponse(operation);
	}

	@SuppressWarnings("unchecked")
	public <SELF extends OperationAwareResponse> SELF withOperation(String operation) {
		this.operation = operation;
		
		return (SELF)this;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.operation == null ) ? 0 : this.operation.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		OperationAwareResponse other = (OperationAwareResponse) obj;
		if(this.operation == null) {
			if(other.operation != null) return false;
		} else if(!this.operation.equals(other.operation)) return false;
		return true;
	}
}
