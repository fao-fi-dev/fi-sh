/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
@XmlRootElement(name="response")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel
public class BasicResponse implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3771024676471159908L;
		
	@XmlElement(name="message")
	@ApiModelProperty(value = "The message associated to this response")
	protected String message;
		
	/**
	 * Class constructor
	 *
	 */
	public BasicResponse() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public BasicResponse(String message) {
		super();
		this.message = message;
	}

	/**
	 * @return the 'message' value
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * @param message the 'message' value to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	@SuppressWarnings("unchecked")
	public <SELF extends BasicResponse> SELF withMessage(String message) {
		this.message = message;
		
		return (SELF)this;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.message == null ) ? 0 : this.message.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		BasicResponse other = (BasicResponse) obj;
		if(this.message == null) {
			if(other.message != null) return false;
		} else if(!this.message.equals(other.message)) return false;
		return true;
	}
}
