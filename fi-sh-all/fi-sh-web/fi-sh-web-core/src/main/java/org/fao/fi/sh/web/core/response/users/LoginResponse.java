/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.users;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreUserModel;
import org.fao.fi.sh.web.core.response.BasicResponse;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
@XmlType(name="loginResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel public class LoginResponse<USER_MODEL extends CoreUserModel> extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5548214063829009076L;

	@XmlElement(name="token")
	@ApiModelProperty(value = "The authenticated session token", required=true)
	private String token;
	
	@XmlElement(name="user")
	@ApiModelProperty(value = "The authenticated user model", required=true)
	private USER_MODEL user;
	
	/**
	 * Class constructor
	 *
	 */
	public LoginResponse() {
		super("login");
	}

	/**
	 * Class constructor
	 *
	 * @param token
	 * @param user
	 * @param message
	 */
	public LoginResponse(String token, USER_MODEL user, String message) {
		this();
		this.token = token;
		this.user = user;
		this.message = message;
	}

	/**
	 * @return the 'token' value
	 */
	public String getToken() {
		return this.token;
	}

	/**
	 * @param token the 'token' value to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the 'user' value
	 */
	public USER_MODEL getUser() {
		return this.user;
	}

	/**
	 * @param user the 'user' value to set
	 */
	public void setUser(USER_MODEL user) {
		this.user = user;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.token == null ) ? 0 : this.token.hashCode() );
		result = prime * result + ( ( this.user == null ) ? 0 : this.user.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		LoginResponse other = (LoginResponse) obj;
		if(this.token == null) {
			if(other.token != null) return false;
		} else if(!this.token.equals(other.token)) return false;
		if(this.user == null) {
			if(other.user != null) return false;
		} else if(!this.user.equals(other.user)) return false;
		return true;
	}
}
