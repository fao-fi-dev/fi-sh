/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.core.response.codelists;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
@XmlRootElement(name="retrieveCodelistResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(description="Container for multiple codelist data entries")
public class RetrieveCodelistResponse<CODELIST extends CoreCodelist<?, ?>> {
	@XmlAttribute(name="codelist")
	@ApiModelProperty(value="codelist", notes="The codelist name", required=true)
	private String codelist;
	
	@XmlElementWrapper(name="entries")
	@XmlElement(name="entry")
	@ApiModelProperty(value="entries", notes="The list of entries for the given codelist", required=true)
	private List<CODELIST> entries;
	
	/**
	 * Class constructor
	 *
	 */
	public RetrieveCodelistResponse() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param codelist
	 * @param entries
	 */
	public RetrieveCodelistResponse(String codelist, List<CODELIST> entries) {
		super();
		this.codelist = codelist;
		this.entries = entries;
	}
	
	/**
	 * @return the 'codelist' value
	 */
	public String getCodelist() {
		return this.codelist;
	}

	/**
	 * @param codelist the 'codelist' value to set
	 */
	public void setCodelist(String codelist) {
		this.codelist = codelist;
	}

	/**
	 * @return the 'entries' value
	 */
	public List<CODELIST> getEntries() {
		return this.entries;
	}
	
	/**
	 * @param entries the 'entries' value to set
	 */
	public void setEntries(List<CODELIST> entries) {
		this.entries = entries;
	}
	
	@ApiModelProperty(hidden=true)
	public boolean isEmpty() {
		return entries == null || entries.isEmpty();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.codelist == null ) ? 0 : this.codelist.hashCode() );
		result = prime * result + ( ( this.entries == null ) ? 0 : this.entries.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		RetrieveCodelistResponse other = (RetrieveCodelistResponse) obj;
		if(this.codelist == null) {
			if(other.codelist != null) return false;
		} else if(!this.codelist.equals(other.codelist)) return false;
		if(this.entries == null) {
			if(other.entries != null) return false;
		} else if(!this.entries.equals(other.entries)) return false;
		return true;
	}
}