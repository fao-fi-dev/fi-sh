/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.codelists.mapping;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.data.mappings.StringPropertyModelMapping;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlRootElement(name="getMappingsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetMappingsResponse<MAPPING extends StringPropertyModelMapping<Integer, ?>> {
	@XmlElementWrapper(name="mappings")
	@XmlElement(name="mapping")
	protected List<MAPPING> _mappings;
	
	/**
	 * Class constructor
	 */
	public GetMappingsResponse() {
	}

	/**
	 * Class constructor
	 *
	 * @param mappings
	 */
	public GetMappingsResponse(List<MAPPING> mappings) {
		super();
		this._mappings = mappings;
	}

	/**
	 * @return the 'mappings' value
	 */
	public List<MAPPING> getMappings() {
		return this._mappings;
	}

	/**
	 * @param mappings the 'mappings' value to set
	 */
	public void setMappings(List<MAPPING> mappings) {
		this._mappings = mappings;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._mappings == null ) ? 0 : this._mappings.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		GetMappingsResponse other = (GetMappingsResponse) obj;
		if(this._mappings == null) {
			if(other._mappings != null) return false;
		} else if(!this._mappings.equals(other._mappings)) return false;
		return true;
	}
}
