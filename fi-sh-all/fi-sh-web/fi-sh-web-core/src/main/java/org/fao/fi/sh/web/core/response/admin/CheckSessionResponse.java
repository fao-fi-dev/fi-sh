/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.admin;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.web.core.response.BasicResponse;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
@XmlRootElement(name="checkSessionResponse")
@ApiModel
public class CheckSessionResponse extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1360407770872107443L;

	@XmlAttribute(name="token")
	@ApiModelProperty(value = "The authenticated session token", required=true)
	private String token;
	
	@XmlAttribute(name="username")
	@ApiModelProperty(value = "The session owner user name", required=true)
	private String username;
	
	@XmlAttribute(name="isValid")
	@ApiModelProperty(value = "Whether the session is still valid or not", required=true)
	private boolean isValid;
	
	@XmlElement(name="loginIP")
	@ApiModelProperty(value = "The IP address originating the successful login for the session", required=true)
	private String loginIP;
	
	@XmlElement(name="created")
	@ApiModelProperty(value = "The login date for the session", required=true)
	private Date created;
	
	@XmlElement(name="lastAccessed")
	@ApiModelProperty(value = "The last access date for the session", required=true)
	private Date lastAccessed;
	
	@XmlElement(name="expiresOn")
	@ApiModelProperty(value = "The current expiration date for the session", required=true)
	private Date expiresOn;
	
	/**
	 * Class constructor
	 *
	 */
	public CheckSessionResponse() {
		super("No check performed");
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public CheckSessionResponse(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 *
	 * @param token
	 * @param username
	 * @param isValid
	 * @param lastAccessed
	 * @param expiresOn
	 */
	public CheckSessionResponse(String token, String username, boolean isValid, String loginIP, Date created, Date lastAccessed, Date expiresOn) {
		this("Check performed: user session is currently" + ( isValid ? "" : " not" ) + " valid"  );
		this.token = token;
		this.username = username;
		this.isValid = isValid;
		this.loginIP = loginIP;
		this.created = created;
		this.lastAccessed = lastAccessed;
		this.expiresOn = expiresOn;
	}

	/**
	 * @return the 'token' value
	 */
	public String getToken() {
		return this.token;
	}

	/**
	 * @param token the 'token' value to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the 'username' value
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username the 'username' value to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the 'isValid' value
	 */
	public boolean getIsValid() {
		return this.isValid;
	}

	/**
	 * @param isValid the 'isValid' value to set
	 */
	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	/**
	 * @return the 'loginIP' value
	 */
	public String getLoginIP() {
		return this.loginIP;
	}

	/**
	 * @param loginIP the 'loginIP' value to set
	 */
	public void setLoginIP(String loginIP) {
		this.loginIP = loginIP;
	}

	/**
	 * @return the 'created' value
	 */
	public Date getCreated() {
		return this.created;
	}

	/**
	 * @param created the 'created' value to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the 'lastAccessed' value
	 */
	public Date getLastAccessed() {
		return this.lastAccessed;
	}

	/**
	 * @param lastAccessed the 'lastAccessed' value to set
	 */
	public void setLastAccessed(Date lastAccessed) {
		this.lastAccessed = lastAccessed;
	}

	/**
	 * @return the 'expiresOn' value
	 */
	public Date getExpiresOn() {
		return this.expiresOn;
	}

	/**
	 * @param expiresOn the 'expiresOn' value to set
	 */
	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.loginIP == null ) ? 0 : this.loginIP.hashCode() );
		result = prime * result + ( ( this.created == null ) ? 0 : this.created.hashCode() );
		result = prime * result + ( ( this.expiresOn == null ) ? 0 : this.expiresOn.hashCode() );
		result = prime * result + ( this.isValid ? 1231 : 1237 );
		result = prime * result + ( ( this.lastAccessed == null ) ? 0 : this.lastAccessed.hashCode() );
		result = prime * result + ( ( this.token == null ) ? 0 : this.token.hashCode() );
		result = prime * result + ( ( this.username == null ) ? 0 : this.username.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		CheckSessionResponse other = (CheckSessionResponse) obj;
		if(this.loginIP == null) {
			if(other.loginIP != null) return false;
		} else if(!this.loginIP.equals(other.loginIP)) return false;
		if(this.created == null) {
			if(other.created != null) return false;
		} else if(!this.created.equals(other.created)) return false;
		if(this.expiresOn == null) {
			if(other.expiresOn != null) return false;
		} else if(!this.expiresOn.equals(other.expiresOn)) return false;
		if(this.isValid != other.isValid) return false;
		if(this.lastAccessed == null) {
			if(other.lastAccessed != null) return false;
		} else if(!this.lastAccessed.equals(other.lastAccessed)) return false;
		if(this.token == null) {
			if(other.token != null) return false;
		} else if(!this.token.equals(other.token)) return false;
		if(this.username == null) {
			if(other.username != null) return false;
		} else if(!this.username.equals(other.username)) return false;
		return true;
	}
}
