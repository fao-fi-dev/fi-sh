/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.request.permits;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 May 2015
 */
@XmlType(name="updatePermitRequest")
@ApiModel(value="The generic permit update container")
public class UpdatePermitRequest extends IssuePermitRequest {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7873363383485310389L;

	@XmlElement(name="permitNumber", required=true, nillable=false)
	@ApiModelProperty(position=10, value="The permit number", required=true)
	private String permitNumber;

	/**
	 * Class constructor
	 *
	 */
	public UpdatePermitRequest() {
		// TODO Auto-generated constructor block
	}

	/**
	 * @return the 'permitNumber' value
	 */
	public String getPermitNumber() {
		return this.permitNumber;
	}

	/**
	 * @param permitNumber the 'permitNumber' value to set
	 */
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.permitNumber == null ) ? 0 : this.permitNumber.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		UpdatePermitRequest other = (UpdatePermitRequest) obj;
		if(this.permitNumber == null) {
			if(other.permitNumber != null) return false;
		} else if(!this.permitNumber.equals(other.permitNumber)) return false;
		return true;
	}
}