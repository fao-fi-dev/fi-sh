/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.request.codelists;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Apr 2015
 */
@XmlType(name="updateEntryRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The object model for the 'Update Codelist' request")
public class UpdateEntryRequest<CODELIST extends CoreCodelist<?, ?>> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8081438661479787796L;

	@XmlAttribute(name="selective")
	@ApiModelProperty(position=1, value = "Tells whether the requested update must be selective (defaults to 'false', i.e. 'non-selective')", 
					  notes = "A request for a 'selective' update will not update attributes that are set to NULL. Conversely, a request for a 'non-selective' update will update the provided data exactly as they are, NULL attributes included)", 
					  required=true)
	private boolean _selective;
	
	@XmlElement(name="data")
	@ApiModelProperty(position=2, value = "The data subject of the update request. It should always have its 'id' value set to a non-null value", 
					  notes = "The actual data structure depends on the codelist the 'update' request is issued for.", 
					  required=true)
	private CODELIST _data;
	
	/**
	 * Class constructor
	 *
	 */
	public UpdateEntryRequest() {
	}

	/**
	 * Class constructor
	 *
	 * @param selective
	 * @param data
	 */
	public UpdateEntryRequest(boolean selective, CODELIST data) {
		super();
		this._selective = selective;
		this._data = data;
	}

	/**
	 * @return the 'selective' value
	 */
	public boolean isSelective() {
		return this._selective;
	}

	/**
	 * @param selective the 'selective' value to set
	 */
	public void setSelective(boolean selective) {
		this._selective = selective;
	}

	/**
	 * @return the 'data' value
	 */
	public CODELIST getData() {
		return this._data;
	}

	/**
	 * @param data the 'data' value to set
	 */
	public void setData(CODELIST data) {
		this._data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._data == null ) ? 0 : this._data.hashCode() );
		result = prime * result + ( this._selective ? 1231 : 1237 );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		UpdateEntryRequest other = (UpdateEntryRequest) obj;
		if(this._data == null) {
			if(other._data != null) return false;
		} else if(!this._data.equals(other._data)) return false;
		if(this._selective != other._selective) return false;
		return true;
	}
}
