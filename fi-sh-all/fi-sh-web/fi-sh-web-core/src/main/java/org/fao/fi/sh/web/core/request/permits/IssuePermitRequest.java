/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.request.permits;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreUpdatableData;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 May 2015
 */
@XmlType(name="issuePermitRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The generic permit request container")
public class IssuePermitRequest extends CoreUpdatableData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3054410722537760736L;
	
	@XmlElement(name="dateFrom", required=true, nillable=false)
	@ApiModelProperty(position=2, value="The permit validity start", required=true)
	private Date dateFrom;
	
	@XmlElement(name="dateTo", required=false, nillable=true)
	@ApiModelProperty(position=3, value="The permit validity end", required=false)
	private Date dateTo;
		
	@XmlElement(name="specialConditions", required=false, nillable=true)
	@ApiModelProperty(position=4, value="The permit special conditions", required=false)
	private String specialConditions;
	
	/**
	 * Class constructor
	 *
	 */
	public IssuePermitRequest() {
	}

	/**
	 * @return the 'dateFrom' value
	 */
	public Date getDateFrom() {
		return this.dateFrom;
	}

	/**
	 * @param dateFrom the 'dateFrom' value to set
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the 'dateTo' value
	 */
	public Date getDateTo() {
		return this.dateTo;
	}

	/**
	 * @param dateTo the 'dateTo' value to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the 'specialConditions' value
	 */
	public String getSpecialConditions() {
		return this.specialConditions;
	}

	/**
	 * @param specialConditions the 'specialConditions' value to set
	 */
	public void setSpecialConditions(String specialConditions) {
		this.specialConditions = specialConditions;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.dateFrom == null ) ? 0 : this.dateFrom.hashCode() );
		result = prime * result + ( ( this.dateTo == null ) ? 0 : this.dateTo.hashCode() );
		result = prime * result + ( ( this.specialConditions == null ) ? 0 : this.specialConditions.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		IssuePermitRequest other = (IssuePermitRequest) obj;
		if(this.dateFrom == null) {
			if(other.dateFrom != null) return false;
		} else if(!this.dateFrom.equals(other.dateFrom)) return false;
		if(this.dateTo == null) {
			if(other.dateTo != null) return false;
		} else if(!this.dateTo.equals(other.dateTo)) return false;
		if(this.specialConditions == null) {
			if(other.specialConditions != null) return false;
		} else if(!this.specialConditions.equals(other.specialConditions)) return false;
		return true;
	}
}