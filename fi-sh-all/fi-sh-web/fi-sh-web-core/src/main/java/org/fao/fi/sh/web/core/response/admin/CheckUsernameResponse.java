/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.admin;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.web.core.response.BasicResponse;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
@XmlRootElement(name="checkUsernameResponse")
@ApiModel
public class CheckUsernameResponse extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1360407770872107443L;

	@XmlAttribute(name="username")
	@ApiModelProperty(value = "The checked user name", required=true)
	private String username;
	
	@XmlAttribute(name="exists")
	@ApiModelProperty(value = "The check result (whether a user with the provided ID exists or not)", required=true)
	private boolean exists;
	
	/**
	 * Class constructor
	 *
	 */
	public CheckUsernameResponse() {
		super("No check performed");
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public CheckUsernameResponse(String message) {
		super(message); 
	}
	
	/**
	 * Class constructor
	 *
	 * @param username
	 * @param exists
	 */
	public CheckUsernameResponse(String username, boolean exists) {
		this("Check performed: provided username does" + ( exists ? "" : " not" ) + " exist");
		this.username = username;
		this.exists = exists;
	}
	
	

	/**
	 * Class constructor
	 *
	 * @param username
	 * @param exists
	 * @param message
	 */
	public CheckUsernameResponse(String username, boolean exists, String message) {
		this();
		this.message = message;
		this.username = username;
		this.exists = exists;
	}
	
	/**
	 * @return the 'username' value
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * @param username the 'username' value to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the 'exists' value
	 */
	public boolean getExists() {
		return this.exists;
	}

	/**
	 * @param exists the 'exists' value to set
	 */
	public void setExists(boolean exists) {
		this.exists = exists;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( this.exists ? 1231 : 1237 );
		result = prime * result + ( ( this.username == null ) ? 0 : this.username.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		CheckUsernameResponse other = (CheckUsernameResponse) obj;
		if(this.exists != other.exists) return false;
		if(this.username == null) {
			if(other.username != null) return false;
		} else if(!this.username.equals(other.username)) return false;
		return true;
	}
}
