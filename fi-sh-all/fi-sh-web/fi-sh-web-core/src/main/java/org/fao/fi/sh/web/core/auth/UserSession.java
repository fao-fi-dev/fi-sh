/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.auth;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreUserModel;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 30 mar 2015
 */
@XmlType(name="userSession")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel public class UserSession<USER_MODEL extends CoreUserModel> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -625601494875783254L;

	@XmlAttribute(name="token")
	@ApiModelProperty(value = "The session token", required=true)
	private String token;
	
	@XmlAttribute(name="creationDate")
	@ApiModelProperty(value = "The session creation date", required=true)
	private Date creationDate;

	@XmlAttribute(name="lastActivityDate")
	@ApiModelProperty(value = "The session last activity date", required=true)
	private Date lastActivityDate;
	
	@XmlAttribute(name="loginIP")
	@ApiModelProperty(value = "The IP originating the login that yield this user session", required=true)
	private String loginIP;
	
	@XmlElement(name="user", required=true, nillable=false)
	@ApiModelProperty(value = "The user owning the session", required=true)
	private USER_MODEL user;

	/**
	 * Class constructor
	 *
	 */
	public UserSession() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param token
	 * @param creationDate
	 * @param lastActivityDate
	 * @param loginIP
	 * @param user
	 */
	public UserSession(String token, Date creationDate, Date lastActivityDate, String loginIP, USER_MODEL user) {
		super();
		this.token = token;
		this.creationDate = creationDate;
		this.lastActivityDate = lastActivityDate;
		this.loginIP = loginIP;
		this.user = user;
	}
	
	/**
	 * Class constructor
	 *
	 * @param token
	 * @param loginIP
	 * @param user
	 */
	public UserSession(String token, String loginIP, USER_MODEL user) {
		this(token, new Date(), new Date(), loginIP, user);
	}

	/**
	 * @return the 'token' value
	 */
	public String getToken() {
		return this.token;
	}

	/**
	 * @return the 'creationDate' value
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * @return the 'lastActivityDate' value
	 */
	public Date getLastActivityDate() {
		return this.lastActivityDate;
	}

	/**
	 * @param lastActivityDate the 'lastActivityDate' value to set
	 */
	public void setLastActivityDate(Date lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	
	public UserSession<USER_MODEL> access() {
		lastActivityDate = new Date();
		
		return this;
	}

	/**
	 * @return the 'loginIP' value
	 */
	public String getLoginIP() {
		return this.loginIP;
	}

	/**
	 * @return the 'user' value
	 */
	public USER_MODEL getUser() {
		return this.user;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.creationDate == null ) ? 0 : this.creationDate.hashCode() );
		result = prime * result + ( ( this.lastActivityDate == null ) ? 0 : this.lastActivityDate.hashCode() );
		result = prime * result + ( ( this.loginIP == null ) ? 0 : this.loginIP.hashCode() );
		result = prime * result + ( ( this.token == null ) ? 0 : this.token.hashCode() );
		result = prime * result + ( ( this.user == null ) ? 0 : this.user.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		UserSession other = (UserSession) obj;
		if(this.creationDate == null) {
			if(other.creationDate != null) return false;
		} else if(!this.creationDate.equals(other.creationDate)) return false;
		if(this.lastActivityDate == null) {
			if(other.lastActivityDate != null) return false;
		} else if(!this.lastActivityDate.equals(other.lastActivityDate)) return false;
		if(this.loginIP == null) {
			if(other.loginIP != null) return false;
		} else if(!this.loginIP.equals(other.loginIP)) return false;
		if(this.token == null) {
			if(other.token != null) return false;
		} else if(!this.token.equals(other.token)) return false;
		if(this.user == null) {
			if(other.user != null) return false;
		} else if(!this.user.equals(other.user)) return false;
		return true;
	}
}
