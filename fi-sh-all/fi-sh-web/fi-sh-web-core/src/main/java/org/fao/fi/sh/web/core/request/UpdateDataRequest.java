/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.request;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Apr 2015
 */
public interface UpdateDataRequest {
	/**
	 * @return whether the requested update should be 'selective' (i.e. should not change any attribute that is currently 'NULL') or not.
	 */
	boolean getSelectiveUpdate();
	
	/**
	 * @param selectiveUpdate the 'selectiveUpdate' value to set
	 */
	void setSelectiveUpdate(boolean isSelectiveUpdate);
}
