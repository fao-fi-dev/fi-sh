/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.exceptions;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
@XmlRootElement(name="Exception")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExceptionEntity implements Serializable {
	private static final long serialVersionUID = 1616843258589155462L;
	
	@XmlElement(name="OriginalType")
	private String _originalType;
	
	@XmlElement(name="Message")
	private String _message;
	
	@XmlElement(name="Trace")
	private String _trace;
	
	/**
	 * Class constructor
	 *
	 */
	public ExceptionEntity() {
	}
	
	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public ExceptionEntity(String message) {
		this(null, message, null);
	}

	/**
	 * Class constructor
	 *
	 * @param originalType
	 * @param message
	 */
	public ExceptionEntity(String originalType, String message) {
		super();
		this._originalType = originalType;
		this._message = message;
	}

	/**
	 * Class constructor
	 *
	 * @param originalType
	 * @param message
	 * @param trace
	 */
	public ExceptionEntity(String originalType, String message, String trace) {
		super();
		this._originalType = originalType;
		this._message = message;
	}
	
	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public ExceptionEntity(Throwable cause) {
		this(cause.getClass().getName(), cause.getMessage());
		
		StringWriter sw = new StringWriter();
		
		try(PrintWriter pw = new PrintWriter(sw)) {
			cause.printStackTrace(pw);
			
			this._trace = sw.toString();
		}
		
		try {
			sw.close();
		} catch(Throwable t) {
			//Suffocate
		}
	}
	
	/**
	 * Class constructor
	 *
	 * @param cause
	 * @param message
	 */
	public ExceptionEntity(Throwable cause, String message) {
		this(cause.getClass().getName(), message);
	}

	/**
	 * @return the 'originalType' value
	 */
	public String getOriginalType() {
		return this._originalType;
	}

	/**
	 * @param originalType the 'originalType' value to set
	 */
	public void setOriginalType(String originalType) {
		this._originalType = originalType;
	}

	/**
	 * @return the 'message' value
	 */
	public String getMessage() {
		return this._message;
	}

	/**
	 * @param message the 'message' value to set
	 */
	public void setMessage(String message) {
		this._message = message;
	}

	/**
	 * @return the 'trace' value
	 */
	public String getTrace() {
		return this._trace;
	}

	/**
	 * @param trace the 'trace' value to set
	 */
	public void setTrace(String trace) {
		this._trace = trace;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._message == null ) ? 0 : this._message.hashCode() );
		result = prime * result + ( ( this._originalType == null ) ? 0 : this._originalType.hashCode() );
		result = prime * result + ( ( this._trace == null ) ? 0 : this._trace.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		ExceptionEntity other = (ExceptionEntity) obj;
		if(this._message == null) {
			if(other._message != null) return false;
		} else if(!this._message.equals(other._message)) return false;
		if(this._originalType == null) {
			if(other._originalType != null) return false;
		} else if(!this._originalType.equals(other._originalType)) return false;
		if(this._trace == null) {
			if(other._trace != null) return false;
		} else if(!this._trace.equals(other._trace)) return false;
		return true;
	}
}
