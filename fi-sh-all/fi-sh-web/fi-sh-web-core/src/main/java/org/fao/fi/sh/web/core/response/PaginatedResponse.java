/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
@XmlRootElement(name="paginatedResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel
public class PaginatedResponse<DATA> extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6126478553437584553L;

	@XmlAttribute(name="currentPageSize")
	@ApiModelProperty(value = "The size (in entries) of the current page dataset")
	private int currentPageSize;
	
	@XmlAttribute(name="maxPageSize")
	@ApiModelProperty(value = "The maximum size (in entries) of each page dataset")
	private int maxPageSize;

	@XmlAttribute(name="previousPageNumber")
	@ApiModelProperty(value = "The previous page number")
	private int previousPageNumber;
	
	@XmlAttribute(name="currentPageNumber")
	@ApiModelProperty(value = "The current page number")
	private int currentPageNumber;

	@XmlAttribute(name="lastPageNumber")
	@ApiModelProperty(value = "The last page number")
	private int lastPageNumber;
	
	@XmlAttribute(name="nextPageNumber")
	@ApiModelProperty(value = "The next page number")
	private int nextPageNumber;
	
	@XmlElement(name="previousPageURI")
	@ApiModelProperty(value = "The previous page URI")
	private String previousPageURI;
	
	@XmlElement(name="currentPageURI")
	@ApiModelProperty(value = "The current page URI")
	private String currentPageURI;

	@XmlElement(name="lastPageURI")
	@ApiModelProperty(value = "The last page URI")
	private String lastPageURI;

	@XmlElement(name="nextPageURI")
	@ApiModelProperty(value = "The next page URI")
	private String nextPageURI;
	
	/**
	 * Class constructor
	 */
	public PaginatedResponse() {
		// TODO Auto-generated constructor block
	}
	
	/**
	 * Class constructor
	 *
	 * @param currentPageSize
	 * @param maxPageSize
	 * @param previousPageNumber
	 * @param currentPageNumber
	 * @param lastPageNumber
	 * @param nextPageNumber
	 * @param previousPageURI
	 * @param currentPageURI
	 * @param lastPageURI
	 * @param nextPageURI
	 */
	public PaginatedResponse(int currentPageSize, int maxPageSize, int previousPageNumber, int currentPageNumber, int lastPageNumber, int nextPageNumber, String previousPageURI, String currentPageURI, String lastPageURI, String nextPageURI) {
		super();
		this.currentPageSize = currentPageSize;
		this.maxPageSize = maxPageSize;
		this.previousPageNumber = previousPageNumber;
		this.currentPageNumber = currentPageNumber;
		this.lastPageNumber = lastPageNumber;
		this.nextPageNumber = nextPageNumber;
		this.previousPageURI = previousPageURI;
		this.currentPageURI = currentPageURI;
		this.lastPageURI = lastPageURI;
		this.nextPageURI = nextPageURI;
	}

	/**
	 * @return the 'currentPageSize' value
	 */
	public int getCurrentPageSize() {
		return this.currentPageSize;
	}

	/**
	 * @param currentPageSize the 'currentPageSize' value to set
	 */
	public void setCurrentPageSize(int currentPageSize) {
		this.currentPageSize = currentPageSize;
	}

	/**
	 * @return the 'maxPageSize' value
	 */
	public int getMaxPageSize() {
		return this.maxPageSize;
	}

	/**
	 * @param maxPageSize the 'maxPageSize' value to set
	 */
	public void setMaxPageSize(int maxPageSize) {
		this.maxPageSize = maxPageSize;
	}

	/**
	 * @return the 'previousPageNumber' value
	 */
	public int getPreviousPageNumber() {
		return this.previousPageNumber;
	}

	/**
	 * @param previousPageNumber the 'previousPageNumber' value to set
	 */
	public void setPreviousPageNumber(int previousPageNumber) {
		this.previousPageNumber = previousPageNumber;
	}

	/**
	 * @return the 'currentPageNumber' value
	 */
	public int getCurrentPageNumber() {
		return this.currentPageNumber;
	}

	/**
	 * @param currentPageNumber the 'currentPageNumber' value to set
	 */
	public void setCurrentPageNumber(int currentPageNumber) {
		this.currentPageNumber = currentPageNumber;
	}

	/**
	 * @return the 'nextPageNumber' value
	 */
	public int getNextPageNumber() {
		return this.nextPageNumber;
	}

	/**
	 * @param nextPageNumber the 'nextPageNumber' value to set
	 */
	public void setNextPageNumber(int nextPageNumber) {
		this.nextPageNumber = nextPageNumber;
	}

	/**
	 * @return the 'previousPageURI' value
	 */
	public String getPreviousPageURI() {
		return this.previousPageURI;
	}

	/**
	 * @param previousPageURI the 'previousPageURI' value to set
	 */
	public void setPreviousPageURI(String previousPageURI) {
		this.previousPageURI = previousPageURI;
	}

	/**
	 * @return the 'currentPageURI' value
	 */
	public String getCurrentPageURI() {
		return this.currentPageURI;
	}

	/**
	 * @param currentPageURI the 'currentPageURI' value to set
	 */
	public void setCurrentPageURI(String currentPageURI) {
		this.currentPageURI = currentPageURI;
	}

	/**
	 * @return the 'nextPageURI' value
	 */
	public String getNextPageURI() {
		return this.nextPageURI;
	}

	/**
	 * @param nextPageURI the 'nextPageURI' value to set
	 */
	public void setNextPageURI(String nextPageURI) {
		this.nextPageURI = nextPageURI;
	}
	
	/**
	 * @return the 'lastPageNumber' value
	 */
	public int getLastPageNumber() {
		return this.lastPageNumber;
	}

	/**
	 * @param lastPageNumber the 'lastPageNumber' value to set
	 */
	public void setLastPageNumber(int lastPageNumber) {
		this.lastPageNumber = lastPageNumber;
	}

	/**
	 * @return the 'lastPageURI' value
	 */
	public String getLastPageURI() {
		return this.lastPageURI;
	}

	/**
	 * @param lastPageURI the 'lastPageURI' value to set
	 */
	public void setLastPageURI(String lastPageURI) {
		this.lastPageURI = lastPageURI;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.currentPageNumber;
		result = prime * result + this.currentPageSize;
		result = prime * result + ( ( this.currentPageURI == null ) ? 0 : this.currentPageURI.hashCode() );
		result = prime * result + this.lastPageNumber;
		result = prime * result + ( ( this.lastPageURI == null ) ? 0 : this.lastPageURI.hashCode() );
		result = prime * result + this.maxPageSize;
		result = prime * result + this.nextPageNumber;
		result = prime * result + ( ( this.nextPageURI == null ) ? 0 : this.nextPageURI.hashCode() );
		result = prime * result + this.previousPageNumber;
		result = prime * result + ( ( this.previousPageURI == null ) ? 0 : this.previousPageURI.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		PaginatedResponse other = (PaginatedResponse) obj;
		if(this.currentPageNumber != other.currentPageNumber) return false;
		if(this.currentPageSize != other.currentPageSize) return false;
		if(this.currentPageURI == null) {
			if(other.currentPageURI != null) return false;
		} else if(!this.currentPageURI.equals(other.currentPageURI)) return false;
		if(this.lastPageNumber != other.lastPageNumber) return false;
		if(this.lastPageURI == null) {
			if(other.lastPageURI != null) return false;
		} else if(!this.lastPageURI.equals(other.lastPageURI)) return false;
		if(this.maxPageSize != other.maxPageSize) return false;
		if(this.nextPageNumber != other.nextPageNumber) return false;
		if(this.nextPageURI == null) {
			if(other.nextPageURI != null) return false;
		} else if(!this.nextPageURI.equals(other.nextPageURI)) return false;
		if(this.previousPageNumber != other.previousPageNumber) return false;
		if(this.previousPageURI == null) {
			if(other.previousPageURI != null) return false;
		} else if(!this.previousPageURI.equals(other.previousPageURI)) return false;
		return true;
	}

	
}