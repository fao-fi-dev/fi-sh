/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.users;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.web.core.response.BasicResponse;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
@XmlRootElement(name="logoutResponse")
@ApiModel public class LogoutResponse extends BasicResponse {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5060769828650038870L;
	
	@XmlAttribute(name="token")
	@ApiModelProperty(value = "The authenticated session token", required=true)
	private String token;
	
	/**
	 * Class constructor
	 *
	 */
	public LogoutResponse() {
		super("logout");
	}

	/**
	 * Class constructor
	 *
	 * @param token
	 */
	public LogoutResponse(String token) {
		this(token, null);
	}
	
	/**
	 * Class constructor
	 *
	 * @param token
	 * @param message
	 */
	public LogoutResponse(String token, String message) {
		this();
		this.token = token;
		this.message = message;
	}

	/**
	 * @return the 'token' value
	 */
	public String getToken() {
		return this.token;
	}

	/**
	 * @param token the 'token' value to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.token == null ) ? 0 : this.token.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		LogoutResponse other = (LogoutResponse) obj;
		if(this.token == null) {
			if(other.token != null) return false;
		} else if(!this.token.equals(other.token)) return false;
		return true;
	}
}
