/**
 * (c) 2015 FAO / UN (project: fmis-web-common)
 */
package org.fao.fi.sh.web.core.response.data.upload;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.upload.FormUploadResult;
import org.fao.fi.sh.model.core.spi.Form;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 May 2015
 */
@XmlRootElement(name="multipleFormsBulkUploadResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for multiple forms bulk upload responses")
public class MultipleFormsBulkUploadResponse<FORM extends Form> {
	@XmlElementWrapper(name="uploadResults")
	@XmlElement(name="uploadResult")
	@ApiModelProperty(value="The list of forms / validation results returned by the upload request")
	protected List<FormUploadResult<FORM>> uploadResults;
	
	/**
	 * Class constructor
	 *
	 */
	public MultipleFormsBulkUploadResponse() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param uploadResults
	 */
	public MultipleFormsBulkUploadResponse(List<FormUploadResult<FORM>> uploadResults) {
		super();
		this.uploadResults = uploadResults;
	}

	/**
	 * @return the 'uploadResults' value
	 */
	public List<FormUploadResult<FORM>> getUploadResults() {
		return this.uploadResults;
	}

	/**
	 * @param uploadResults the 'uploadResults' value to set
	 */
	public void setUploadResults(List<FormUploadResult<FORM>> uploadResults) {
		this.uploadResults = uploadResults;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.uploadResults == null ) ? 0 : this.uploadResults.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		MultipleFormsBulkUploadResponse other = (MultipleFormsBulkUploadResponse) obj;
		if(this.uploadResults == null) {
			if(other.uploadResults != null) return false;
		} else if(!this.uploadResults.equals(other.uploadResults)) return false;
		return true;
	}
}
