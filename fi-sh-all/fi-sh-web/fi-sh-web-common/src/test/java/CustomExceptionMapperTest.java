import org.fao.fi.sh.web.common.support.jersey.CustomExceptionMapper;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.DataAccessException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Sep 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Sep 2015
 */
public class CustomExceptionMapperTest {
	private class DataAccessExceptionMock extends DataAccessException {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 4115983449624531131L;

		public DataAccessExceptionMock(String msg, Throwable cause) {
			super(msg, cause); // TODO Auto-generated constructor block
		}

		public DataAccessExceptionMock(String msg) {
			super(msg); // TODO Auto-generated constructor block
		}
	}
	
	public @Test void testMessageExtraction() {
		String message = "Error updating database. Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique' ### The error may involve org.fao.fi.fmis.persistence.mybatis.mapper.generated.annotated.RegVesselsLicensePermitMapper.insertSelective-Inline ### The error occurred while setting parameters ### SQL: INSERT INTO reg_vessels_license_permit (REG_VESSEL_ID, PERMIT_NUMBER, APPLICATION_DATE, PERMIT_DATE, DATE_FROM, DATE_TO, PERSONS_CARRIED_ONBOARD, REG_ENTITY_ID_APPLICANT, APPLICANT_IS_OWNER, SPECIAL_CONDITIONS, UPDATER_ID, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ### Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique' ; SQL []; Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique'; nested exception is com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique'";
		
		String parsed = new CustomExceptionMapper().extractMessage(new DataAccessExceptionMock(message));
		
		Assert.assertEquals("Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique'", parsed);
	}
	
	public @Test void testMessageExtractionWithNewlines() {
		String message = "Error updating database.\n Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate\r entry \n'FDC-001186-2015-09-15' \n\rfor key 'reg_vessels_license_permit_unique' ### The error may involve org.fao.fi.fmis.persistence.mybatis.mapper.generated.annotated.RegVesselsLicensePermitMapper.insertSelective-Inline ### The error occurred while setting parameters ### SQL: INSERT INTO reg_vessels_license_permit (REG_VESSEL_ID, PERMIT_NUMBER, APPLICATION_DATE, PERMIT_DATE, DATE_FROM, DATE_TO, PERSONS_CARRIED_ONBOARD, REG_ENTITY_ID_APPLICANT, APPLICANT_IS_OWNER, SPECIAL_CONDITIONS, UPDATER_ID, UPDATE_DATE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ### Cause: com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique' ; SQL []; Duplicate entry 'FDC-001186-2015-09-15' for key 'reg_vessels_license_permit_unique'; nested exception is com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry 'FDC-001186-2015-09-15' \nfor key \n\r'reg_vessels_license_permit_unique'";
		
		String parsed = new CustomExceptionMapper().extractMessage(new DataAccessExceptionMock(message));
		
		Assert.assertEquals("Duplicate\r entry \n'FDC-001186-2015-09-15' \n\rfor key 'reg_vessels_license_permit_unique'", parsed);
	}
	
	public @Test void testMessageExtractionVerbatim() {
		String message = "Foobaz bar snafu";
		
		String parsed = new CustomExceptionMapper().extractMessage(new DataAccessExceptionMock(message));
		
		Assert.assertEquals(message, parsed);
	}
	
	public @Test void testMessageExtractionOther() {
		String message = "Foobaz bar snafu";
		
		String parsed = new CustomExceptionMapper().extractMessage(new RuntimeException(message));
		
		Assert.assertEquals(message, parsed);
	}
}
