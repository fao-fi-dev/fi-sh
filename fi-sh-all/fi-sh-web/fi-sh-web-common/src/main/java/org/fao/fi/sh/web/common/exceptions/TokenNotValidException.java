/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.exceptions;

import javax.ws.rs.core.Response;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
public class TokenNotValidException extends TokenException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2989767597289350561L;

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param status
	 */
	public TokenNotValidException(String message) {
		super(message, Response.Status.FORBIDDEN); 
	}
}
