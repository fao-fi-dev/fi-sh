/**
 * (c) 2015 FAO / UN (project: fis-web-common)
 */
package org.fao.fi.sh.web.common.config;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2015
 */
public interface AuthTokenConfiguration {
	String getAuthTokenHeaderName();
}
