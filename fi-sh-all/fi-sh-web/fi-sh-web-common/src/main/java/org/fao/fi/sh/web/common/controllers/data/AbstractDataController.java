/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.controllers.data;

import java.util.List;

import javax.inject.Inject;

import org.fao.fi.sh.business.processes.data.validation.BusinessRulesValidatorFactory;
import org.fao.fi.sh.business.processes.data.validation.spi.BusinessRulesValidator;
import org.fao.fi.sh.business.processes.data.validation.spi.exceptions.BusinessValidatorException;
import org.fao.fi.sh.model.core.basic.upload.FormUploadResult;
import org.fao.fi.sh.model.core.basic.upload.UploadValidationResult;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationResult;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.web.common.controllers.AbstractController;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
abstract public class AbstractDataController extends AbstractController {
	@Inject protected BusinessRulesValidatorFactory _validatorFactory;
	
	protected <F extends Form, U extends FormUploadResult<F>> U validate(U toValidate) throws BusinessValidatorException {
		F form;
		
		boolean uploadValidationFailed = false;
		
		form = toValidate.getForm();
		
		for(UploadValidationResult of : toValidate.getUploadValidationResults()) {
			uploadValidationFailed |= of.hasFailed();
		}
			
		if(!uploadValidationFailed) {
			for(BusinessRulesValidator<Form> validator : _validatorFactory.validatorsFor(form))
				toValidate.setBusinessValidationResults(validator.validate(form));
		}
		
		return toValidate;
	}
	
	protected <F extends Form, U extends FormUploadResult<F>> List<U> validate(List<U> toValidate) throws BusinessValidatorException {
		F form;
		
		boolean uploadValidationFailed = false;
		
		for(FormUploadResult<F> in : toValidate) {
			form = in.getForm();
		
			for(UploadValidationResult of : in.getUploadValidationResults()) {
				uploadValidationFailed |= of.hasFailed();
			}
			
			if(!uploadValidationFailed) {
				for(BusinessRulesValidator<Form> validator : _validatorFactory.validatorsFor(form))
					in.setBusinessValidationResults(validator.validate(form));
			}
		}
		
		return toValidate;
	}
	
	protected <F extends Form, U extends FormUploadResult<F>> boolean uploadValidationFailed(U result) {
		for(UploadValidationResult of : result.getUploadValidationResults()) {
			if(of.hasFailed())
				return true;
		}
		
		return false;
	}
	
	protected <F extends Form, U extends FormUploadResult<F>> boolean uploadValidationFailed(List<U> results) {
		for(U in : results) {
			if(uploadValidationFailed(in))
				return true;
		}
		
		return false;
	}
	
	protected <F extends Form, U extends FormUploadResult<F>> boolean businessValidationFailed(U result) {
		for(BusinessValidationResult of : result.getBusinessValidationResults()) {
			if(of.hasFailed())
				return true;
		}
		
		return false;
	}
	
	protected <F extends Form, U extends FormUploadResult<F>> boolean businessValidationFailed(List<U> results) {
		for(U in : results) {
			if(businessValidationFailed(in))
				return true;
		}
		
		return false;
	}
}