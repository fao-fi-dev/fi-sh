/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
public class TokenException extends WebApplicationException  {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6976463859878219886L;

	/**
	 * Class constructor
	 *
	 * @param status
	 */
	public TokenException(Status status) {
		super(status); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param status
	 */
	public TokenException(String message, Status status) {
		super(message, status); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 * @param status
	 * @throws IllegalArgumentException
	 */
	public TokenException(String message, Throwable cause, Status status) throws IllegalArgumentException {
		super(message, cause, status); // TODO Auto-generated constructor block
	}
}
