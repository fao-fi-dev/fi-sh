/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.controllers;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$pos;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.model.core.spi.Commentable;
import org.fao.fi.sh.model.core.spi.Updatable;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.fao.fi.sh.web.common.UserSessionsManager;
import org.fao.fi.sh.web.common.config.AuthTokenConfiguration;
import org.fao.fi.sh.web.common.exceptions.ControllerException;
import org.fao.fi.sh.web.common.exceptions.PersistenceException;
import org.fao.fi.sh.web.common.exceptions.TokenException;
import org.fao.fi.sh.web.common.exceptions.TokenMissingException;
import org.fao.fi.sh.web.core.response.OperationAwareResponse;
import org.springframework.dao.DataAccessException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
public abstract class AbstractController extends AbstractLoggingAwareClient {
	final static public String[] XML_OR_JSON_MEDIA_TYPES     = new String[] { MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON };
	final static public String XML_OR_JSON_MEDIA_TYPE_STRING = MediaType.APPLICATION_XML + ", " + MediaType.APPLICATION_JSON;
	
	@Inject protected AuthTokenConfiguration _tokenConfiguration;
	@Inject protected UserSessionsManager _sessionManager;
	
	/**
	 * Class constructor
	 */
	public AbstractController() {
	}
	
	public <O> O notNull(String operation, O data) {
		if(data == null) throw new ControllerException(operation, Status.NO_CONTENT);
		
		return data;
	}
	
	public <O, C extends Collection<O>> C notNull(String operation, C collection) {
		if(nullified(collection) == null) throw new ControllerException(operation, Status.NO_CONTENT);
		
		return collection;
	}
	
	public <O> O[] notNull(String operation, O[] array) {
		if(array == null || array.length == 0) throw new ControllerException(operation, Status.NO_CONTENT);
		
		return array;
	}
	
	public <O, C extends Collection<O>> C nullified(C collection) {
		return collection == null || collection.isEmpty() ? null : collection;
	}
	
	public <O> O[] nullified(O[] array) {
		return array == null || array.length == 0 ? null : array;
	}
	
	public <U extends Updatable> U withCurrentUpdater(U data, UserModel current) {
		data.setUpdaterId(current.getId());
		data.setUpdateDate(new Date());
		
		return data;
	}
	
	public <C extends Commentable> C comment(C data, String comment) {
		data.setComment(comment);
		
		return data;
	}
	
	/**
	 * @param hook should be an inner class created by the invoker method by providing <code>new Object(){}</code> as argument.
	 * @return the <code>@Path</code> value for the invoker method (if available).
	 */
	final static protected String methodPath(Object hook) {
		Method invoker = hook.getClass().getEnclosingMethod();
		
		if(invoker.isAnnotationPresent(Path.class)) 
			return invoker.getAnnotation(Path.class).value();
		
		return null;
	}
	
	/**
	 * @param hook should be an inner class created by the invoker method by providing <code>new Object(){}</code> as argument.
	 * @return the name of the invoker method.
	 */
	final static protected String methodName(Object hook) {
		return hook.getClass().getEnclosingMethod().getName();
	}
	
	/**
	 * @param hook should be an inner class created by the invoker method by providing <code>new Object(){}</code> as argument.
	 * @return the first non-null result among {@link #methodPath(Object)} and {@link #methodName(Object)} for the provided argument.
	 */
	final static protected String currentOperation(Object hook) {
		String path = methodPath(hook);
		
		return path != null ? path : methodName(hook); 
	}
	
	/**
	 * @param request
	 * @return
	 * @throws TokenMissingException
	 */
	final protected String extractTokenFrom(HttpServletRequest request) throws TokenMissingException {
		$nN(request, "The HTTP Servlet Request cannot be null");
		
		String token = request.getHeader(_tokenConfiguration.getAuthTokenHeaderName());
		
		token = token == null ? null : token.trim();
				
		if(token == null || "".equals(token)) {
			token = request.getParameter(_tokenConfiguration.getAuthTokenHeaderName());
			
			token = token == null ? null : token.trim();
			
			if(token == null || "".equals(token)) {
				throw new TokenMissingException("Null (or null-equivalent) token provided");
			}
		}
		
		return token;
	}
	
	/**
	 * @param request
	 * @param quiet
	 * @return
	 * @throws TokenMissingException
	 */
	final protected String requireValidToken(HttpServletRequest request, boolean quiet) throws TokenMissingException {
		return _sessionManager.mustBeValid(extractTokenFrom(request), quiet);
	}
	
	/**
	 * @param operation
	 * @param cause
	 * @return
	 */
	final protected Response handle(String operation, TokenException cause) {
		_log.warn("{} caught{}: {}", cause.getClass().getSimpleName(), operation == null ? "" : " for '" + operation + "' operation", cause.getMessage(), cause);
		
		return Response.status(cause.getResponse().getStatus()).entity(OperationAwareResponse.forOperation(operation).withMessage(cause.getMessage())).build();
	}

	/**
	 * @param cause
	 * @return
	 */
	final protected Response handle(TokenException cause) {
		return this.handle(null, cause);
	}
	
	/**
	 * @param cause
	 * @return
	 */
	final protected WebApplicationException wrap(Throwable cause) {
		_log.error("{} caught: {}", cause.getClass().getSimpleName(), cause.getMessage(), cause);
		
		if(cause instanceof TokenException) { return (TokenException)cause; }
		else if (cause instanceof DataAccessException) { return new PersistenceException(cause.getMessage(), cause); }
			
		return new InternalServerErrorException(cause.getMessage(), cause);
	}
	
	final protected Authorizer checkThat(UserModel user) {
		return new Authorizer(user);
	}
	
	final protected UserModel userIn(HttpServletRequest request) {
		String token = extractTokenFrom(request);
		
		if(token == null) throw new TokenMissingException("Missing " + _tokenConfiguration.getAuthTokenHeaderName() + " request header");
		
		return _sessionManager.user(token);
	}
	
	protected class Authorizer {
		protected UserModel _user = null;
		
		public Authorizer(UserModel user) {
			$nN(user, "Cannot check authorization on a null user");
			
			_user = user;
		}
	
		protected boolean isAdmin() {
			if(_user != null) { for(String in : _user.listRoles()) { if("*".equals(in)) return true; } };
			
			return false;
		}
		
		public Checker exists() throws NotAuthorizedException {
			if(_user == null) throw new NotAuthorizedException("A valid user session is required for the requested operation");

			return new Checker(_user);
		}
	}

	final protected class Checker extends Authorizer {
		private boolean _valid = true;
		
		/**
		 * Class constructor
		 *
		 * @param user
		 */
		public Checker(UserModel user) {
			super(user);
		}
	
		public Checker canAny(String... capabilities) {
			$pos($nN(capabilities, "Provided capabilities cannot be null").length, "Provided capabilities cannot be empty");			
			
			for(String capability : capabilities) {
				_valid |= _user.can($nN(capability, "One of the provided capability is null"));
			}
			
			_valid |= isAdmin();
			
			return this;
		}
		
		public Checker orCanAny(String... capabilities) {
			return _valid ? this : canAny(capabilities);
		}
		
		public Checker andCanAny(String... capabilities) {
			$pos($nN(capabilities, "Provided capabilities cannot be null").length, "Provided capabilities cannot be empty");
			
			boolean local = false;
			
			if(_valid) {
				for(String capability : capabilities) {
					local |= _user.can($nN(capability, "One of the provided capability is null"));
				}
				
				_valid &= local;
			}
			
			return this;
		}
		
		public Checker can(String... capabilities) {
			$pos($nN(capabilities, "Provided capabilities cannot be null").length, "Provided capabilities cannot be empty");
			
			if(_valid)
				for(String capability : capabilities) {
					_valid &= _user.can($nN(capability, "One of the provided capability is null"));
				}
			
			_valid |= isAdmin();
			
			return this;
		}
		
		public Checker orCan(String... capabilities) {
			return _valid ? this : orCanAny(capabilities);
		}
		
		public Checker andCan(String... capabilities) {
			$pos($nN(capabilities, "Provided capabilities cannot be null").length, "Provided capabilities cannot be empty");
	
			boolean local = true;
			
			if(_valid) {
				for(String capability : capabilities) {
					local &= _user.can($nN(capability, "One of the provided capability is null"));
				}
				
				_valid &= local;
			}
			
			_valid |= isAdmin();
			
			return this;
		}
		
		public Checker isAny(String... roles) {
			$pos($nN(roles, "Provided roles cannot be null").length, "Provided roles cannot be empty");
			
			for(String role : roles) {
				_valid |= _user.is($nN(role, "One of the provided role is null"));
			}
	
			_valid |= isAdmin();
			
			return this;
		}
		
		public Checker orIsAny(String... roles) {
			return _valid ? this : isAny(roles);
		}
		
		public Checker andIsAny(String... roles) {
			$pos($nN(roles, "Provided roles cannot be null").length, "Provided roles cannot be empty");
	
			boolean local = true;
			
			if(_valid) {
				for(String role : roles) {
					local |= _user.is($nN(role, "One of the provided role is null"));
				}
				
				_valid &= local;
			}
	
			_valid |= isAdmin();
	
			return this;
		}
		
		public Checker is(String... roles) {
			$pos($nN(roles, "Provided roles cannot be null").length, "Provided roles cannot be empty");
			
			if(_valid) {
				for(String role : roles) {
					_valid &= _user.is($nN(role, "One of the provided role is null"));
				}
			}
			
			_valid |= isAdmin();
	
			return this;
		}
		
		public Checker orIs(String... roles) {
			return _valid ? this : is(roles);
		}
		
		public Checker andIs(String... roles) {
			$pos($nN(roles, "Provided roles cannot be null").length, "Provided roles cannot be empty");
	
			boolean local = true;
			
			if(_valid) {
				for(String role : roles) {
					local &= _user.is($nN(role, "One of the provided role is null"));
				}
				
				_valid &= local;
			}
	
			_valid |= isAdmin();
	
			return this;
		}
		
		public boolean isValid() {
			exists();
			
			return _valid;
		}
		
		public <O> O thenReturn(O toReturn) throws NotAuthorizedException {
			if(!_valid) throw new ForbiddenException("Current user (" + _user.getId() + ") is not authorized to perform the requested operation");
			
			return toReturn;
		}
		
		public UserModel thenProceed() throws NotAuthorizedException {
			return thenReturn(_user);
		}
	}
}