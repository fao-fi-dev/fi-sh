/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.exceptions;

import javax.ws.rs.core.Response.Status;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Apr 2015
 */
public class CodelistEntryNotFoundException extends ControllerException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3031579984323781915L;

	/**
	 * Class constructor
	 *
	 * @param operation
	 * @param status
	 */
	public CodelistEntryNotFoundException(String operation) {
		super(operation, Status.NOT_FOUND); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param operation
	 * @param message
	 * @param status
	 */
	public CodelistEntryNotFoundException(String operation, String message) {
		super(operation, message, Status.NOT_FOUND); // TODO Auto-generated constructor block
	}
}
