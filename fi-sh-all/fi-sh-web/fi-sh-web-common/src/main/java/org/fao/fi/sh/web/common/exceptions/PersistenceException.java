/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
public class PersistenceException extends WebApplicationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3588798423347378696L;

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public PersistenceException(String message) {
		super(message, Status.INTERNAL_SERVER_ERROR);
	}
	
	/**
	 * Class constructor
	 *
	 * @param cause
	 * @param status
	 * @throws IllegalArgumentException
	 */
	public PersistenceException(Throwable cause) throws IllegalArgumentException {
		super(cause, Status.INTERNAL_SERVER_ERROR); 
	}
	
	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 * @throws IllegalArgumentException
	 */
	public PersistenceException(String message, Throwable cause) throws IllegalArgumentException {
		super(message, cause, Status.INTERNAL_SERVER_ERROR); 
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param status
	 */
	public PersistenceException(String message, Status status) {
		super(message, status);
	}
	
	/**
	 * Class constructor
	 *
	 * @param cause
	 * @param status
	 * @throws IllegalArgumentException
	 */
	public PersistenceException(Throwable cause, Status status) throws IllegalArgumentException {
		super(cause, status); 
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 * @param status
	 * @throws IllegalArgumentException
	 */
	public PersistenceException(String message, Throwable cause, Status status) throws IllegalArgumentException {
		super(message, cause, status); 
	}
}
