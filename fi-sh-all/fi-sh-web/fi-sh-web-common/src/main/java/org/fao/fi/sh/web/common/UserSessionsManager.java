/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.fao.fi.sh.model.core.auth.UserModel;
import org.fao.fi.sh.model.core.basic.composite.CoreUserModel;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.common.helpers.MD5Helper;
import org.fao.fi.sh.web.common.exceptions.TokenAlreadyRegisterdException;
import org.fao.fi.sh.web.common.exceptions.TokenNotFoundException;
import org.fao.fi.sh.web.common.exceptions.TokenNotValidException;
import org.fao.fi.sh.web.core.auth.UserSession;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Apr 2015
 */
@Singleton @Named("user.sessions.manager")
public class UserSessionsManager extends AbstractLoggingAwareClient {
	final static public boolean QUIET_SESSION_CHECK = true;
	final static public boolean TOUCH_SESSION_CHECK  = !QUIET_SESSION_CHECK;
	
	@Inject private @Named("user.sessions.ehcache") Cache _cache;
	
	/**
	 * Class constructor
	 *
	 */
	public UserSessionsManager() {
	}
	
	private String tokenFor(String userId) {
		try {
			return MD5Helper.digest(MD5Helper.digest(System.nanoTime() + userId + System.currentTimeMillis()));
		} catch(Throwable t) {
			return userId;
		}
	}

	public <USER_MODEL extends CoreUserModel> String register(USER_MODEL user, String loginIP) throws TokenAlreadyRegisterdException {
		String token = tokenFor($nN($nN(user, "The user cannot be null").getId(), "The user ID cannot be null"));
		
		if(!_cache.isKeyInCache(token)) {
			_cache.put(new Element(token, new UserSession<USER_MODEL>(token, loginIP, user)));
		} else
			throw new TokenAlreadyRegisterdException("Session token ('" + token + "') is already registered!");
		
		return token;
	}
	
	
	/**
	 * Checks that a user session token is valid or not.
	 * 
	 * <b>Invoking this method with quiet=true will 'touch' the user session (if available) thus updating its last access time</b>
	 * 
	 * @param token a user session token
	 * @return the token, if valid.
	 * @throws TokenNotValidException
	 */
	public String mustBeValid(String token, boolean quiet) throws TokenNotValidException {
		if(!isValid($nN(token, "The User Session token cannot be null"))) {
			throw new TokenNotValidException("Provided token ('" + token + "') is not valid!");
		}
		
		Element current = quiet ? _cache.getQuiet(token) : _cache.get(token);
		
		if(current == null || _cache.isExpired(current))
			throw new TokenNotValidException("Provided token ('" + token + "') refers to an invalid (expired) session!");
		
		@SuppressWarnings("unchecked")
		UserSession<? extends UserModel> currentUserSession = (UserSession<? extends UserModel>)current.getObjectValue();
		
		if(currentUserSession != null) {
			if(currentUserSession.getUser() != null) {
				if(!quiet) _cache.put(new Element(token, currentUserSession.access()));
			} else 
				throw new TokenNotValidException("Provided token ('" + token + "') refers to a session with a null user!");
		} else {
			throw new TokenNotValidException("Provided token ('" + token + "') refers to null session!");
		}
		
		return token;
	}
	
	public boolean isValid(String token) {
		return _cache.isKeyInCache($nN(token, "The User Session token cannot be null"));
	}
	
	public String invalidate(String token) throws TokenNotFoundException {
		if(_cache.isKeyInCache($nN(token, "The User Session token cannot be null")))
			_cache.remove(token);
		else
			throw new TokenNotFoundException("Provided token ('" + token + "') is not registered!");
		
		return token;
	}
	
	@SuppressWarnings("unchecked")
	public <USER_MODEL extends CoreUserModel> USER_MODEL user(String token) {
		mustBeValid($nN(token, "The User Session token cannot be null"), TOUCH_SESSION_CHECK);
		
		return ((UserSession<USER_MODEL>)_cache.get(token).getObjectValue()).getUser();
	}
	
	public Element userSessionElement(String token) {
		mustBeValid($nN(token, "The User Session token cannot be null"), QUIET_SESSION_CHECK);
		
		return _cache.getQuiet(token);
	}
	
	@SuppressWarnings("unchecked")
	public <USER_MODEL extends CoreUserModel, USER_SESSION extends UserSession<USER_MODEL>> List<USER_SESSION> listSessions() {
		List<USER_SESSION> sessions = new ArrayList<USER_SESSION>();
		
		for(Object key : _cache.getKeys()) {
			sessions.add((USER_SESSION)_cache.getQuiet(key).getObjectValue());
		}
		
		return sessions;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> sessionTokensForUser(String userId) {
		List<String> tokens = new ArrayList<String>();
		
		UserSession<? extends UserModel> current;
		for(Object key : _cache.getKeys()) {
			current = (UserSession<? extends UserModel>)_cache.getQuiet(key).getObjectValue();
			
			if(current.getUser() != null && $nN(userId, "Please provide a non-null user id").equals(current.getUser().getId()))
				tokens.add(current.getToken());
		}

		return tokens;
	}
}