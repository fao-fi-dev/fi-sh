/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
public class ControllerException extends WebApplicationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3588798423347378696L;

	private String _operation = "operation not set";
	
	/**
	 * Class constructor
	 *
	 * @param status
	 */
	public ControllerException(String operation, Status status) {
		this(operation, null, null, status);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param status
	 */
	public ControllerException(String operation, String message, Status status) {
		this(operation, message, null, status);
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 * @param status
	 * @throws IllegalArgumentException
	 */
	public ControllerException(String operation, String message, Throwable cause, Status status) throws IllegalArgumentException {
		super(message, cause, status); // TODO Auto-generated constructor block
		
		this._operation = operation;
	}

	/**
	 * @return the 'operation' value
	 */
	public String getOperation() {
		return this._operation;
	}

	/**
	 * @param operation the 'operation' value to set
	 */
	public void setOperation(String operation) {
		this._operation = operation;
	}
}
