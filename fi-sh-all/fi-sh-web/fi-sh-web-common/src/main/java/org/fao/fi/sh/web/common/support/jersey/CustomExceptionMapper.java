/**
 * (c) 2015 FAO / UN (project: fmis-web-controllers)
 */
package org.fao.fi.sh.web.common.support.jersey;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataException;
import org.fao.fi.sh.web.common.exceptions.ControllerException;
import org.fao.fi.sh.web.core.exceptions.ExceptionEntity;
import org.fao.fi.sh.web.core.response.BasicResponse;
import org.fao.fi.sh.web.core.response.OperationAwareResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
@Provider
public class CustomExceptionMapper implements ExceptionMapper<Throwable> {
	final private Logger _log = LoggerFactory.getLogger(this.getClass());

	final private Pattern DAE_MESSAGE_EXTRACTOR_PATTERN = Pattern.compile("(.+Exception\\:)([^\\#]+)(\\#.+)", Pattern.DOTALL);
	
	private boolean isManaged(Throwable cause) {
		Class<?> type = cause.getClass();
		
		return IllegalArgumentException.class.isAssignableFrom(type) ||
			   DataAccessException.class.isAssignableFrom(type) ||
			 ( WebApplicationException.class.isAssignableFrom(type) && !InternalServerErrorException.class.isAssignableFrom(type) );
	}
	
	private boolean isAControllerException(Throwable cause) {
		Class<?> type = cause.getClass();
		
		return ControllerException.class.isAssignableFrom(type);
	}
	
	public Response toResponse(Throwable cause) {
		return Response.status(this.toStatusCode(cause)).
						entity(
							isAControllerException(cause) ?
								new OperationAwareResponse(((ControllerException)cause).getOperation(), extractMessage(cause))
							: isManaged(cause) ? 
								new BasicResponse(extractMessage(cause)) 
								: new ExceptionEntity(cause)).
						build();
	}
	
	protected Status toStatusCode(Throwable cause) {
		Status fromCode = Status.INTERNAL_SERVER_ERROR;
		
		try {
			throw cause;
		} catch(IllegalArgumentException IAe) {
			fromCode = Status.BAD_REQUEST;
			
			_log.warn("Handling managed {} ({}) as an HTTP {} error ({})", cause.getClass().getName(), cause.getMessage(), fromCode.getStatusCode(), fromCode.getReasonPhrase(), cause);
		} catch(DataAccessException DAe) {
			fromCode = Status.CONFLICT;
			
			String message = DAe.getMessage(), parsed = extractMessage(DAe);
			
			_log.warn("Handling managed {} ({}) as an HTTP {} error ({})", cause.getClass().getName(), message.equals(parsed) ? message : parsed, fromCode.getStatusCode(), fromCode.getReasonPhrase(), cause);
		} catch(BulkDataException BDe) {
			fromCode = Status.BAD_REQUEST;
			
			_log.warn("Handling managed {} ({}) as an HTTP {} error ({})", cause.getClass().getName(), cause.getMessage(), fromCode.getStatusCode(), fromCode.getReasonPhrase(), cause);
		} catch(WebApplicationException WAe) {
			fromCode = Status.fromStatusCode(((WebApplicationException)cause).getResponse().getStatus());
			
			_log.warn("Handling managed {} ({}) as an HTTP {} error ({})", cause.getClass().getName(), cause.getMessage(), fromCode.getStatusCode(), fromCode.getReasonPhrase(), cause);
		} catch(Throwable t) {
			_log.error("Handling uncaught {} ({}) as an HTTP {} error ({})", cause.getClass().getName(), cause.getMessage(), fromCode.getStatusCode(), fromCode.getReasonPhrase(), cause);
		}
		
		return fromCode;
	}
	
	public String extractMessage(Throwable e) {
		return e.getMessage();
	}
	
	public String extractMessage(DataAccessException DAe) {
		String message = DAe.getMessage();

		Matcher matcher = DAE_MESSAGE_EXTRACTOR_PATTERN.matcher(message);
		
		if(matcher.matches()) return matcher.group(2).trim();
		
		return message;
	}
}