/**
 * (c) 2015 FAO / UN (project: fis-utility-common)
 */
package org.fao.fi.sh.utility.common.helpers;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$eq;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$gte;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
final public class MD5Helper extends AbstractHelper {
	/**
	 * Class constructor
	 *
	 */
	private MD5Helper() {
	}
	
	static public String digest(String content) throws NoSuchAlgorithmException {
		return MD5Helper.digest(content, 1);
	}
	
	static public String digest(String content, int iterations) throws NoSuchAlgorithmException {
		return MD5Helper.digest(content == null ? null : content.getBytes(), iterations);
	}
	
	static public String digest(byte[] content) throws NoSuchAlgorithmException {
		return MD5Helper.digest(content, 1);
	}
	
	static public String digest(byte[] content, int iterations) throws NoSuchAlgorithmException {
		$gte(iterations, 1, "The number of iterations should be greater than (or equal) to 1 (currently: {})", iterations);
		
		MessageDigest complete = MessageDigest.getInstance("MD5");
		
		complete.update(content == null ? new byte[0] : content);

		String digest = MD5Helper.convertToHexString(complete.digest());
		
		for(int n=0; n < iterations - 1; iterations--)
			digest = MD5Helper.digest(digest, 1);
			
		return digest;
	}
	
	static public String digest(File file) throws NoSuchAlgorithmException, IOException {
		return MD5Helper.digest(file, 1);
	}
	
	static public String digest(File file, int iterations) throws NoSuchAlgorithmException, IOException {
		return MD5Helper.digest(file == null ? null : new FileInputStream(file), iterations);
	}
	
	static public String digest(InputStream stream) throws NoSuchAlgorithmException, IOException {
		return MD5Helper.digest(stream, 1);
	}
	
	static public String digest(InputStream stream, int iterations) throws NoSuchAlgorithmException, IOException {
		$gte(iterations, 1, "The number of iterations should be greater than (or equal) to 1 (currently: {})", iterations);
		$nN(stream, "The stream to digest should not be null");
		
		MessageDigest messageDigest = MessageDigest.getInstance("MD5");

		String digest;
		
		try(DigestInputStream digestStream = new DigestInputStream(stream, messageDigest)) {
			byte[] buffer = new byte[8192];

			@SuppressWarnings("unused") int len = -1;
			
			while((len = digestStream.read(buffer)) != -1) {
				//Read source stream... 
			}
		} 
		
		digest = MD5Helper.convertToHexString(messageDigest.digest());
		
		for(int n=0; n < iterations - 1; iterations--)
			digest = MD5Helper.digest(digest, 1);
			
		return digest;
	}
	
	static private String convertToHexString(byte[] toConvert) {
		$eq($nN(toConvert, "Byte array to convert should not be null").length, 16, "Result length (in bytes) should be 16 instead of {}", toConvert.length);
		
		StringBuffer result = new StringBuffer();
		
		for (byte in : toConvert) {
			result.append(Integer.toString((in & 0xff) + 0x100, 16).substring(1));
		}
		
		$eq(result.length(), 32, "Converted result length should be 32 instead of {}", result.length());
		
		return result.toString();
	}
}
