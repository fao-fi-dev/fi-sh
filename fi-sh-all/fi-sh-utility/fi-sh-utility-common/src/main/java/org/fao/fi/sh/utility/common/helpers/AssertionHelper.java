/**
 * (c) 2015 FAO / UN (project: fis-utility-common)
 */
package org.fao.fi.sh.utility.common.helpers;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;

final public class AssertionHelper extends AbstractHelper {
	static private boolean ASSERTS_ARE_ENABLED = false;

	//Checks 'assert' availability at runtime (depends on the JVM -ea flag set at runtime)
	static {
		try {
			assert false : "Asserts are enabled";
		} catch (AssertionError Ae) {
			ASSERTS_ARE_ENABLED = true;
		}
	}

	/**
	 * Class constructor
	 *
	 */
	private AssertionHelper() {
	}
	
	/**
	 * Asserts true
	 *
	 * @param condition
	 * @param messageTemplate
	 * @param params
	 */
	static public void $true(boolean condition, String messageTemplate, Object... params) {
		$_assert(condition, IllegalArgumentException.class, messageTemplate, params);
	}

	/**
	 * Asserts false
	 *
	 * @param condition
	 * @param messageTemplate
	 * @param params
	 */
	static public void $false(boolean condition, String messageTemplate, Object... params) {
		$_nAssert(condition, IllegalArgumentException.class, messageTemplate, params);
	}

	/**
	 * Asserts non nullity
	 *
	 * @param nonNullable
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O $nN(O nonNullable, String messageTemplate, Object... params) {
		$_iArg(nonNullable != null, messageTemplate, params);
		
		return nonNullable;
	}
	
	/**
	 * Asserts nullity
	 *
	 * @param nullable
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O $n(O nullable, String messageTemplate, Object... params) {
		$_iArg(nullable == null, messageTemplate, params);
		
		return nullable;
	}

	/**
	 * Checks a map's emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param map
	 * @param messageTemplate
	 * @param params
	 */
	static public <K, V, M extends Map<K, V>> M $em(M map, String messageTemplate, Object... params) {
		$_iArg(map != null && map.isEmpty(), messageTemplate, params);
		
		return map;
	}

	/**
	 * Checks a collection's emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public <O, C extends Collection<O>> C $em(C collection, String messageTemplate, Object... params) {
		$_iArg(collection != null && collection.isEmpty(), messageTemplate, params);
		
		return collection;
	}

	/**
	 * Checks an array's emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param array
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O[] $em(O[] array, String messageTemplate, Object... params) {
		$_iArg(array != null && array.length == 0, messageTemplate, params);
		
		return array;
	}

	/**
	 * Checks a char sequence emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param sequence
	 * @param messageTemplate
	 * @param params
	 */
	static public <S extends CharSequence> S $em(S sequence, String messageTemplate, Object... params) {
		$_iArg(sequence != null && sequence.length() == 0, messageTemplate, params);
		
		return sequence;
	}

	/**
	 * Checks a map's null-ity or emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param map
	 * @param messageTemplate
	 * @param params
	 */
	static public <K, V, M extends Map<K, V>> M $emn(M map, String messageTemplate, Object... params) {
		$_iArg(map == null || map.isEmpty(), messageTemplate, params);
		
		return map;
	}

	/**
	 * Checks a collection's null-ity or emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param array
	 * @param messageTemplate
	 * @param params
	 */
	static public <O, C extends Collection<O>> C $emn(C collection, String messageTemplate, Object... params) {
		$_iArg(collection == null || collection.isEmpty(), messageTemplate, params);
		
		return collection;
	}

	/**
	 * Checks an array's null-ity or emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param array
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O[] $emn(O[] array, String messageTemplate, Object... params) {
		$_iArg(array == null || array.length == 0, messageTemplate, params);
		
		return array;
	}

	/**
	 * Checks a char sequence null-ity or emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param sequence
	 * @param messageTemplate
	 * @param params
	 */
	static public <S extends CharSequence> S $emn(S sequence, String messageTemplate, Object... params) {
		$_iArg(sequence == null || sequence.length() > 0, messageTemplate, params);
		
		return sequence;
	}

	/**
	 * Checks a map's non null-ity and non emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param map
	 * @param messageTemplate
	 * @param params
	 */
	static public <K, V, M extends Map<K, V>> M $nEm(M map, String messageTemplate, Object... params) {
		$_iArg(map != null && !map.isEmpty(), messageTemplate, params);
		
		return map;
	}

	/**
	 * Checks a collection's non null-ity and non emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public <O, C extends Collection<O>> C $nEm(C collection, String messageTemplate, Object... params) {
		$_iArg(collection != null && !collection.isEmpty(), messageTemplate, params);
		
		return collection;
	}

	/**
	 * Checks an array's non null-ity and non emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param array
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O[] $nEm(O[] array, String messageTemplate, Object... params) {
		$_iArg(array != null && array.length > 0, messageTemplate, params);
		
		return array;
	}

	/**
	 * Checks a char sequence non null-ity and non emptiness and throws an illegal argument exception if the check fails
	 *
	 * @param sequence
	 * @param messageTemplate
	 * @param params
	 */
	static public <S extends CharSequence> S $nEm(S sequence, String messageTemplate, Object... params) {
		$_iArg(sequence != null && sequence.length() > 0, messageTemplate, params);
		
		return sequence;
	}

	/**
	 * Checks the equality between two objects and throws an illegal argument exception if the check fails.
	 * It succeeds even if both objects are null.
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O $eq(O first, Object second, String messageTemplate, Object... params) {
		$_iArg((first == null && second == null) ||
			   (first != null && first.equals(second)), messageTemplate, params);
		
		return first;
	}
	
	/**
	 * Checks the non-equality between two objects and throws an illegal argument exception if the check fails.
	 * It fails even if both objects are null.
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public <O> O $nEq(O first, Object second, String messageTemplate, Object... params) {
		$_iArg((first != null && second == null) ||
			   (!first.equals(second)), messageTemplate, params);
		
		return first;
	}

	/**
	 * Checks that a number is zero and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N $z(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) == 0, messageTemplate, params);
		
		return number;
	}

	/**
	 * Checks that a number is not zero (or null) and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N $nZ(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) != 0, messageTemplate, params);
		
		return number;
	}

	/**
	 * Checks that a number is negative (and not null) and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N $neg(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) < 0, messageTemplate, params);
		
		return number;
	}

	/**
	 * Checks that a number is not negative (and not null) and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N  $nNeg(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) >= 0, messageTemplate, params);

		return number;
	}

	/**
	 * Checks that a number is positive (and not null) and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N  $pos(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) > 0, messageTemplate, params);

		return number;
	}

	/**
	 * Checks that a number is not positive (and not null) and throws an illegal argument exception if the check fails
	 * @param number
	 * @param messageTemplate
	 * @param params
	 */
	static public <N extends Number> N  $nPos(N number, String messageTemplate, Object... params) {
		$_iArg(number != null && Double.compare(number.doubleValue(), 0D) <= 0, messageTemplate, params);

		return number;
	}

	/**
	 * Checks that two numbers are one (the first) LTE to the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $lte(Number first, Number second, String messageTemplate, Object... params) {
		$_iArg(first != null && second != null &&
			   Double.compare(first.doubleValue(), second.doubleValue()) <= 0, messageTemplate, params);
	}

	/**
	 * Checks that two numbers are one (the first) LT the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $lt(Number first, Number second, String messageTemplate, Object... params) {
		$_iArg(first != null && second != null &&
			   Double.compare(first.doubleValue(), second.doubleValue()) < 0, messageTemplate, params);
	}

	/**
	 * Checks that two numbers are one (the first) GT the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $gt(Number first, Number second, String messageTemplate, Object... params) {
		$lt(second, first, messageTemplate, params);
	}

	/**
	 * Checks that two doubles are one (the first) GTE to the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $gte(Number first, Number second, String messageTemplate, Object... params) {
		$lte(second, first, messageTemplate, params);
	}
	
	/**
	 * Checks that two dates are one (the first) LTE to the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $lte(Date first, Date second, String messageTemplate, Object... params) {
		$_iArg(first != null && second != null &&
			   first.getTime() <= second.getTime(), messageTemplate, params);
	}

	/**
	 * Checks that two dates are one (the first) LT the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $lt(Date first, Date second, String messageTemplate, Object... params) {
		$_iArg(first != null && second != null &&
			   first.getTime() < second.getTime(), messageTemplate, params);
	}

	/**
	 * Checks that two dates are one (the first) GT the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $gt(Date first, Date second, String messageTemplate, Object... params) {
		$lt(second, first, messageTemplate, params);
	}

	/**
	 * Checks that two dates are one (the first) GTE to the other and throws an illegal argument exception if the check fails.
	 * @param first
	 * @param second
	 * @param messageTemplate
	 * @param params
	 */
	static public void $gte(Date first, Date second, String messageTemplate, Object... params) {
		$lte(second, first, messageTemplate, params);
	}

	/**
	 * Asserts an expression and throws an illegal argument exception if it is false
	 *
	 * @param condition
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_iArg(boolean condition, String messageTemplate, Object... params) {
		$_assert(condition, IllegalArgumentException.class, messageTemplate, params);
	}

	/**
	 * Asserts an expression and throws an unsupported operation exception if it is false
	 *
	 * @param condition
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_uOp(boolean condition, String messageTemplate, Object... params) {
		$_assert(condition, UnsupportedOperationException.class, messageTemplate, params);
	}

	/**
	 * @param messageTemplate
	 * @param params
	 * @return
	 */
	static public <O> O $_failRet(String messageTemplate, Object... params) {
		$_assert(false, AssertionError.class, messageTemplate, params);

		return null;
	}

	/**
	 * @param thrown
	 * @param messageTemplate
	 * @param params
	 * @return
	 * @throws T
	 */
	static public <O, T extends Throwable> O $_failRet(Class<T> thrown, String messageTemplate, Object... params) throws T {
		$_assert(false, thrown, messageTemplate, params);

		return null;
	}

	/**
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_fail(String messageTemplate, Object... params) {
		$_failRet(messageTemplate, params);
	}

	/**
	 * @param messageTemplate
	 * @param thrown
	 * @param params
	 * @throws T
	 */
	static public <T extends Throwable> void $_fail(String messageTemplate, Class<T> thrown, Object... params) throws T {
		$_failRet(thrown, messageTemplate, params);
	}

	/**
	 * Checks a condition and throws an exception if it is false.
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_assert(boolean condition, String messageTemplate, Object... params) {
		$_assert(condition, AssertionError.class, messageTemplate, params);
	}

	/**
	 * Checks a condition and throws an exception if it is true.
	 *
	 * @param collection
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_nAssert(boolean condition, String messageTemplate, Object... params) {
		$_assert(!condition, AssertionError.class, messageTemplate, params);
	}

	/**
	 * Checks a condition and throws an exception if it is true.
	 *
	 * @param collection
	 * @param uncheckedException
	 * @param messageTemplate
	 * @param params
	 */
	static public void $_nAssert(boolean condition, Class<? extends RuntimeException> uncheckedException, String messageTemplate, Object... params) {
		$_assert(!condition, uncheckedException, messageTemplate, params);
	}

	/**
	 * Checks a condition and throws a provided exception if it is false.
	 *
	 * @param collection
	 * @param thrownException
	 * @param messageTemplate
	 * @param thrownException
	 */
	static public <T extends Throwable> void $_assert(boolean condition, Class<T> thrownException, String messageTemplate, Object... params) throws T {
		String expandedMessage = null;

		//If assertions are enabled and the condition is not met, throws an AssertionError with the given message
		assert ASSERTS_ARE_ENABLED && condition : expandedMessage = $_expand(messageTemplate, params);

		//If assertions are disabled and the condition is not met, throws the specified runtime exception with the given message
		if(!ASSERTS_ARE_ENABLED && !condition) {
			if(thrownException != null) {
				Constructor<T> toThrow = null;

				expandedMessage = $_expand(messageTemplate, params);

				try {
					toThrow = ( thrownException.isAssignableFrom(AssertionError.class) )
							? thrownException.getConstructor(new Class<?>[] { Object.class })
							: thrownException.getConstructor(new Class<?>[] { String.class });
				} catch(NoSuchMethodException NSMe) {
					getLogger(AssertionHelper.class).warn("Provided exception class ({}) doesn't have a constructor with one argument of type java.lang.String: using default constructor instead", thrownException);
				}

				if(toThrow != null) {
					try {
						throw toThrow.newInstance(expandedMessage == null ? new Object[0] : new Object[] { expandedMessage });
					} catch(InvocationTargetException ITe) {
						getLogger(AssertionHelper.class).error("Unable to throw provided exception ({})", thrownException, ITe);
					} catch(IllegalAccessException IAe) {
						getLogger(AssertionHelper.class).error("Unable to throw provided exception ({})", thrownException, IAe);
					} catch(InstantiationException Ie) {
						getLogger(AssertionHelper.class).error("Unable to throw provided exception ({})", thrownException, Ie);
					}
				}
			}

			//If the provided exception couldn't be thrown, defaults to AssertionError...
			throw expandedMessage == null ? new AssertionError() : new AssertionError($_expand(messageTemplate, params));
		}
	}

	/**
	 * @param messageTemplate
	 * @param params
	 * @return
	 */
	static private String $_expand(String messageTemplate, Object... params) {
		if(messageTemplate == null || params == null || params.length == 0)
			return messageTemplate;

		String expanded = messageTemplate;

		for(Object param : params) {
			expanded = expanded.replaceFirst("\\{\\}", Matcher.quoteReplacement(param == null ? "<NULL>" : 
				param instanceof Date ? new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format((Date)param) : param.toString()));
		}

		return expanded;
	}
}
