/**
 * (c) 2015 FAO / UN (project: fis-utility-common)
 */
package org.fao.fi.sh.utility.common.helpers;

import java.lang.reflect.Field;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Mar 2015
 */
abstract public class AbstractHelper {
	/**
	 * Class constructor
	 */
	protected AbstractHelper() { 
	};
	
	/**
	 * @return a reference to the log provided by the delegate for a given class.
	 */
	static protected Logger getLogger(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz.getName());
	}

	/**
	 * @return a reference to the log provided by the delegate for a given category.
	 */
	static protected Logger getLogger(String category) {
		return LoggerFactory.getLogger(category);
	}
	
	static public Object coalesce(Object... objects) {
		for(Object in : objects)
			if(in != null)
				return in;
		
		return null;
	}
	
	static public boolean rawEquals(Object first, Object second) {
		return first == null ? second == null : first.equals(second);
	}
	
	static public List<Field> getAllFields(Class<?> clazz) {
		List<Field> allFields = new ArrayList<Field>();
		
		Class<?> current = clazz;
		
		do {
			for(Field in : current.getDeclaredFields())
				allFields.add(in);
			
			current = current.getSuperclass();
		} while(current != null);
		
		return allFields;
	}
	
	final static public Object fromValue(String type, String value) {
		if(value == null)
			return null;
		
		boolean isEmpty = "".equals(value.trim());
		
		try {
			switch(type) {
				case "Boolean": return isEmpty ? null : Boolean.parseBoolean(value);
				case "Integer": return isEmpty ? null : Integer.parseInt(value);
				case "Long": return isEmpty ? null : Long.parseLong(value);
				case "Float": return isEmpty ? null : Float.parseFloat(value);
				case "Double": return isEmpty ? null : Double.parseDouble(value);
				case "String": return value;
				case "Date" : return isEmpty ? null : new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(value);
				default:
					return value;
			}
		} catch(Throwable t) {
			throw new IllegalArgumentException("Cannot parse value " + value + " of type " + type + ": " + t.getMessage(), t);
		}
	}
	
	final static public String toValue(Object source) {
		if(source == null)
			return null;
		
		final Class<?> clazz = source.getClass();
		
		if(Date.class.isAssignableFrom(clazz))
			return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format((Date)source);
		
		return source.toString();
	}

	/**
	 * @return a reference to the log provided by the delegate.
	 */
	static protected Logger getLogger() {
		try {
			return AbstractHelper.getLogger(getInvokerClass());
		} catch(Throwable t) {
			return AbstractHelper.getLogger(AbstractHelper.class);	
		}
	}
	
	static protected String getInvokerClassName() {
		return Thread.currentThread().getStackTrace()[2].getClassName();
	}

	static protected Class<?> getInvokerClass() {
		final long now = System.currentTimeMillis();
		final String key = "getInvokerClass_" + now;

		try {
			throw new RuntimeException(key);
		} catch(RuntimeException Re) {
			StackTraceElement[] trace = Re.getStackTrace();

			final String className = trace[2].getClassName();

			try {
				return Class.forName(className);
			} catch(Throwable t) {
				return null;
			}
		}
	}
}
