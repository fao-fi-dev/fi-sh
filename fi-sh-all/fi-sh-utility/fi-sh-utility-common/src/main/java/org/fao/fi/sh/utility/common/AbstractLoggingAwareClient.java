/**
 * (c) 2015 FAO / UN (project: fis-utility-common)
 */
package org.fao.fi.sh.utility.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
abstract public class AbstractLoggingAwareClient {
	final protected Logger _log = LoggerFactory.getLogger(this.getClass());
}
