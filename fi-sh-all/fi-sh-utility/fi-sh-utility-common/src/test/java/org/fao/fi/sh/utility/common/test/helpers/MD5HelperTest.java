/**
 * (c) 2015 FAO / UN (project: fis-utility-common)
 */
package org.fao.fi.sh.utility.common.test.helpers;

import java.io.InputStream;

import org.fao.fi.sh.utility.common.helpers.MD5Helper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Apr 2015
 */
public class MD5HelperTest {
	@Test public void TestStringDigest() throws Throwable {
		String data = "FooBar";
		
		Assert.assertEquals("f32a26e2a3a8aa338cd77b6e1263c535", MD5Helper.digest(data));
	}
		
	@Test public void TestByteDigest() throws Throwable {
		String data = "FooBar";
		
		Assert.assertEquals(MD5Helper.digest(data), MD5Helper.digest(data.getBytes()));
	}
	
	@Test public void TestInputStreamDigest() throws Throwable {
		String data = "FooBar";
		
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("org/fao/fi/sh/utility/common/helpers/test/resources/FooBar.txt");
		
		Assert.assertNotNull(stream);
		
		Assert.assertEquals(MD5Helper.digest(data), MD5Helper.digest(stream));
	}
	
	@Test public void TestMultipleStringDigest() throws Throwable {
		String data = "FooBar";
		
		String digest;
		for(int n=1; n<=10; n++) {
			digest = MD5Helper.digest(data);
			
			for(int nn=1; nn<n; nn++)
				digest = MD5Helper.digest(digest);
			
			Assert.assertEquals(digest, MD5Helper.digest(data, n));
		}
	}
	
	@Test(expected=IllegalArgumentException.class) public void TestZeroIterationsStringDigest() throws Throwable {
		MD5Helper.digest("FooBar", 0);
	}
	
	@Test(expected=IllegalArgumentException.class) public void TestNegativeIterationsStringDigest() throws Throwable {
		MD5Helper.digest("FooBar", -1);
	}
}
