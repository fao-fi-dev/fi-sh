/**
 * (c) 2015 FAO / UN (project: fi-sh-utility-common)
 */
package org.fao.fi.sh.utility.common.test.helpers;

import java.io.InputStream;

import org.junit.Test;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2015
 */
public class AssertionHelperTest {
	private class Mock {
		public InputStream getInputStream() {
			return $_failRet("foo", "bar");
		}
	}
	
	@Test(expected=AssertionError.class) public void testTypeAdherence() {
		@SuppressWarnings("unused")
		Double foo = $_failRet("foo", "bar");
	}
	
	@Test(expected=AssertionError.class) public void testReturnTypeAdherence() {
		new Mock().getInputStream();
	}
}
