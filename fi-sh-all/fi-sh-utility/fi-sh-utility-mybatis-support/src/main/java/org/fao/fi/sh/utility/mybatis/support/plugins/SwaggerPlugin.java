/**
 * (c) 2015 FAO / UN (project: fis-utility-mybatis-support)
 */
package org.fao.fi.sh.utility.mybatis.support.plugins;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
public class SwaggerPlugin extends AbstractModelPlugin {
	public SwaggerPlugin() {
		super();
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return addSwaggerModelAnnotations(topLevelClass, introspectedTable);
	}

	protected boolean addSwaggerModelAnnotations(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType(new FullyQualifiedJavaType(ApiModel.class.getName()));
		
		if(hasNonStaticFields(topLevelClass)) topLevelClass.addImportedType(new FullyQualifiedJavaType(ApiModelProperty.class.getName()));
		
		topLevelClass.addAnnotation("@ApiModel(value=\"" + remarksFor(introspectedTable).replaceAll("\\\"", "\\\"") + "\")");

		int position = 1;
		
		for(Field in : topLevelClass.getFields()) {
			if(!in.isStatic()) {
				in.addAnnotation(
					"@ApiModelProperty(" + 
						"position=" + ( position++ ) + ", " +
						"required=" + !isNillable(introspectedTable, in) + ", " + 
						"value=\"" + remarksFor(introspectedTable, in) + 
					"\")");
			}
		}
		
		return true;
	}
}
