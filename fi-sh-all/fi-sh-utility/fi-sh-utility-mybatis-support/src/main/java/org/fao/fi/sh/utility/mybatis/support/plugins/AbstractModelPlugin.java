/**
 * (c) 2015 FAO / UN (project: fis-utility-mybatis-support)
 */
package org.fao.fi.sh.utility.mybatis.support.plugins;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.config.ModelType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Apr 2015
 */
abstract public class AbstractModelPlugin extends PluginAdapter {
	protected Map<String, Set<String>> NOT_NULLS = new HashMap<String, Set<String>>();
	protected Map<String, Map<String, String>> REMARKS = new HashMap<String, Map<String, String>>();
	protected Map<String, Map<String, String>> COLUMNS = new HashMap<String, Map<String, String>>();

	/**
	 * Class constructor
	 */
	public AbstractModelPlugin() {
		// TODO Auto-generated constructor block
	}
	
	public boolean validate(List<String> warnings) {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.mybatis.generator.api.PluginAdapter#initialized(org.mybatis.generator.api.IntrospectedTable)
	 */
	@Override
	public void initialized(IntrospectedTable introspectedTable) {
		super.initialized(introspectedTable); 
		
		this.buildTableMetadata(introspectedTable);
	}
	
	final protected boolean hasPrimaryKey(IntrospectedTable table) {
		boolean check = table.hasPrimaryKeyColumns();
		
		return check;
	}
	
	final protected boolean hasCompositePrimaryKey(IntrospectedTable table) {
		boolean check = hasPrimaryKey(table);
		
		check &= ModelType.HIERARCHICAL.equals(table.getTableConfiguration().getModelType());
		
		check &= table.getPrimaryKeyColumns().size() > 1;
		
		return check;
	}
	
	final protected boolean isIdentified(IntrospectedTable table) {
		return hasPrimaryKey(table) && !hasCompositePrimaryKey(table);
	}
	
	final protected boolean isKeyed(IntrospectedTable table) {
		return hasPrimaryKey(table) && hasCompositePrimaryKey(table);
	}
	
	final protected boolean isPlain(IntrospectedTable table) {
		return !hasPrimaryKey(table);
	}

	final protected void buildTableMetadata(IntrospectedTable introspectedTable) {
		Set<String> notNulls = NOT_NULLS.get(name(introspectedTable));
		Map<String, String> remarks = REMARKS.get(name(introspectedTable));
		Map<String, String> columns = COLUMNS.get(name(introspectedTable));
		
		if(notNulls == null) {
			notNulls = new HashSet<String>();
			NOT_NULLS.put(name(introspectedTable), notNulls);
		}
		
		if(remarks == null) {
			remarks = new HashMap<String, String>();
			REMARKS.put(name(introspectedTable), remarks);
		}
		
		if(columns == null) {
			columns = new HashMap<String, String>();
			COLUMNS.put(name(introspectedTable), columns);
		}
		
		for(IntrospectedColumn column : introspectedTable.getAllColumns()) {
			if(!column.isNullable())
				notNulls.add(column.getJavaProperty());
			
			if(column.getRemarks() != null && !"".equals(column.getRemarks().trim()))
				remarks.put(column.getJavaProperty(), column.getRemarks().trim());
			
			columns.put(column.getJavaProperty(), column.getActualColumnName());
		}
	}
	
	final protected String name(IntrospectedTable introspectedTable) {
		return introspectedTable.getFullyQualifiedTable().getIntrospectedTableName();
	}
	
	final protected String remarksFor(IntrospectedTable introspectedTable) {
		return "Models the '" + name(introspectedTable) + "' table"; //No way, so far, to get the table comment (as available in MySQL, for instance)
	}
	
	final protected String remarksFor(IntrospectedTable introspectedTable, Field field) {
		return remarksFor(introspectedTable, field.getName());
	}
	
	final protected String remarksFor(IntrospectedTable introspectedTable, String fieldName) {
		String remarks = REMARKS.get(name(introspectedTable)).get(fieldName);
		
		if(remarks == null)
			remarks = "Models the '" + columnNameFor(introspectedTable, fieldName) + "' column";
		
		return remarks.replaceAll("\\\"", "\\\"");
	}
	
	final protected String columnNameFor(IntrospectedTable introspectedTable, Field field) {
		return columnNameFor(introspectedTable, field.getName());
	}
	
	final protected String columnNameFor(IntrospectedTable introspectedTable, String fieldName) {
		return COLUMNS.get(name(introspectedTable)).get(fieldName);
	}
	
	final protected boolean isNillable(IntrospectedTable introspectedTable, String fieldName) {
		return !NOT_NULLS.get(name(introspectedTable)).contains(fieldName);
	}
	
	final protected boolean isNillable(IntrospectedTable introspectedTable, Field field) {
		return !NOT_NULLS.get(name(introspectedTable)).contains(field.getName());
	}
	
	final protected boolean hasNonStaticFields(TopLevelClass clazz) {
		List<Field> fields = clazz.getFields();
		
		if(fields == null || fields.isEmpty())
			return false;
		
		int numTotal = fields.size(), numStatic = 0;
		
		for(Field in : fields)
			if(in.isStatic()) numStatic++;
		
		return numTotal - numStatic > 0;
	}
}
