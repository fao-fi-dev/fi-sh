/**
 * (c) 2015 FAO / UN (project: fis-utility-mybatis-support)
 */
package org.fao.fi.sh.utility.mybatis.support.plugins;

import org.fao.fi.sh.model.core.annotations.ModelMetadata;
import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.spi.persistence.Data;
import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedData;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedMapper;
import org.fao.fi.sh.model.core.spi.persistence.KeyedData;
import org.fao.fi.sh.model.core.spi.persistence.KeyedMapper;
import org.fao.fi.sh.model.core.spi.persistence.Mapper;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
public class EnhancedModelPlugin extends AbstractModelPlugin {
	public EnhancedModelPlugin() {
		super();
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType(ModelMetadata.class.getName());
		
		if(hasNonStaticFields(topLevelClass)) topLevelClass.addImportedType(ModelPropertyMetadata.class.getName());
		
		topLevelClass.addImportedType(isKeyed(introspectedTable) ? 
										KeyedData.class.getName() : 
											isIdentified(introspectedTable) ? 
												IdentifiedData.class.getName() : 
													Data.class.getName());
		
		topLevelClass.addSuperInterface(
			new FullyQualifiedJavaType(
					isKeyed(introspectedTable) ?
						KeyedData.class.getName() + "<" + introspectedTable.getPrimaryKeyType() + ">" :
							isIdentified(introspectedTable) ?
								IdentifiedData.class.getName() + "<" + introspectedTable.getPrimaryKeyColumns().get(0).getFullyQualifiedJavaType() + ">" :
									Data.class.getName()
			)
		);
		
		topLevelClass.addAnnotation("@ModelMetadata(name=\"" + topLevelClass.getType().getShortName() + "\", description=\"" + remarksFor(introspectedTable) + "\")");
		
		for(Field in : topLevelClass.getFields()) {
			if(!in.isStatic()) {
				in.addAnnotation(
					"@ModelPropertyMetadata(" + 
						"isKey=" + ( !introspectedTable.getPrimaryKeyColumns().isEmpty() && introspectedTable.getPrimaryKeyColumns().get(0).getJavaProperty().equals(in.getName())) + ", " +
						"name=\"" + in.getName() + "\", " +
						"type=\"" + in.getType().getShortName() + "\", " +  
						"isNullable=" + isNillable(introspectedTable, in) + ", " +
						"description=\"" + remarksFor(introspectedTable, in) +
					"\")");
			}
		}
		
		return true;
	}

	@Override
	public boolean modelPrimaryKeyClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.setAbstract(true);
		
		return true; 
	}
	
	/* (non-Javadoc)
	 * @see org.mybatis.generator.api.PluginAdapter#modelExampleClassGenerated(org.mybatis.generator.api.dom.java.TopLevelClass, org.mybatis.generator.api.IntrospectedTable)
	 */
	@Override
	public boolean modelExampleClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType(Example.class.getName());
		topLevelClass.addSuperInterface(new FullyQualifiedJavaType(Example.class.getName() + "<" + topLevelClass.getType().getFullyQualifiedName().replaceAll("Example$", "") + ">"));
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.mybatis.generator.api.PluginAdapter#clientGenerated(org.mybatis.generator.api.dom.java.Interface, org.mybatis.generator.api.dom.java.TopLevelClass, org.mybatis.generator.api.IntrospectedTable)
	 */
	@Override
	public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		interfaze.addImportedType(
			new FullyQualifiedJavaType(
				( isKeyed(introspectedTable) ?
					KeyedMapper.class : 
						isIdentified(introspectedTable) ?
							IdentifiedMapper.class :
								Mapper.class
				).getName()
			)
		);
		
		if(hasPrimaryKey(introspectedTable)) {
			interfaze.addSuperInterface(
				new FullyQualifiedJavaType((
					isKeyed(introspectedTable) ?
						KeyedMapper.class.getName() + "<" + introspectedTable.getPrimaryKeyType() :
						IdentifiedMapper.class.getName() + "<" + introspectedTable.getPrimaryKeyColumns().get(0).getFullyQualifiedJavaType() 
					) + ", " +
					interfaze.getType().getFullyQualifiedName().replaceAll("Mapper$", "") + ", " + 
					interfaze.getType().getFullyQualifiedName().replaceAll("Mapper$", "Example") + 
				">")
			);
		} else {
			interfaze.addSuperInterface(new FullyQualifiedJavaType(
				Mapper.class.getName() + "<" +
					interfaze.getType().getFullyQualifiedName().replaceAll("Mapper$", "") + ", " + 
					interfaze.getType().getFullyQualifiedName().replaceAll("Mapper$", "Example") + 
				">")
			);
		}

		return true;
	}
}
