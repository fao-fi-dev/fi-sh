/**
 * (c) 2015 FAO / UN (project: fis-utility-mybatis-support)
 */
package org.fao.fi.sh.utility.mybatis.support.plugins;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.TopLevelClass;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
public class JAXBPlugin extends AbstractModelPlugin {
	public JAXBPlugin() {
		super();
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return makeJAXBAware(topLevelClass, introspectedTable);
	}

	protected boolean makeJAXBAware(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		topLevelClass.addImportedType(new FullyQualifiedJavaType(XmlRootElement.class.getName()));
		topLevelClass.addImportedType(new FullyQualifiedJavaType(XmlAccessorType.class.getName()));
		topLevelClass.addImportedType(new FullyQualifiedJavaType(XmlAccessType.class.getName()));
		
		if(hasNonStaticFields(topLevelClass)) topLevelClass.addImportedType(new FullyQualifiedJavaType(XmlElement.class.getName()));
		
		topLevelClass.addAnnotation("@XmlRootElement(name=\"" + topLevelClass.getType().getShortName() + "\")");
		topLevelClass.addAnnotation("@XmlAccessorType(XmlAccessType.FIELD)");
		
		for(Field in : topLevelClass.getFields()) {
			if(!in.isStatic()) {
				in.addAnnotation("@XmlElement(" + 
					"name=\"" + in.getName() + "\", " + 
					"required=" + !isNillable(introspectedTable, in) + ", " + 
					"nillable=" + isNillable(introspectedTable, in) + 
				")");
			}
		}
		
		return true;
	}
}