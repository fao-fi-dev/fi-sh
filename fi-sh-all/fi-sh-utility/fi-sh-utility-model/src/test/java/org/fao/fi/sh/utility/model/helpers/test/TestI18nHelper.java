/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.utility.model.helpers.test;

import java.util.Arrays;
import java.util.List;

import org.fao.fi.sh.model.core.spi.Localized;
import org.fao.fi.sh.model.core.spi.Localizer;
import org.fao.fi.sh.utility.model.helpers.I18nHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
public class TestI18nHelper {
	private class LocalizerMock implements Localizer<Integer> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -3917313523100034211L;
		
		private Integer clId;
		private String i18nLangId, i18nCountryId, name, description;
		
		/**
		 * Class constructor
		 *
		 * @param parentId
		 * @param sysLangI18nId
		 * @param sysCountryI18nId
		 * @param name
		 * @param description
		 */
		public LocalizerMock(Integer clId, String i18nLangId, String i18nCountryId, String name, String description) {
			super();
			this.clId = clId;
			this.i18nLangId = i18nLangId;
			this.i18nCountryId = i18nCountryId;
			this.name = name;
			this.description = description;
		}

		/**
		 * @return the 'parentId' value
		 */
		public Integer getParentId() {
			return this.clId;
		}

		/**
		 * @param parentId the 'parentId' value to set
		 */
		public void setParentId(Integer clId) {
			this.clId = clId;
		}

		/**
		 * @return the 'sysLangI18nId' value
		 */
		public String getSysLangI18nId() {
			return this.i18nLangId;
		}

		/**
		 * @param sysLangI18nId the 'sysLangI18nId' value to set
		 */
		public void setSysLangI18nId(String i18nLangId) {
			this.i18nLangId = i18nLangId;
		}

		/**
		 * @return the 'sysCountryI18nId' value
		 */
		public String getSysCountryI18nId() {
			return this.i18nCountryId;
		}

		/**
		 * @param sysCountryI18nId the 'sysCountryI18nId' value to set
		 */
		public void setSysCountryI18nId(String i18nCountryId) {
			this.i18nCountryId = i18nCountryId;
		}

		/**
		 * @return the 'name' value
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * @param name the 'name' value to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the 'description' value
		 */
		public String getDescription() {
			return this.description;
		}

		/**
		 * @param description the 'description' value to set
		 */
		public void setDescription(String description) {
			this.description = description;
		}
	}
	
	private class LocalizedMock implements Localized<LocalizerMock> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 323527049566232564L;
		
		private String _name;
		private String _description;
		private List<LocalizerMock> _localization;
		
		/**
		 * Class constructor
		 *
		 * @param name
		 * @param description
		 * @param localization
		 */
		public LocalizedMock(String name, String description, List<LocalizerMock> localization) {
			super();
			this._name = name;
			this._description = description;
			this._localization = localization;
		}

		/**
		 * @return the 'name' value
		 */
		public String getName() {
			return this._name;
		}

		/**
		 * @param name the 'name' value to set
		 */
		public void setName(String name) {
			this._name = name;
		}

		/**
		 * @return the 'description' value
		 */
		public String getDescription() {
			return this._description;
		}

		/**
		 * @param description the 'description' value to set
		 */
		public void setDescription(String description) {
			this._description = description;
		}

		/**
		 * @return the 'localization' value
		 */
		public List<LocalizerMock> getLocalization() {
			return this._localization;
		}

		/**
		 * @param localization the 'localization' value to set
		 */
		public void setLocalization(List<LocalizerMock> localization) {
			this._localization = localization;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ( ( this._description == null ) ? 0 : this._description.hashCode() );
			result = prime * result + ( ( this._localization == null ) ? 0 : this._localization.hashCode() );
			result = prime * result + ( ( this._name == null ) ? 0 : this._name.hashCode() );
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if(this == obj) return true;
			if(obj == null) return false;
			if(getClass() != obj.getClass()) return false;
			LocalizedMock other = (LocalizedMock) obj;
			if(!getOuterType().equals(other.getOuterType())) return false;
			if(this._description == null) {
				if(other._description != null) return false;
			} else if(!this._description.equals(other._description)) return false;
			if(this._localization == null) {
				if(other._localization != null) return false;
			} else if(!this._localization.equals(other._localization)) return false;
			if(this._name == null) {
				if(other._name != null) return false;
			} else if(!this._name.equals(other._name)) return false;
			return true;
		}

		private TestI18nHelper getOuterType() {
			return TestI18nHelper.this;
		}
	}
	
	private List<LocalizerMock> getMockLocalizers() {
		return Arrays.asList(new LocalizerMock[] { 
			new LocalizerMock(1, "es", null, "name_es", "description_es"),
			new LocalizerMock(1, "en", null, "name_en", "description_en"),
			new LocalizerMock(1, "en", "bs", "name_en_bs", "description_en_bs"),
			new LocalizerMock(1, "en", "us", "name_en_us", "description_en_us"),
			new LocalizerMock(1, "fr", "ca", "name_fr_ca", "description_fr_ca"),
			new LocalizerMock(1, "fr", "be", "name_fr_be", "description_fr_be"),
		});
	}
	
	private LocalizedMock getLocalizedMock() {
		return new LocalizedMock("name_default", "description_default", this.getMockLocalizers());
	}
	
	@Test
	public void testWithMissingLanguage() {
		Assert.assertEquals("name_default", I18nHelper.name(getLocalizedMock(), "it"));
		Assert.assertEquals("description_default", I18nHelper.description(getLocalizedMock(), "it"));
	}
	
	@Test
	public void testWithMissingLanguageAndCountry() {
		Assert.assertEquals("name_default", I18nHelper.name(getLocalizedMock(), "it", "it"));
		Assert.assertEquals("description_default", I18nHelper.description(getLocalizedMock(), "it", "it"));
	}
	
	@Test
	public void testWithLanguage() {
		Assert.assertEquals("name_en", I18nHelper.name(getLocalizedMock(), "en"));
		Assert.assertEquals("description_en", I18nHelper.description(getLocalizedMock(), "en"));
	}
	
	@Test
	public void testWithLanguageAndCountry() {
		Assert.assertEquals("name_en_us", I18nHelper.name(getLocalizedMock(), "en", "us"));
		Assert.assertEquals("description_en_us", I18nHelper.description(getLocalizedMock(), "en", "us"));
	}
	
	@Test
	public void testWithLanguageAndMissingCountry() {
		Assert.assertEquals("name_en", I18nHelper.name(getLocalizedMock(), "en", "uk"));
		Assert.assertEquals("description_en", I18nHelper.description(getLocalizedMock(), "en", "uk"));
	}
	
	@Test
	public void testWithLanguageAndMissingCountryButNoLanguageOnly() {
		Assert.assertEquals("name_fr_be", I18nHelper.name(getLocalizedMock(), "fr", "ch"));
		Assert.assertEquals("description_fr_be", I18nHelper.description(getLocalizedMock(), "fr", "ch"));
	}
}
