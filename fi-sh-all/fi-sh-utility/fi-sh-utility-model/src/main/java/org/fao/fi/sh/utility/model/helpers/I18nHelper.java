/**
 * (c) 2015 FAO / UN (project: fis-utility-model)
 */
package org.fao.fi.sh.utility.model.helpers;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.util.List;
import java.util.stream.Collectors;

import org.fao.fi.sh.model.core.spi.Localized;
import org.fao.fi.sh.model.core.spi.Localizer;
import org.fao.fi.sh.utility.common.helpers.AbstractHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
final public class I18nHelper extends AbstractHelper {
	/**
	 * Class constructor
	 *
	 */
	private I18nHelper() { }
	
	/**
	 * @param entity
	 * @param language
	 * @param country
	 * @return
	 */
	static public <LR extends Localizer<?>, LD extends Localized<LR>> String name(LD entity, String language, String country) {
		LR localizer = localize(entity, language, country);
		
		return ( localizer == null ? entity : localizer ).getName();
	}
	
	/**
	 * @param entity
	 * @param language
	 * @return
	 */
	static public <LR extends Localizer<?>, LD extends Localized<LR>> String name(LD entity, String language) {
		return I18nHelper.name(entity, language, null);
	}
	
	/**
	 * @param entity
	 * @param language
	 * @param country
	 * @return
	 */
	static public <LR extends Localizer<?>, LD extends Localized<LR>> String description(LD entity, String language, String country) {
		LR localizer = localize(entity, language, country);
		
		return ( localizer == null ? entity : localizer ).getDescription();
	}
	
	/**
	 * @param entity
	 * @param language
	 * @return
	 */
	static public <LR extends Localizer<?>, LD extends Localized<LR>> String description(LD entity, String language) {
		return I18nHelper.description(entity, language, null);
	}
	
	/**
	 * @param entity
	 * @param language
	 * @param country
	 * @return
	 */
	static private <LR extends Localizer<?>, LD extends Localized<LR>> LR localize(LD entity, String language, String country) {
		$nN(entity, "The entity to localize cannot be null");
		$nN(language, "Localization language cannot be null");
		
		List<LR> byLanguage = 
			entity.getLocalization().
				stream().
					filter(L -> language.equals(L.getSysLangI18nId())).
						sorted((L1, L2) -> L1.getSysCountryI18nId() == null ? -1 : L2.getSysCountryI18nId() == null ? 1 : L1.getSysCountryI18nId().compareTo(L2.getSysCountryI18nId())).
							collect(Collectors.toList());
		
		List<LR> byCountry = 
			byLanguage.
				stream().
					filter(L -> country == null || country.equals(L.getSysCountryI18nId())).
						collect(Collectors.toList());
		
		LR l = byLanguage.isEmpty() ? null : byLanguage.get(0);
		LR c = byCountry.isEmpty() ? null : byCountry.get(0);
		
		return ( country == null || c == null ) ? l : c;
	}
}
