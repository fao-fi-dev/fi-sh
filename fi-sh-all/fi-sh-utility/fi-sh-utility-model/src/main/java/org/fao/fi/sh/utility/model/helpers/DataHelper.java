/**
 * (c) 2015 FAO / UN (project: fis-utility-model)
 */
package org.fao.fi.sh.utility.model.helpers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.basic.data.meta.ValuedDataModelProperty;
import org.fao.fi.sh.utility.common.helpers.AbstractHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
final public class DataHelper extends AbstractHelper {
	private DataHelper() {
		super();
	}
	
	final static public <O> O fromProperties(O target, List<ValuedDataModelProperty> properties) throws InvocationTargetException, IllegalAccessException {
		for(ValuedDataModelProperty in : properties) {
			BeanUtils.setProperty(target, in.getName(), AbstractHelper.fromValue(in.getType(), in.getValue()));
		}
		
		return target;
	}
	
	final static public List<ValuedDataModelProperty> toProperties(Object target) throws InvocationTargetException, IllegalAccessException {
		if(target == null)
			return null;
		
		List<ValuedDataModelProperty> properties = new ArrayList<ValuedDataModelProperty>();
		
		Class<?> current = target.getClass();
		
		ModelPropertyMetadata meta;
		do {
			for(Field in : current.getDeclaredFields()) {
				in.setAccessible(true);
				
				if((in.getModifiers() & Modifier.STATIC) != Modifier.STATIC) {
					if(in.isAnnotationPresent(ModelPropertyMetadata.class)) {
						meta = in.getAnnotation(ModelPropertyMetadata.class);
						
						properties.add(new ValuedDataModelProperty(
							AbstractHelper.toValue(in.get(target)),
							meta.name(),
							meta.type(),
							meta.description(),
							meta.isKey(),
							meta.isNullable()
						));
					}
				}
			}
			
			current = current.getSuperclass();
		} while(current != null);
		
		return properties;
	}
}
