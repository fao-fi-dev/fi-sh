/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.KeyedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Apr 2015
 */
public interface KeyedDataUpdateAccessor<ID extends Serializable, RAW extends KeyedData<ID>, DATA extends RAW, FILTER extends Example<RAW>> 
		 extends DataDeleteAccessor<RAW, DATA, FILTER> {
	RAW updateRaw(RAW updated, boolean selective) throws Exception;
	
	/**
	 * @param updated the data to update
	 * @param selective whether the update must be selective or not
	 * @return the provided data after it has been updated (and re-read from the database)
	 * @throws Exception
	 */
	DATA update(DATA updated, boolean selective) throws Exception;
}
