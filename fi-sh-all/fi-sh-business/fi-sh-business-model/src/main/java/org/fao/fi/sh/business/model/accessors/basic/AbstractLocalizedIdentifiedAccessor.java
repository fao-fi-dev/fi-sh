/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors.basic;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Localizable;
import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedData;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedMapper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2015
 */
abstract public class AbstractLocalizedIdentifiedAccessor<ID extends Serializable, RAW extends IdentifiedData<ID> & Localizable, DATA extends RAW, FILTER extends Example<RAW>, MAPPER extends IdentifiedMapper<ID, RAW, FILTER>> 
					  extends AbstractIdentifiedAccessor<ID, RAW, DATA, FILTER, MAPPER> {
	
	abstract public DATA localize(DATA data) throws Exception;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.basic.AbstractAccessor#fromRaw(org.fao.fi.sh.model.core.spi.persistence.Data)
	 */
	@Override
	public DATA fromRaw(RAW data) throws Exception {
		DATA wide = super.fromRaw(data);
		
		if(wide == null)
			return null;
		
		return localize(wide);
	}
}