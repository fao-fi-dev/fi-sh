/**
 * (c) 2015 FAO / UN (project: fmis-business-processes)
 */
package org.fao.fi.sh.business.model.mappers.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
public class MappingMaterializationException extends MappingException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -632877998976175580L;

	/**
	 * Class constructor
	 *
	 */
	public MappingMaterializationException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MappingMaterializationException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MappingMaterializationException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MappingMaterializationException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}

}
