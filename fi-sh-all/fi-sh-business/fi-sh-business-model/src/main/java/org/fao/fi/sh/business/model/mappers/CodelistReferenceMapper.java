/**
 * (c) 2015 FAO / UN (project: fmis-business-processes)
 */
package org.fao.fi.sh.business.model.mappers;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.List;

import org.fao.fi.sh.business.model.mappers.exceptions.MappingDataMaterializationException;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingException;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingParsingException;
import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.basic.data.mappings.Mapping;
import org.fao.fi.sh.model.core.basic.data.mappings.MappingTarget;
import org.fao.fi.sh.model.core.spi.Mapped;
import org.w3c.dom.Document;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
public interface CodelistReferenceMapper<ID extends Serializable, CODELIST extends CoreCodelist<ID, ?> & Mapped, REFERENCE, MAPPING_TARGET extends MappingTarget<REFERENCE>, MAPPING extends Mapping<ID, CODELIST, REFERENCE, MAPPING_TARGET>> {
	Document loadMappings() throws MappingDataMaterializationException, MappingParsingException;
	Document loadMappings(String xml) throws MappingDataMaterializationException, MappingParsingException;
	Document loadMappings(File file) throws MappingDataMaterializationException, MappingParsingException;
	Document loadMappings(URL url) throws MappingDataMaterializationException, MappingParsingException;
	Document loadMappings(InputStream stream) throws MappingDataMaterializationException, MappingParsingException;
	
	MAPPING getMappingsFor(CODELIST data) throws MappingException;

	List<MAPPING> getAllMappings() throws MappingException;
}