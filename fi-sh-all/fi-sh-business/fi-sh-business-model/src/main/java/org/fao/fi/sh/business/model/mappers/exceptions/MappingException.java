/**
 * (c) 2015 FAO / UN (project: fmis-business-processes)
 */
package org.fao.fi.sh.business.model.mappers.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
public class MappingException extends Exception {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3167059684882092971L;

	/**
	 * Class constructor
	 *
	 */
	public MappingException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MappingException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MappingException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MappingException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}
