/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors.basic;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$eq;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.io.Serializable;

import org.fao.fi.sh.business.model.accessors.KeyedAccessor;
import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.KeyedData;
import org.fao.fi.sh.model.core.spi.persistence.KeyedMapper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2015
 */
abstract public class AbstractKeyedAccessor<ID extends Serializable, RAW extends KeyedData<ID>, DATA extends RAW, FILTER extends Example<RAW>, MAPPER extends KeyedMapper<ID, RAW, FILTER>> 
					  extends AbstractAccessor<RAW, DATA, FILTER, MAPPER> 
					  implements KeyedAccessor<ID, RAW, DATA, FILTER> {
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.KeyedAccessor#getById(java.io.Serializable)
	 */
	public DATA byKey(ID id) throws Exception {
		return fromRaw(mapper().selectByPrimaryKey(id)); 
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.KeyedDataDeleteAccessor#delete(java.io.Serializable)
	 */
	@Override
	public int delete(ID id) throws Exception {
		int numDeleted = mapper().deleteByPrimaryKey($nN(id, "Please provide a non-null key"));
		
		return $eq(numDeleted, 1, "An attempt to delete a data by key resulted in {} records being updated (expected: 1)", numDeleted);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.KeyedDataUpdateAccessor#updateRaw(org.fao.fi.sh.model.core.spi.persistence.KeyedData, boolean)
	 */
	@Override
	public RAW updateRaw(RAW updated, boolean selective) throws Exception {
		$nN(mapper().selectByPrimaryKey(key($nN(updated, "Please provide a non-null data to update"))), "Provided data key cannot identify any data");

		updated = checkRawConformity(updated, selective);
		
		int numUpdated = selective ? mapper().updateByPrimaryKeySelective(updated) : mapper().updateByPrimaryKey(updated);
		
		$eq(numUpdated, 1, "An attempt to update a data resulted in {} records being updated (expected: 1)", numUpdated);
		
		return mapper().selectByPrimaryKey(key(updated));
	}

	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.KeyedDataUpdateAccessor#update(org.fao.fi.sh.model.core.spi.persistence.KeyedData, boolean)
	 */
	@Override
	public DATA update(DATA updated, boolean selective) throws Exception {
		this.updateRaw(toRaw(checkConformity(updated, selective)), selective);
		
		this.doUpdate(updated, selective);
		
		return byKey(key(toRaw(updated)));
	}
	
	abstract protected void doUpdate(DATA updated, boolean selective) throws Exception;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.DataDeleteAccessor#deleteRaw(org.fao.fi.sh.model.core.spi.persistence.Data)
	 */
	@Override
	public int deleteRaw(RAW toDelete) throws Exception {
		return mapper().deleteByPrimaryKey(
			$nN(
				key(
					checkRawConformity($nN(toDelete, "Please provide a non-null data to delete"), false)
				), "Please provide a data with a non-null key to delete"
			)
		);
	}
}