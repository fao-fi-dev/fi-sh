/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.mappers.basic;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.fao.fi.sh.business.model.mappers.CodelistReferenceMapper;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingDataMaterializationException;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingException;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingMaterializationException;
import org.fao.fi.sh.business.model.mappers.exceptions.MappingParsingException;
import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.basic.data.mappings.Mapping;
import org.fao.fi.sh.model.core.basic.data.mappings.MappingTarget;
import org.fao.fi.sh.model.core.spi.Mapped;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
abstract public class AbstractReferenceMapper<ID extends Serializable, CODELIST extends CoreCodelist<ID, ?> & Mapped, REFERENCE, MAPPING_TARGET extends MappingTarget<REFERENCE>, MAPPING extends Mapping<ID, CODELIST, REFERENCE, MAPPING_TARGET>> 
					  extends AbstractLoggingAwareClient 
					  implements CodelistReferenceMapper<ID, CODELIST, REFERENCE, MAPPING_TARGET, MAPPING> {
	
	protected DocumentBuilderFactory DOCUMENT_BUILDER_FACTORY;
	protected DocumentBuilder DOCUMENT_BUILDER;
	
	protected Document _mappings;
	
	/**
	 * Class constructor
	 *
	 */
	public AbstractReferenceMapper() {
		try {
			DOCUMENT_BUILDER_FACTORY = DocumentBuilderFactory.newInstance();
			DOCUMENT_BUILDER = DOCUMENT_BUILDER_FACTORY.newDocumentBuilder();
		} catch (Throwable t) {
			_log.error("Unable to initialize XML document builder", t);
		}
	}
	
	abstract protected InputStream defaultResource() throws Exception;
	
	final public Document loadMappings() throws MappingDataMaterializationException {
		try {
			return loadMappings(defaultResource());
		} catch(Exception e) {
			throw new MappingDataMaterializationException(e.getMessage(), e);
		}
	}
	
	final public Document loadMappings(InputStream resource) throws MappingDataMaterializationException, MappingParsingException {
		try {
			return ( _mappings = parseMappings(resource) );
		} catch(Exception e) {
			throw new MappingDataMaterializationException(e.getMessage(), e);
		}
	}
	
	final public Document loadMappings(String xml) throws MappingDataMaterializationException, MappingParsingException {
		try(InputStream stream = new ByteArrayInputStream($nN(xml, "Please provide a non-null XML string").getBytes("UTF-8"))) {
			return this.loadMappings(stream);
		} catch(IOException e) {
			throw new MappingParsingException("Unable to parse mapping XML from String resource: " + e.getMessage(), e);
		}
	}
	
	final public Document loadMappings(URL url) throws MappingDataMaterializationException, MappingParsingException {
		try(InputStream stream = $nN(url, "Please provide a non-null URL").openConnection().getInputStream()) {
			return this.loadMappings(stream);
		} catch(IOException e) {
			throw new MappingParsingException("Unable to parse mapping XML from URL resource: " + e.getMessage(), e);
		}
	}

	final public Document loadMappings(File file) throws MappingDataMaterializationException, MappingParsingException {
		try(FileInputStream stream = new FileInputStream($nN(file, "Please provide a non-null file"))) {
			return this.loadMappings(stream);
		} catch(IOException e) {
			throw new MappingParsingException("Unable to parse mapping XML from file resource: " + e.getMessage(), e);
		}
	}
	
	final protected Document parseMappings(InputStream stream) throws MappingParsingException {
		try {
			return DOCUMENT_BUILDER.parse($nN(stream, "Please provide a non-null stream"));
		} catch(SAXException|IOException e) {
			throw new MappingParsingException("Unable to parse mapping XML from resource: " + e.getMessage(), e);
		}
	}

	final protected Document getMappingsDocument() throws MappingDataMaterializationException {
		try {
			return ( _mappings = _mappings == null ? loadMappings(defaultResource()) : _mappings );
		} catch(Exception e) {
			throw new MappingDataMaterializationException(e.getMessage(), e);
		}
	}
	
	abstract protected CODELIST sourceFrom(Element sourceElement) throws MappingDataMaterializationException;
	abstract protected REFERENCE targetFrom(Element targetElement) throws MappingDataMaterializationException;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.processes.data.mappings.spi.ReferenceMapper#getAllMappings()
	 */
	@Override
	final public List<MAPPING> getAllMappings() throws MappingException {
		return getAllMappings(getMappingsDocument()); 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.processes.data.mappings.spi.ReferenceMapper#getMappingsFor(org.fao.fi.sh.model.core.basic.composite.MappedCodelist)
	 */
	@Override
	final public MAPPING getMappingsFor(CODELIST data) throws MappingException {
		return getMappingsFor(getMappingsDocument(), data);
	}
	
	final protected List<MAPPING> retrieveAllMappings(Document mappingDocument) throws MappingMaterializationException {
		$nN(mappingDocument, "Please provide a non-null mapping document");
		
		try {
			String version, sourceDataSource, targetDataSource;
			
			version = ((Element)mappingDocument.getFirstChild()).getAttribute("version");
			
			Element sourceProviderElement = ((Element)mappingDocument.getElementsByTagName("SourceDataProvider").item(0));
			Element targetProviderElement = ((Element)mappingDocument.getElementsByTagName("TargetDataProvider").item(0));
			
			sourceDataSource = sourceProviderElement.getAttribute("providerId") + "/" + sourceProviderElement.getAttribute("dataSourceId").replace("urn:", "");
			targetDataSource = targetProviderElement.getAttribute("providerId") + "/" + targetProviderElement.getAttribute("dataSourceId").replace("urn:", "");
					
			NodeList mappings = mappingDocument.getElementsByTagName("Mapping");
			
			Element mapping, sourceElement, targetElement, mappingDetail;
			NodeList mappingDetails; 
			
			MAPPING currentMapping;
			MAPPING_TARGET currentMappingTarget;
			
			List<MAPPING> toReturn = new ArrayList<MAPPING>();
			
			for(int m=0; m<mappings.getLength(); m++) {
				mapping = (Element)mappings.item(m);
				
				sourceElement = (Element)mapping.getElementsByTagName("SourceElement").item(0); //<SourceElement>

				currentMapping = createMappingFor(sourceFrom(sourceElement), sourceDataSource, version);
				
				mappingDetails = mapping.getElementsByTagName("MappingDetail"); 
				
				for(int d=0; d<mappingDetails.getLength(); d++) {
					mappingDetail = (Element)mappingDetails.item(d);
					
					//TODO: IMPROVE!!!
					targetElement = (Element)mappingDetail.getElementsByTagName("TargetElement").item(0);
					
					currentMappingTarget = createMappingTargetFor(targetFrom(targetElement), targetDataSource, version, Double.parseDouble(mappingDetail.getAttribute("score")));
					
					currentMapping.getTargets().add(currentMappingTarget);
				}
				
				toReturn.add(currentMapping);
			}
			
			return toReturn;
		} catch(Throwable t) {
			throw new MappingMaterializationException(t.getMessage(), t); 
		}
	}
	
	abstract protected MAPPING createMappingFor(CODELIST entry, String sourceProvider, String version);
	abstract protected MAPPING_TARGET createMappingTargetFor(REFERENCE target, String targetProvider, String version, double score);
	
	final protected List<MAPPING> getAllMappings(Document mappingDocument) throws MappingMaterializationException {
		return retrieveAllMappings($nN(mappingDocument, "Please provide a non-null mapping document"));
	}
	
	final protected MAPPING getMappingsFor(Document mappingDocument, CODELIST data) throws MappingMaterializationException {
		$nN(mappingDocument, "Please provide a non-null mapping document");
		$nN(data, "Please provide a non-null codelist data");
		
		try {
			return 
				retrieveAllMappings(mappingDocument).
					stream().
						filter(m -> m != null && m.getEntry() != null && data.getId().equals(m.getEntry().getId())).
							findFirst().get();
		} catch(NoSuchElementException NSEe) {
			return null;
		}
	}
	
	final protected String getId(Element sourceOrTargetElement) {
		return 
			$nN(
				$nN(sourceOrTargetElement, "The element cannot be null").
					getElementsByTagName("ElementIdentifier").item(0).
						getAttributes().getNamedItem("elementId").getTextContent().
							replaceAll("^urn\\:", ""),
				"The mapping stores a NULL element ID"
			);
	}
}