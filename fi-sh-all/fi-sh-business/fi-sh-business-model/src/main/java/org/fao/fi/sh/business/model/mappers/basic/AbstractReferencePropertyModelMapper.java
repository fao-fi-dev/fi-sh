/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.mappers.basic;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.io.Serializable;

import org.fao.fi.sh.business.model.mappers.exceptions.MappingDataMaterializationException;
import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.basic.data.mappings.StringPropertyModelMapping;
import org.fao.fi.sh.model.core.basic.data.mappings.StringPropertyModelMappingTarget;
import org.fao.fi.sh.model.core.basic.properties.PropertyModel;
import org.fao.fi.sh.model.core.basic.properties.StringProperty;
import org.fao.fi.sh.model.core.basic.properties.StringPropertyModel;
import org.fao.fi.sh.model.core.spi.Mapped;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 23 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 23 Apr 2015
 */
abstract public class AbstractReferencePropertyModelMapper<ID extends Serializable, CODELIST extends CoreCodelist<ID, ?> & Mapped, MAPPING extends StringPropertyModelMapping<ID, CODELIST>> extends AbstractReferenceMapper<ID, CODELIST, StringPropertyModel, StringPropertyModelMappingTarget, MAPPING> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.mappings.spi.impl.AbstractReferenceMapper#createMappingTargetFor(java.lang.Object, java.lang.String, java.lang.String, double)
	 */
	@Override
	final protected StringPropertyModelMappingTarget createMappingTargetFor(StringPropertyModel target, String targetProvider, String version, double score) {
		return new StringPropertyModelMappingTarget(targetProvider, version, score, target);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.mappings.spi.impl.AbstractReferenceMapper#targetFrom(org.w3c.dom.Element)
	 */
	@Override
	final protected StringPropertyModel targetFrom(Element targetElement) throws MappingDataMaterializationException {
		try {
			StringPropertyModel data = new StringPropertyModel();
			
			String targetID = $nN(getId(targetElement), "The target ID is null");

			data.addProperty(new StringProperty(PropertyModel.ID_PROPERTY, targetID));
			
			NodeList rowData = targetElement.getElementsByTagName("RowColumn");
			
			Element current;
			String name, value;
			for(int n=0; n<rowData.getLength(); n++) {
				value = null;
				
				current = (Element)rowData.item(n);
				
				name = current.getElementsByTagName("ColumnName").item(0).getTextContent();
				
				if(current.getElementsByTagName("ColumnValue").getLength() > 0) {
					value = current.getElementsByTagName("ColumnValue").item(0).getTextContent();
				}
				
				if(value != null)
					data.addProperty(new StringProperty(name, value));
			}
			
			return data;
		} catch(Throwable t) {
			throw new MappingDataMaterializationException("Unable to extract target properties from mappings: " + t.getMessage(), t);
		}
	}
}