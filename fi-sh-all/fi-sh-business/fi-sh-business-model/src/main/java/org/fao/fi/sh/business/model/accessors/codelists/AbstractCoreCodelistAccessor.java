/**
 * (c) 2015 FAO / UN (project: fmis-business-accessors)
 */
package org.fao.fi.sh.business.model.accessors.codelists;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import net.jodah.typetools.TypeResolver;

import org.fao.fi.sh.business.model.accessors.basic.AbstractLocalizedIdentifiedAccessor;
import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedMapper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2015
 */
abstract public class AbstractCoreCodelistAccessor<CODELIST_ID extends Serializable, CODELIST_CODE extends Serializable, RAW extends CoreCodelist<CODELIST_ID, CODELIST_CODE>, DATA extends RAW, FILTER extends Example<RAW>, MAPPER extends IdentifiedMapper<CODELIST_ID, RAW, FILTER>> 
					  extends AbstractLocalizedIdentifiedAccessor<CODELIST_ID, RAW, DATA, FILTER, MAPPER>  {
	public List<DATA> getAllEnabled() throws Exception {
		return getAll().stream().filter(c -> c != null && Boolean.TRUE.equals(c.getEnabled())).collect(Collectors.toList());
	}
	
	@SuppressWarnings("unchecked")
	public Class<RAW> getRawCodelistClass() {
		return (Class<RAW>)TypeResolver.resolveRawArguments(AbstractCoreCodelistAccessor.class, this.getClass())[2];
	}
	
	@SuppressWarnings("unchecked")
	public Class<DATA> getCodelistClass() {
		return (Class<DATA>)TypeResolver.resolveRawArguments(AbstractCoreCodelistAccessor.class, this.getClass())[3];
	}
	
	public boolean isForRawClass(Class<? extends CoreCodelist<CODELIST_ID, CODELIST_CODE>> clazz) {
		return clazz.equals(getRawCodelistClass());
	}
	
	public boolean isForClass(Class<? extends CoreCodelist<CODELIST_ID, CODELIST_CODE>> clazz) {
		return clazz.equals(getCodelistClass());
	}
	
	public DATA changeAbilitation(CODELIST_ID id, boolean enabled, String updater) throws Exception {
		RAW current = mapper().selectByPrimaryKey(id);
		
		if(current == null)
			return null;
		
		current.setEnabled(enabled);
		
		if(updater != null)
			current.setUpdaterId(updater);
		
		mapper().updateByPrimaryKeySelective(current);
		
		return byKey(id);
	}
}