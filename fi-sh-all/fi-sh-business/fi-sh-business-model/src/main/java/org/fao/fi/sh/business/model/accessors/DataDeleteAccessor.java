/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors;

import org.fao.fi.sh.model.core.spi.persistence.Data;
import org.fao.fi.sh.model.core.spi.persistence.Example;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Apr 2015
 */
public interface DataDeleteAccessor<RAW extends Data, DATA extends RAW, FILTER extends Example<RAW>> {
	int delete(FILTER filter) throws Exception;
	int delete(DATA toDelete) throws Exception;
	int deleteRaw(RAW toDelete) throws Exception;
}
