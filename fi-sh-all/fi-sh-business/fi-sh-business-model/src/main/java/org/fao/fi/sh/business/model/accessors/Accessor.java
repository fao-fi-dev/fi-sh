/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors;

import java.util.List;

import org.fao.fi.sh.model.core.spi.persistence.Data;
import org.fao.fi.sh.model.core.spi.persistence.Example;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 31 mar 2015
 */
public interface Accessor<RAW extends Data, DATA extends RAW, FILTER extends Example<RAW>> extends
				 DataDeleteAccessor<RAW, DATA, FILTER>,
				 DataInsertAccessor<RAW, DATA, FILTER> {
	List<DATA> getAll() throws Exception;
	List<DATA> filter(FILTER filter) throws Exception;
	int countAll() throws Exception;
	int countFiltered(FILTER filter) throws Exception;
	
	default boolean isAny(FILTER filter) throws Exception {
		return countFiltered(filter) > 0;
	}
	
	List<RAW> getAllRaw() throws Exception;
	List<RAW> filterRaw(FILTER filter) throws Exception;
}
