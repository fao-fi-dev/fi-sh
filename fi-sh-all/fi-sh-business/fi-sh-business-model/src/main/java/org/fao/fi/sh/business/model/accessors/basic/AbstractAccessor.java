/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors.basic;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$eq;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import net.jodah.typetools.TypeResolver;

import org.fao.fi.sh.business.model.accessors.Accessor;
import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.sh.model.core.spi.persistence.Data;
import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.Mapper;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Apr 2015
 */
@Transactional(propagation=Propagation.NESTED, isolation=Isolation.READ_UNCOMMITTED) 
abstract public class AbstractAccessor<RAW extends Data, DATA extends RAW, FILTER extends Example<RAW>, MAPPER extends Mapper<RAW, FILTER>> 
					  extends AbstractLoggingAwareClient
					  implements Accessor<RAW, DATA, FILTER> {
	
	final static public boolean SELECTIVE_UPDATE	 = true;
	final static public boolean NON_SELECTIVE_UPDATE = !SELECTIVE_UPDATE;

	@Inject protected MAPPER _mapper;
	
	@SuppressWarnings("unchecked")
	protected FILTER example() {
		Class<?> filter = TypeResolver.resolveRawArguments(AbstractAccessor.class, this.getClass())[2];
		
		try {
			return (FILTER)filter.newInstance();
		} catch(Throwable t) {
			_log.error("Cannot create filter of type {} for {} using reflection: {} [ {} ]", filter.getName(), this.getClass().getName(), t.getClass().getSimpleName(), t.getMessage(), t);
			
			throw new RuntimeException(t);
		}
	}
	
	protected MAPPER mapper() {
		return _mapper;
	}
	
	@SuppressWarnings("unchecked")
	public DATA fromRaw(RAW data) throws Exception {
		if(data == null)
			return null;
		
		Class<?> raw 	  = TypeResolver.resolveRawArguments(AbstractAccessor.class, this.getClass())[0];
		Class<?> concrete = TypeResolver.resolveRawArguments(AbstractAccessor.class, this.getClass())[1];
		
		try {
			return (DATA)concrete.getConstructor(raw).newInstance(data);
		} catch(Exception t) {
			_log.error("Cannot create data class for {} from raw data {} using reflection: {} [ {} ]", concrete.getName(), raw.getName(), t.getClass().getSimpleName(), t.getMessage(), t);
			
			throw t;
		}
	}
	
	public RAW toRaw(DATA data) throws Exception {
		if(data == null)
			return null;
		
		return (RAW)data;
	}
	
	public RAW checkRawConformity(RAW toCheck, boolean selective) throws Exception {
		return doCheckRawConformity($nN(toCheck, "The data cannot be null"), selective);
	}
	
	protected RAW doCheckRawConformity(RAW toCheck, boolean selective) throws Exception {
		return toCheck;
	}
	
	public DATA checkConformity(DATA toCheck, boolean selective) throws Exception {
		return doCheckConformity($nN(toCheck, "The data cannot be null"), selective);
	}
	
	protected DATA doCheckConformity(DATA toCheck, boolean selective) throws Exception {
		return toCheck;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.FilterableAccessor#getAll()
	 */
	public List<DATA> getAll() throws Exception {
		return filter(example());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.FilterableAccessor#filter(org.fao.fi.sh.model.core.spi.persistence.Example)
	 */
	@Override
	public List<DATA> filter(FILTER filter) throws Exception {
		List<DATA> toReturn = new ArrayList<DATA>();
		
		for(RAW in : mapper().selectByExample(filter)) 
			if(in != null) toReturn.add(fromRaw(in));
		
		return toReturn;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.Accessor#countAll()
	 */
	@Override
	public int countAll() throws Exception {
		return countFiltered(example());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.Accessor#countFiltered(org.fao.fi.sh.model.core.spi.persistence.Example)
	 */
	@Override
	public int countFiltered(FILTER filter) throws Exception {
		return _mapper.countByExample(filter); // TODO Auto-generated method stub
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.Accessor#getAllRaw()
	 */
	@Override
	public List<RAW> getAllRaw() {
		return filterRaw(example());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.Accessor#filterRaw(org.fao.fi.sh.model.core.spi.persistence.Example)
	 */
	@Override
	public List<RAW> filterRaw(FILTER filter) {
		return mapper().selectByExample(filter);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.DataDeleteAccessor#delete(org.fao.fi.sh.model.core.spi.persistence.Example)
	 */
	@Override
	public int delete(FILTER filter) throws Exception {
		return _mapper.deleteByExample($nN(filter, "Please provide a non-null filter"));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.DataDeleteAccessor#delete(org.fao.fi.sh.model.core.spi.persistence.Data)
	 */
	@Override
	public int delete(DATA toDelete) throws Exception {
		int deleted = deleteRaw(toRaw(toDelete)); // TODO Auto-generated method stub
		
		return $eq(deleted, 1, "An attempt to delete a data resulted in {} records being deleted (expected: 1)", deleted);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.DataInsertAccessor#insert(org.fao.fi.sh.model.core.spi.persistence.Data)
	 */
	@Override
	public DATA insert(DATA toInsert) throws Exception {
		this.insertRaw(toRaw(toInsert));

		return toInsert;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.DataInsertAccessor#insertRaw(org.fao.fi.sh.model.core.spi.persistence.Data)
	 */
	@Override
	public RAW insertRaw(RAW toInsert) throws Exception {
		int numInserted =_mapper.insertSelective(checkRawConformity(toInsert, AbstractAccessor.NON_SELECTIVE_UPDATE));
		
		$eq(numInserted, 1, "An attempt to insert a data resulted in {} records being updated (expected: 1)", numInserted);
		
		return toInsert;
	}		
	
	final static protected boolean $$id(Identified<?> toCheck) {
		return toCheck != null && toCheck.getId() != null;
	}
	
	final static protected boolean $$nid(Identified<?> toCheck) {
		return toCheck != null && toCheck.getId() == null;
	}
	
	final static protected boolean $$nNid(Identified<?> toCheck) {
		return toCheck == null || toCheck.getId() == null;
	}
}
