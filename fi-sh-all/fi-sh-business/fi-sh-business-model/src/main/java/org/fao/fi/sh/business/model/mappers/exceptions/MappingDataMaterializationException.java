/**
 * (c) 2015 FAO / UN (project: fmis-business-processes)
 */
package org.fao.fi.sh.business.model.mappers.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
public class MappingDataMaterializationException extends MappingMaterializationException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -632877998976175580L;

	/**
	 * Class constructor
	 *
	 */
	public MappingDataMaterializationException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MappingDataMaterializationException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MappingDataMaterializationException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MappingDataMaterializationException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}

}
