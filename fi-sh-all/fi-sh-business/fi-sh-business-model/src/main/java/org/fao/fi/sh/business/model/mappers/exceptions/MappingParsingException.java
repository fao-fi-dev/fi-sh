/**
 * (c) 2015 FAO / UN (project: fmis-business-processes)
 */
package org.fao.fi.sh.business.model.mappers.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Apr 2015
 */
public class MappingParsingException extends MappingException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8101509771228625362L;

	/**
	 * Class constructor
	 *
	 */
	public MappingParsingException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public MappingParsingException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public MappingParsingException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public MappingParsingException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}
