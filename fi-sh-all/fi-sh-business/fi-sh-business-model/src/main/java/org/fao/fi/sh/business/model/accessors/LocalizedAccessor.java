/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.sh.model.core.spi.Localized;
import org.fao.fi.sh.model.core.spi.Localizer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2015
 */
public interface LocalizedAccessor<ID extends Serializable, DATA extends Identified<ID> & Localized<? extends Localizer<ID>>> {
	DATA localize(DATA data) throws Exception;
}
