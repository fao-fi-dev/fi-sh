/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.KeyedData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 9 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 9 Apr 2015
 */
public interface KeyedDataDeleteAccessor<ID extends Serializable, RAW extends KeyedData<ID>, DATA extends RAW, FILTER extends Example<RAW>> 
		 extends DataDeleteAccessor<RAW, DATA, FILTER> {
	
	int delete(ID id) throws Exception;
}
