/**
 * (c) 2015 FAO / UN (project: fis-business-model)
 */
package org.fao.fi.sh.business.model.accessors.basic;

import java.io.Serializable;

import org.fao.fi.sh.model.core.spi.persistence.Example;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedData;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedMapper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Mar 2015
 */
abstract public class AbstractIdentifiedAccessor<ID extends Serializable, RAW extends IdentifiedData<ID>, DATA extends RAW, FILTER extends Example<RAW>, MAPPER extends IdentifiedMapper<ID, RAW, FILTER>> 
					  extends AbstractKeyedAccessor<ID, RAW, DATA, FILTER, MAPPER> {
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.KeyedAccessor#getKey(org.fao.fi.sh.model.core.spi.persistence.KeyedData)
	 */
	@Override
	public ID key(RAW data) {
		return data == null ? null : data.getId();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.business.model.accessors.basic.AbstractKeyedAccessor#doUpdate(org.fao.fi.sh.model.core.spi.persistence.KeyedData, boolean)
	 */
	@Override
	protected void doUpdate(DATA updated, boolean selective) throws Exception {
	}
}