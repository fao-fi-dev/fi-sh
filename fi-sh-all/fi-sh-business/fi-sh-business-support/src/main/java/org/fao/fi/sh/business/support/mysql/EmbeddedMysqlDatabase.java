/**
 * (c) 2015 FAO / UN (project: fis-business-support)
 */
package org.fao.fi.sh.business.support.mysql;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import com.mysql.management.MysqldResource;
import com.mysql.management.MysqldResourceI;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
 
public class EmbeddedMysqlDatabase extends DriverManagerDataSource {
	private final Logger _log = LoggerFactory.getLogger(EmbeddedMysqlDatabase.class);

	private DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
	private ResourceDatabasePopulator databasePopulator;

	private MysqldResource mysqldResource;

	private String baseDatabaseDir = System.getProperty("java.io.tmpdir");
	
	private String databaseName = "test_db_" + System.nanoTime();
	
	private int port = -1;
	
	private int portFrom = -1;
	private int portTo = -1;
	
	private String username = "root";
	private String password = "";
	
	private boolean foreignKeyCheck = true;

	private List<String> _scripts = new ArrayList<String>();
	
	/**
	 * Class constructor
	 *
	 */
	public EmbeddedMysqlDatabase() {
	}

	@PostConstruct
	private void initialize() throws Exception {
		databasePopulator = new ResourceDatabasePopulator();
		databasePopulator.setSqlScriptEncoding("UTF-8");
		
		mysqldResource = createMysqldResource();
		
		createDatabase(mysqldResource.getPort());

		if(this._scripts != null)
			for(String script : this._scripts)
				this.addSqlScript(script);
		
		populateScripts();
	}

	/**
	 * @return the 'baseDatabaseDir' value
	 */
	public String getBaseDatabaseDir() {
		return this.baseDatabaseDir;
	}

	/**
	 * @param baseDatabaseDir the 'baseDatabaseDir' value to set
	 */
	public void setBaseDatabaseDir(String baseDatabaseDir) {
		this.baseDatabaseDir = baseDatabaseDir;
	}

	/**
	 * @return the 'port' value
	 */
	public int getPort() {
		return this.port;
	}

	/**
	 * @param port the 'port' value to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the 'portFrom' value
	 */
	public int getPortFrom() {
		return this.portFrom;
	}

	/**
	 * @param portFrom the 'portFrom' value to set
	 */
	public void setPortFrom(int portFrom) {
		this.portFrom = portFrom;
	}

	/**
	 * @return the 'portTo' value
	 */
	public int getPortTo() {
		return this.portTo;
	}

	/**
	 * @param portTo the 'portTo' value to set
	 */
	public void setPortTo(int portTo) {
		this.portTo = portTo;
	}

	/**
	 * @return the 'foreignKeyCheck' value
	 */
	public boolean isForeignKeyCheck() {
		return this.foreignKeyCheck;
	}

	/**
	 * @param foreignKeyCheck the 'foreignKeyCheck' value to set
	 */
	public void setForeignKeyCheck(boolean foreignKeyCheck) {
		this.foreignKeyCheck = foreignKeyCheck;
	}

	/**
	 * @return the 'databaseName' value
	 */
	public String getDatabaseName() {
		return this.databaseName;
	}

	/**
	 * @param databaseName the 'databaseName' value to set
	 */
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	/**
	 * @return the 'scripts' value
	 */
	public List<String> getScripts() {
		return this._scripts;
	}

	/**
	 * @param scripts the 'scripts' value to set
	 */
	public void setScripts(List<String> scripts) {
		this._scripts = scripts;
	}
	
	private int getActualPort() {
		if(port != -1) {
			_log.info("Determining MySql port: returning user-specified port #" + port);
			
			return port;
		}
		
		if(portFrom != -1 && portTo != -1) {
			if(portFrom < portTo) {
				_log.info("Determining MySql port: selecting a random port number between " + portFrom + " and " + portTo);
				
				return portFrom + new Random().nextInt(portTo - portFrom);
			}
		}
		
		_log.info("Returning a random port number from 3306 to 13306...");
		
		return new Random().nextInt(10000) + 3306; 
	}
	
	private EmbeddedMysqlDatabase createDatabase(int actualPort) {
		if(!mysqldResource.isRunning()) {
			_log.error("MySQL instance not found... Terminating");
			throw new RuntimeException("Cannot get Datasource, MySQL instance not started.");
		}

		this.setDriverClassName("com.mysql.jdbc.Driver");
		this.setUsername(username);
		this.setPassword(password);
		
		String url = "jdbc:mysql://localhost:" + actualPort + "/" + databaseName + "?" + "createDatabaseIfNotExist=true";

		if(!foreignKeyCheck) {
			url += "&sessionVariables=FOREIGN_ID_CHECKS=0";
		}
		
		_log.debug("database url: {}", url);
		
		this.setUrl(url);
		
		return this;
	}
	
	private MysqldResource createMysqldResource() throws IOException {
		String actualDatabaseDir = baseDatabaseDir + ( baseDatabaseDir.endsWith(File.separator) ? "" : File.separator ) + "EmbeddedMySQL" + System.nanoTime();
		
		FileUtils.forceMkdir(new File(actualDatabaseDir));
		
		int actualPort = getActualPort();
		
		_log.info("=============== Starting Embedded MySQL using these parameters ===============");
		_log.info("baseDatabaseDir : " + baseDatabaseDir);
		_log.info("actualDatabaseDir : " + actualDatabaseDir);
		_log.info("databaseName : " + databaseName);
		_log.info("host : localhost (hardcoded)");
		_log.info("port : " + actualPort);
		_log.info("username : root (hardcoded)");
		_log.info("password : (no password)");
		_log.info("=============================================================================");

		Map<String, String> databaseOptions = new HashMap<String, String>();
		databaseOptions.put("lower_case_table_names", "1");
		databaseOptions.put(MysqldResourceI.PORT, Integer.toString(actualPort));

		MysqldResource mysqldResource = new MysqldResource(new File(actualDatabaseDir, databaseName));
		mysqldResource.start("embedded-mysqld-thread-" + System.currentTimeMillis(), databaseOptions);

		if(!mysqldResource.isRunning()) { throw new RuntimeException("MySQL did not start."); }

		_log.info("MySQL started successfully @ {}", System.currentTimeMillis());
		return mysqldResource;
	}

	private void populateScripts() {
		try {
			DatabasePopulatorUtils.execute(databasePopulator, this);
		} catch(Exception e) {
			e.printStackTrace();
			
			_log.error(e.getMessage(), e);
			
			this.shutdown();
		}
	}

	public EmbeddedMysqlDatabase addSqlScript(String script) {
		databasePopulator.addScript(resourceLoader.getResource(script));
		
		return this;
	}

	@PreDestroy
	public void shutdown() {
		if(mysqldResource != null) {
			mysqldResource.shutdown();
			if(!mysqldResource.isRunning()) {
				
				_log.info("===================================================================");
				_log.info("Deleting Embedded MySQL base dir [{}]", mysqldResource.getBaseDir());
				_log.info("===================================================================");
				
				try {
					FileUtils.forceDelete(mysqldResource.getBaseDir());
				} catch(IOException e) {
					_log.error(e.getMessage(), e);
				}
			}
		}
	}
}
