/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.impl.xls;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataExtractionException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataFormatException;
import org.fao.fi.sh.business.processes.data.upload.spi.impl.AbstractBulkDataExtractor;
import org.fao.fi.sh.model.core.spi.Form;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
abstract public class AbstractXLSBulkDataExtractor<FORM extends Form> extends AbstractBulkDataExtractor<Workbook, FORM> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#fromStream(java.io.InputStream)
	 */
	@Override
	public Workbook fromStream(InputStream stream) throws IOException, BulkDataFormatException {
		try {
			return WorkbookFactory.create(stream);
		} catch(Throwable t) {
			throw new BulkDataFormatException("Unable to parse provided stream as an XLS file. " + t.getClass().getSimpleName() + " caught: " + t.getMessage(), t);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#isValid(java.lang.Object)
	 */
	@Override
	public Workbook check(Workbook container) throws BulkDataExtractionException {

		//Removed following YLaurent request
		
//		int numSheets = container.getNumberOfSheets();
//		
//		if(numSheets != 1) {
//			throw new BulkDataExtractionException("Provided XLS is not valid (" + numSheets + " sheets found, expected: 1)");
//		}
		
		Sheet sheet = getSheet(container);
		
		int numRows = sheet.getLastRowNum() - sheet.getFirstRowNum() + 1;
		
		if(numRows <= 1) {
			throw new BulkDataExtractionException("Provided XLS is not valid (" + numRows + " rows found, expected > 1)");
		}
		
		Row header = sheet.getRow(0);
		
		int numCols = header.getLastCellNum() - header.getFirstCellNum();
		
		if(numCols < getExpectedNumberOfColumns()) {
			throw new BulkDataExtractionException("Provided XLS is not valid (" + numCols + " columns found in header row, expected: " + getExpectedNumberOfColumns() + ")");
		}
		
		return container;
	}
		
	protected Date getDate(Cell cell) {
		return cell.getDateCellValue();
	}
	
	protected String getString(Cell cell) {
		return trim(cell.getStringCellValue());
	}
	
	protected int getInteger(Cell cell) {
		return (int)Math.round(cell.getNumericCellValue());
	}
	
	protected double getDouble(Cell cell) {
		return cell.getNumericCellValue();
	}
	
	protected Sheet getSheet(Workbook container) {
		return container.getSheetAt(0);
	}
	
	protected String trim(String toTrim) {
		if(toTrim == null)
			return null;
		
		toTrim = toTrim.trim();
		
		if("".equals(toTrim))
			return null;
		
		return toTrim;
	}
}