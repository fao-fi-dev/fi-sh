/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public class BulkDataExtractionException extends BulkDataException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7097367426351786911L;

	/**
	 * Class constructor
	 *
	 */
	public BulkDataExtractionException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public BulkDataExtractionException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public BulkDataExtractionException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public BulkDataExtractionException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}
