/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi;

import java.util.List;

import org.fao.fi.sh.business.processes.data.validation.spi.exceptions.BusinessValidatorException;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationResult;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationRule;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.model.core.spi.Severity;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
public interface BusinessRulesValidator<F extends Form> extends Comparable<BusinessRulesValidator<F>> {
	/** @return the validator priority, from 0 (higher) to whatever other positive integer (the higher, the lower the priority) */
	int priority();
	
	/** @return whether this validator applies to the given form */
	boolean appliesTo(F form);
	
	/** @return the list of validation rules defined for this validator */
	List<BusinessValidationRule> getValidationRules();
	
	/**
	 * @param form a Form (that the validator applies to) 
	 * @return the list of validation results obtained by applying the validator business rules to the given form
	 */
	List<BusinessValidationResult> validate(F form) throws BusinessValidatorException;
	
	/**
	 * @param results a list of validation results obtained by applying the validator business rules to a form 
	 * @param severities a list of severities through which results will be filtered (by retaining only results with any of the given severities)
	 * 
	 * @return the list of validation results filtered by severity
	 */
	List<BusinessValidationResult> filterValidationResults(List<BusinessValidationResult> results, List<Severity> severities);

	/**
	 * @param results a list of validation results obtained by applying the validator business rules to a form
	 * @return whether none of the validation results has a severity of type {@link Severity#FATAL} or {@link Severity#ERROR}  
	 */
	boolean isValidationSuccessful(List<BusinessValidationResult> results);
}