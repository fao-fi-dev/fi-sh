/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.impl;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$lt;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;
import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nNeg;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.fao.fi.sh.business.processes.data.upload.spi.BulkDataExtractor;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataFormatException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataMultipleReferencesException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataNoReferenceException;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
abstract public class AbstractBulkDataExtractor<DATA_FORMAT, FORM extends Form> extends AbstractLoggingAwareClient implements BulkDataExtractor<DATA_FORMAT, FORM> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#fromRawBytes(byte[])
	 */
	@Override
	final public DATA_FORMAT fromRawBytes(byte[] content) throws IOException, BulkDataFormatException {
		try(ByteArrayInputStream stream = new ByteArrayInputStream($nN(content, "The raw content cannot be null"))) {
			return fromStream(stream);
		}
	}
	
	abstract protected int getExpectedNumberOfColumns();
	
	final protected boolean isMandatory(int columnNumber) {
		$lt($nNeg(columnNumber, "The column number should be non negative"), getExpectedNumberOfColumns(), "The maximum number of allowed columns is {}", getExpectedNumberOfColumns());
		
		return this.doCheckIfMandatory(columnNumber);
	}
	
	abstract protected boolean doCheckIfMandatory(int columnNumber);
	
	protected <R> R one(List<R> data) throws BulkDataMultipleReferencesException, BulkDataNoReferenceException {
		if(data == null || data.isEmpty()) throw new BulkDataNoReferenceException();
		if(data.size() > 1) throw new BulkDataMultipleReferencesException(data.size());
		
		return data.get(0);
	}
	
	protected <R> R one(R data) throws BulkDataMultipleReferencesException, BulkDataNoReferenceException {
		if(data == null) throw new BulkDataNoReferenceException();
		
		return data;
	}
}
