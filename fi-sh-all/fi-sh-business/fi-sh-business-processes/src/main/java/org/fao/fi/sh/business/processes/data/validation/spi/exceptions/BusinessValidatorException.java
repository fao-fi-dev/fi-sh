/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
public class BusinessValidatorException extends Exception {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 561436832305667127L;

	/**
	 * Class constructor
	 *
	 */
	public BusinessValidatorException() {
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public BusinessValidatorException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public BusinessValidatorException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public BusinessValidatorException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}