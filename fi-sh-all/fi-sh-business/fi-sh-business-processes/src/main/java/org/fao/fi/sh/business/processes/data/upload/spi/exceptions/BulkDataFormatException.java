/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.exceptions;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public class BulkDataFormatException extends BulkDataException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4846138944261219357L;

	/**
	 * Class constructor
	 *
	 */
	public BulkDataFormatException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public BulkDataFormatException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public BulkDataFormatException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public BulkDataFormatException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}

}
