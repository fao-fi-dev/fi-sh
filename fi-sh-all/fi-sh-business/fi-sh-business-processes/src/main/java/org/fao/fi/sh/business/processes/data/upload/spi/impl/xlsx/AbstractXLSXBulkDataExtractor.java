/**
 * (c) 2015 FAO / UN (project: fxis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.impl.xlsx;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataExtractionException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataFormatException;
import org.fao.fi.sh.business.processes.data.upload.spi.impl.AbstractBulkDataExtractor;
import org.fao.fi.sh.model.core.spi.Form;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
abstract public class AbstractXLSXBulkDataExtractor<FORM extends Form> extends AbstractBulkDataExtractor<XSSFWorkbook, FORM> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#fromStream(java.io.InputStream)
	 */
	@Override
	public XSSFWorkbook fromStream(InputStream stream) throws IOException, BulkDataFormatException {
		return new XSSFWorkbook(stream);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#isValid(java.lang.Object)
	 */
	@Override
	public XSSFWorkbook check(XSSFWorkbook container) throws BulkDataExtractionException {
		//Removed following YLaurent request
		
//		int numSheets = container.getNumberOfSheets();
//		
//		if(numSheets != 1) {
//			throw new BulkDataExtractionException("Provided XLS is not valid (" + numSheets + " sheets found, expected: 1)");
//		}
		
		XSSFSheet sheet = getSheet(container);
		
		int numRows = sheet.getLastRowNum() - sheet.getFirstRowNum();
		
		if(numRows <= 1) {
			throw new BulkDataExtractionException("Provided XLS is not valid (" + numRows + " rows found, expected > 1)");
		}
		
		XSSFRow header = sheet.getRow(0);
		
		int numCols = header.getLastCellNum() - header.getFirstCellNum();
		
		if(numCols < getExpectedNumberOfColumns()) {
			throw new BulkDataExtractionException("Provided XLS is not valid (" + numCols + "columns found in header row, expected: " + getExpectedNumberOfColumns() + ")");
		}
		
		return container;
	}
	
	protected XSSFSheet getSheet(XSSFWorkbook container) {
		return container.getSheetAt(0);
	}
}