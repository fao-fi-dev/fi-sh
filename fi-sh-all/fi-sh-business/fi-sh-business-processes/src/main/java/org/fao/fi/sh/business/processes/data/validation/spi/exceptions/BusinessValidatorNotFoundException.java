/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
public class BusinessValidatorNotFoundException extends BusinessValidatorException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6101999983366606621L;

	/**
	 * Class constructor
	 *
	 */
	public BusinessValidatorNotFoundException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public BusinessValidatorNotFoundException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public BusinessValidatorNotFoundException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public BusinessValidatorNotFoundException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}
