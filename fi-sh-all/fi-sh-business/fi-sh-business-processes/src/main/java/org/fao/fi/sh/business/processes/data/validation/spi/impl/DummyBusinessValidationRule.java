/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi.impl;

import java.util.Arrays;
import java.util.List;

import org.fao.fi.sh.model.core.basic.validation.BusinessValidationResult;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationRule;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.model.core.spi.Severity;

/**
 * A dummy business validation rule that mandatorily applies to all forms and returns an {@link Severity#ERROR} if 
 * the form it is applied to is null  
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public class DummyBusinessValidationRule extends BusinessValidationRule {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8840413405797300346L;

	public static DummyBusinessValidationRule RULE = new DummyBusinessValidationRule();
	
	/**
	 * Class constructor
	 *
	 */
	public DummyBusinessValidationRule() {
		// TODO Auto-generated constructor block
	}

	@Override
	public List<BusinessValidationResult> validate(Form form) throws Exception {
		return Arrays.asList(
			new BusinessValidationResult[] {
				new BusinessValidationResult(
					form == null ? Severity.ERROR : Severity.INFO,
					this,
					form == null ? "The form is NULL" : "The form has been successfully validated"
				)
			}
		);
	}
	
	@Override
	public boolean isOptionalFor(Form form) {
		return false;
	}
	
	@Override
	public boolean canBeAppliedTo(Form form) {
		return true;
	}
	
	@Override
	public boolean appliesTo(Form form) {
		return true;
	}
}
