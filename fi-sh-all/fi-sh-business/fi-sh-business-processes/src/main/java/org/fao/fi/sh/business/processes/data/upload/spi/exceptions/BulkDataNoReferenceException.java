/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2015
 */
public class BulkDataNoReferenceException extends BulkDataException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5849124563741959805L;

	/**
	 * Class constructor
	 *
	 */
	public BulkDataNoReferenceException() {
	}
}
