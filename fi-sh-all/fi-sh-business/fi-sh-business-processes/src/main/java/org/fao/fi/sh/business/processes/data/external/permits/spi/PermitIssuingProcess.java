/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.external.permits.spi;

import org.fao.fi.sh.business.processes.data.external.permits.spi.exceptions.PermitIssuingProcessException;
import org.fao.fi.sh.business.processes.data.external.permits.spi.exceptions.PermitIssuingProcessPreconditionException;
import org.fao.fi.sh.model.core.basic.data.permits.Permit;
import org.fao.fi.sh.model.core.basic.data.permits.PermitTarget;

/**
 * The interface modeling the request for a certificate of registration for a vessel.
 * 
 * It assumes that after the registration has been granted, a permit number is returned to the client.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public interface PermitIssuingProcess<CONTEXT, PERMIT extends Permit<?> & PermitTarget<CONTEXT>> {
	/** 
	 * Request a permit with the given details
	 * 
	 * @param context any additional context that might be required to issue the permit
	 * @param details all the permit request details (with blank attributes to be filled by the issuing authority)
	 * @return the permit with all attributes filled by the issuing authority
	 * @throws PermitIssuingProcessException if something goes wrong
	 */
	PERMIT requestPermit(CONTEXT context, PERMIT request) throws PermitIssuingProcessException;

	PERMIT checkRequestPreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException;

	PERMIT updatePermit(CONTEXT context, PERMIT request) throws PermitIssuingProcessException;

	PERMIT checkUpdatePreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException;
}