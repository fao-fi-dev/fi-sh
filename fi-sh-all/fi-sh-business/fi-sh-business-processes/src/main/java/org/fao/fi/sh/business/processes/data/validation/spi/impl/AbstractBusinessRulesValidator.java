/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi.impl;

import static org.fao.fi.sh.model.core.spi.Severity.ERROR;
import static org.fao.fi.sh.model.core.spi.Severity.FATAL;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.fao.fi.sh.business.processes.data.validation.spi.BusinessRulesValidator;
import org.fao.fi.sh.business.processes.data.validation.spi.exceptions.BusinessValidatorException;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationResult;
import org.fao.fi.sh.model.core.basic.validation.BusinessValidationRule;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.model.core.spi.Severity;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.common.annotations.di.InheritedComponent;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
abstract public @InheritedComponent class AbstractBusinessRulesValidator<F extends Form> 
										  extends AbstractLoggingAwareClient 
										  implements BusinessRulesValidator<F> {
	protected int _priority = 0;
	
	final public void setPriority(int priority) {
		_priority = $nNeg(priority, "Validator priorities must be >= 0 (currently: {})", priority);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#priority()
	 */
	@Override
	final public int priority() {
		return _priority;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	final public int compareTo(BusinessRulesValidator<F> o) {
		return this.priority() - o.priority(); // TODO Auto-generated method stub
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#getValidationRules()
	 */
	@Override
	final public List<BusinessValidationRule> getValidationRules() {
		List<BusinessValidationRule> rules = this.doGetValidationRules();
		
		if(rules != null)
			Collections.sort(rules);
		
		return rules; // TODO Auto-generated method stub
	}
	
	abstract protected List<BusinessValidationRule> doGetValidationRules();
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#filterValidationResults(java.util.List, java.util.List)
	 */
	@Override
	final public List<BusinessValidationResult> filterValidationResults(List<BusinessValidationResult> results, List<Severity> severities) {
		return 
			results == null ? null :
				severities == null || severities.isEmpty() ? results :
					results.stream().
						filter(r -> r!= null && r.getSeverity() != null && severities.contains(r.getSeverity())).
							collect(Collectors.toList());
	}
	
	/**
	 * @param results
	 * @param severities
	 * @return
	 */
	final public List<BusinessValidationResult> filterValidationResults(List<BusinessValidationResult> results, Severity... severities) {
		return this.filterValidationResults(results, severities == null ? null : Arrays.asList(severities));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#validate(org.fao.fi.fmis.model.common.Form)
	 */
	@Override
	final public List<BusinessValidationResult> validate(F form) throws BusinessValidatorException {
		return validate(form, getValidationRules());
	}

	/**
	 * @param form
	 * @param rules
	 * @return
	 * @throws BusinessValidatorException
	 */
	final protected List<BusinessValidationResult> validate(F form, List<BusinessValidationRule> rules) throws BusinessValidatorException {
		$nN(form, "The form to validate must not be null");
		$nEm(rules, "The validation rules must not be null or empty");
		
		$true(this.appliesTo(form), "This validator ({}) does not apply to form {} of type {}", this.getClass().getSimpleName(), form, form.getFormType());
		
		List<BusinessValidationResult> overall = new ArrayList<BusinessValidationResult>();
		
		List<BusinessValidationResult> partials;
		
		for(BusinessValidationRule in : rules) {
			$true(
				$nN(in, "Cannot apply a null rule to form {} of type {}", form, form.getFormType()).
				appliesTo(form), "Rule #{} ({}) does not applies to form {} of type {}", in.getId(), in.getName(), form, form.getFormType()
			);
			
			$true(in.canBeAppliedTo(form) || in.isOptionalFor(form), "Mandatory rule #{} ({}) cannot be applied to form {} of type {}", in.getId(), in.getName(), form, form.getFormType());
			
			try {
				if((partials = in.validate(form)) != null) overall.addAll(partials);
			} catch(Throwable t) {
				overall.add(new BusinessValidationResult(FATAL, in, "Unexpected " + t.getClass().getSimpleName() + " caught: " + t.getMessage()));
			}
		}
		
		return overall;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#isValidationSuccessful(java.util.Collection)
	 */
	@Override
	final public boolean isValidationSuccessful(List<BusinessValidationResult> results) {
		return filterValidationResults(
			$nN(results, "Please provide a non-null collection of validation results"), 
			ERROR, 
			FATAL
		).isEmpty();
	}
}