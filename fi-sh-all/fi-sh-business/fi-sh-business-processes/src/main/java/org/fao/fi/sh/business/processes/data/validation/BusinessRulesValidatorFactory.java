/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.$nN;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.business.processes.data.validation.spi.BusinessRulesValidator;
import org.fao.fi.sh.business.processes.data.validation.spi.exceptions.BusinessValidatorNotFoundException;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
@Singleton @Named("business.processes.validator.factory") 
final public class BusinessRulesValidatorFactory extends AbstractLoggingAwareClient {
	@Inject private List<BusinessRulesValidator<Form>> _validators;
	
	public List<BusinessRulesValidator<Form>> validatorsFor(Form form) throws BusinessValidatorNotFoundException {
		$nN(form, "The form cannot be null");
		
		List<BusinessRulesValidator<Form>> validators = _validators.stream().filter(v -> $nN(v, "Null validator found...").appliesTo(form)).collect(Collectors.toList());

		if(validators.isEmpty()) throw new BusinessValidatorNotFoundException("Can't find any business rules validator for Form " + form.getClass().getName() + " of type " + form.getFormType());
		
		Collections.sort(validators);
		
		return validators;
	}
}
