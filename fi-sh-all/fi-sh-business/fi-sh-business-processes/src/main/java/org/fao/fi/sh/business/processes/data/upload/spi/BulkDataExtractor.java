/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi;

import java.io.IOException;
import java.io.InputStream;

import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataExtractionException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataFormatException;
import org.fao.fi.sh.model.core.spi.Form;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public interface BulkDataExtractor<DATA_FORMAT, FORM extends Form> {
	DATA_FORMAT check(DATA_FORMAT container) throws BulkDataExtractionException;
	
	DATA_FORMAT fromStream(InputStream stream) throws IOException, BulkDataFormatException;
	
	DATA_FORMAT fromRawBytes(byte[] content) throws IOException, BulkDataFormatException;
}