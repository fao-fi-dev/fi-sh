/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.external.permits.spi.exceptions;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
public class PermitIssuingProcessPreconditionException extends PermitIssuingProcessException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7590756333646308712L;

	/**
	 * Class constructor
	 *
	 */
	public PermitIssuingProcessPreconditionException() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public PermitIssuingProcessPreconditionException(String message) {
		super(message); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param cause
	 */
	public PermitIssuingProcessPreconditionException(Throwable cause) {
		super(cause); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param message
	 * @param cause
	 */
	public PermitIssuingProcessPreconditionException(String message, Throwable cause) {
		super(message, cause); // TODO Auto-generated constructor block
	}
}
