/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.impl.csv;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataExtractionException;
import org.fao.fi.sh.business.processes.data.upload.spi.exceptions.BulkDataFormatException;
import org.fao.fi.sh.business.processes.data.upload.spi.impl.AbstractBulkDataExtractor;
import org.fao.fi.sh.model.core.spi.Form;

import au.com.bytecode.opencsv.CSVReader;

import static org.fao.fi.sh.utility.common.helpers.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
abstract public class AbstractCSVBulkDataExtractor<FORM extends Form> extends AbstractBulkDataExtractor<CSVReader, FORM> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#fromStream(java.io.InputStream)
	 */
	@Override
	public CSVReader fromStream(InputStream stream) throws IOException, BulkDataFormatException {
		try(Reader reader = new InputStreamReader($nN(stream, "The data stream cannot be null"), Charset.forName("UTF-8"))) {
			return new CSVReader(reader, ',', '"');
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.upload.spi.BulkDataExtractor#isValid(java.lang.Object)
	 */
	@Override
	public CSVReader check(CSVReader container) throws BulkDataExtractionException {
		try {
			int numRecords = 0;
			
			String[] data = null;
			
			while((data = container.readNext()) != null) { numRecords++; }
			
			if(numRecords <= 1) { 
				throw new BulkDataExtractionException("Provided CSV is not valid (" + numRecords + " records found, expected at least 2, including header)");
			}
			
			int numCols = data == null ? 0 : data.length;
			
			if(numCols < getExpectedNumberOfColumns()) {
				throw new BulkDataExtractionException("Provided CSV is not valid (" + numCols + " columns found in header row, expected: " + getExpectedNumberOfColumns() + ")");
			}
			
			return container;
		} catch(Throwable t) {
			throw new BulkDataExtractionException("Cannot properly read provided CSV. " + t.getClass().getSimpleName() + " caught (" + t.getMessage() + ")", t);
		}
	}
}