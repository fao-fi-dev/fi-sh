/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.external.permits.spi.impl;

import java.util.Date;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.business.processes.data.external.permits.spi.PermitIssuingProcess;
import org.fao.fi.sh.business.processes.data.external.permits.spi.exceptions.PermitIssuingProcessException;
import org.fao.fi.sh.business.processes.data.external.permits.spi.exceptions.PermitIssuingProcessPreconditionException;
import org.fao.fi.sh.model.core.basic.data.permits.Permit;
import org.fao.fi.sh.model.core.basic.data.permits.PermitTarget;
import org.fao.fi.sh.utility.common.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.common.annotations.di.InheritedComponent;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
@Named @Singleton @InheritedComponent
abstract public class AbstractCorePermitIssuingProcess<CONTEXT, PERMIT extends Permit<?> & PermitTarget<CONTEXT>> extends AbstractLoggingAwareClient implements PermitIssuingProcess<CONTEXT, PERMIT> {
	protected PERMIT assignPermitNumber(CONTEXT context, PERMIT request) {
		request.setPermitNumber(this.doAssignPermitNumber(context, request));
		
		return request;
	};
	
	abstract protected String doAssignPermitNumber(CONTEXT context, PERMIT request);
	
	final private PERMIT doCheckCommonPreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException {
		if(details == null) throw new PermitIssuingProcessPreconditionException("The permit request cannot be null");
		
		if(details.getDateFrom() == null) throw new PermitIssuingProcessPreconditionException("The requested permit start date must not be null");
		
		if(details.getDateTo() != null) {
			if(details.getDateTo().getTime() < System.currentTimeMillis()) throw new PermitIssuingProcessPreconditionException("The requested permit end date must be in the immediate future");
			
			if(details.getDateTo().before(details.getDateFrom())) throw new PermitIssuingProcessPreconditionException("The requested permit end date must follow the permit start date");
		}
		
		return details;
	}
	
	abstract protected PERMIT doCheckCustomCommonPreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException;

	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.external.permits.spi.PermitIssuingProcess#checkPreconditions(java.lang.Object, org.fao.fi.fmis.model.core.basic.composite.Permit)
	 */
	@Override
	final public PERMIT checkRequestPreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException {
		if(details.getPermitNumber() != null) throw new PermitIssuingProcessPreconditionException("The permit request must not specify a permit number");

		return this.doCheckRequestPreconditions(context, this.doCheckCustomCommonPreconditions(context, this.doCheckCommonPreconditions(context, details)));
	}
	
	abstract protected PERMIT doCheckRequestPreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException;

	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.external.permits.spi.PermitIssuingProcess#requestPermit(java.lang.Object, org.fao.fi.fmis.model.core.basic.composite.Permit)
	 */
	@Override
	final public PERMIT requestPermit(CONTEXT context, PERMIT request) throws PermitIssuingProcessException {
		return assignPermitNumber(context, this.doRequestPermit(context, checkRequestPreconditions(context, request)));
	}
	
	protected PERMIT doRequestPermit(CONTEXT context, PERMIT details) throws PermitIssuingProcessException {
		Date now = new Date();
		
		details.setApplicationDate(now);
		details.setPermitDate(now);
		
		return details;
	}


	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.external.permits.spi.PermitIssuingProcess#checkUpdatePreconditions(java.lang.Object, org.fao.fi.fmis.model.core.basic.permits.Permit)
	 */
	@Override
	public PERMIT checkUpdatePreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException {
		if(details.getPermitNumber() == null) throw new PermitIssuingProcessPreconditionException("The permit request must specify an existing permit number");
		
		return this.doCheckUpdatePreconditions(context, this.doCheckCustomCommonPreconditions(context, this.doCheckCommonPreconditions(context, details)));
	}
	
	abstract protected PERMIT doCheckUpdatePreconditions(CONTEXT context, PERMIT details) throws PermitIssuingProcessPreconditionException;

	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.external.permits.spi.PermitIssuingProcess#updatePermit(java.lang.Object, org.fao.fi.fmis.model.core.basic.permits.Permit)
	 */
	@Override
	final public PERMIT updatePermit(CONTEXT context, PERMIT request) throws PermitIssuingProcessException {
		return this.doUpdatePermit(context, checkUpdatePreconditions(context, request));
	}
	
	protected PERMIT doUpdatePermit(CONTEXT context, PERMIT details) throws PermitIssuingProcessException {
		return this.doRequestPermit(context, details);
	}
}
