/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.validation.spi.impl;

import java.util.Arrays;
import java.util.List;

import org.fao.fi.sh.model.core.basic.validation.BusinessValidationRule;
import org.fao.fi.sh.model.core.spi.Form;

/**
 * A dummy business validator that applies the {@link DummyBusinessValidationRule} to all forms  
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
final public class DummyBusinessRulesValidator<FORM extends Form> extends AbstractBusinessRulesValidator<FORM> {
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.BusinessRulesValidator#appliesTo(org.fao.fi.fmis.model.common.Form)
	 */
	@Override
	public boolean appliesTo(FORM form) {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.fmis.business.processes.data.validators.spi.impl.AbstractBusinessRulesValidator#doGetValidationRules()
	 */
	@Override
	protected List<BusinessValidationRule> doGetValidationRules() {
		return Arrays.asList(new BusinessValidationRule[] { DummyBusinessValidationRule.RULE });
	}
}