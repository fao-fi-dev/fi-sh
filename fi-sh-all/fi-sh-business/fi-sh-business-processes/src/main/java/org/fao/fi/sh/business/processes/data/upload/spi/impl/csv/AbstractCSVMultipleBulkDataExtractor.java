/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.impl.csv;

import org.fao.fi.sh.business.processes.data.upload.spi.MultipleBulkDataExtractor;
import org.fao.fi.sh.model.core.spi.Form;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
abstract public class AbstractCSVMultipleBulkDataExtractor<FORM extends Form> extends AbstractCSVBulkDataExtractor<FORM> implements MultipleBulkDataExtractor<CSVReader, FORM> {
}