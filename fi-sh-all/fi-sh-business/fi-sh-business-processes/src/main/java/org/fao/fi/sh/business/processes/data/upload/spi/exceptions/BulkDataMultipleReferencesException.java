/**
 * (c) 2015 FAO / UN (project: fis-business-processes)
 */
package org.fao.fi.sh.business.processes.data.upload.spi.exceptions;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2015
 */
public class BulkDataMultipleReferencesException extends BulkDataException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5849124563741959805L;

	private int _numReferences = Integer.MAX_VALUE;
	
	/**
	 * Class constructor
	 *
	 */
	public BulkDataMultipleReferencesException(int numReferences) {
		this._numReferences = numReferences;
	}

	/**
	 * @return the 'numReferences' value
	 */
	public int getNumReferences() {
		return this._numReferences;
	}

	/**
	 * @param numReferences the 'numReferences' value to set
	 */
	public void setNumReferences(int numReferences) {
		this._numReferences = numReferences;
	}	
}
