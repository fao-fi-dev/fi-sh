/**
 * (c) 2015 FAO / UN (project: fi-sh-model-core)
 */
package org.fao.fi.sh.model.core.test.basic.composite;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.basic.composite.CoreCodelistLocalizer;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2015
 */
public class TestJAXBSerialization {
	@SuppressWarnings("serial")
	@XmlRootElement(name="DummyCodelist")
	static private class DummyCodelist extends CoreCodelist<Integer, String> {	
		@XmlElement(name="id")
		private Integer id;
		
		@SuppressWarnings("unused")
		public DummyCodelist() {
			super(); // TODO Auto-generated constructor block
		}

		/**
		 * @return the 'id' value
		 */
		public Integer getId() {
			return this.id;
		}



		/**
		 * @param id the 'id' value to set
		 */
		public void setId(Integer id) {
			this.id = id;
		}



		public DummyCodelist(Integer id, String code, String name, String description) {
			super();
			this.setId(id);
			this.setCode(code);
			this.setName(name);
			this.setDescription(description);
		}
	}
	
	@SuppressWarnings("serial")
	@XmlRootElement(name="DummyCodelistLocalizer")
	static private class DummyCodelistLocalizer extends CoreCodelistLocalizer<Integer> {
		@SuppressWarnings("unused")
		public DummyCodelistLocalizer() {
			super(); // TODO Auto-generated constructor block
		}

		public DummyCodelistLocalizer(Integer parentId, String lang, String country, String name, String description) {
			super();
			this.setParentId(parentId);
			this.setSysLangI18nId(lang);
			this.setSysCountryI18nId(country);
			this.setName(description);
			this.setDescription(description);
		}
	}
	
	@Test public void testCodelistXMLSerialization() throws Throwable {
		DummyCodelist mock = new DummyCodelist(1, "FOO", "Foo", "Bar");
		
		JAXBContext.newInstance(DummyCodelist.class).createMarshaller().marshal(mock, System.out);
	}
	
	@Test public void testCodelistXMLDeSerialization() throws Throwable {
		DummyCodelist mock = new DummyCodelist(1, "FOO", "Foo", "Bar");
		
		JAXBContext ctx = JAXBContext.newInstance(DummyCodelist.class);
		
		StringWriter sw = new StringWriter();
		
		ctx.createMarshaller().marshal(mock, sw);

		StringReader sr = new StringReader(sw.toString());

		Assert.assertEquals(DummyCodelist.class, ctx.createUnmarshaller().unmarshal(sr).getClass());
	}
	
	@Test public void testCodelistLocalizerXMLSerialization() throws Throwable {
		DummyCodelistLocalizer mock = new DummyCodelistLocalizer(1, "en", "us", "Foo", "Bar");
		
		JAXBContext.newInstance(DummyCodelistLocalizer.class).createMarshaller().marshal(mock, System.out);
	}
}
