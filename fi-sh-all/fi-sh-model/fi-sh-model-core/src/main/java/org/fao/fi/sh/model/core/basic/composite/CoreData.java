/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import org.fao.fi.sh.model.core.spi.Identified;


/**
 * Base class for core data entities (tables prefixed by DT_).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
abstract public class CoreData<ID> extends CoreUpdatableData implements Identified<ID> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4286175703386424015L;
}