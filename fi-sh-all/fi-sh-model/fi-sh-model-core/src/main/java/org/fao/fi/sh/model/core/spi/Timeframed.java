/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.util.Date;

/**
 * Common interface for all entities having a validity in time.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface Timeframed {
	Date getDateFrom();
	void setDateFrom(Date date);
	
	Date getDateTo();
	void setDateTo(Date date);
}
