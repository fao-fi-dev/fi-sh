/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.auth;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 30 mar 2015
 */
public interface UserModel {
	String getId();
	
	@ApiModelProperty(hidden=true)
	default String getUsername() { return getId(); }
	
	default boolean can(String... capabilities) { return false; }
	default boolean is(String... roles) { return false; }
	
	@ApiModelProperty(hidden=true)
	default String[] listRoles() { return new String[0]; }
	
	@ApiModelProperty(hidden=true)
	default String[] listCapabilities() { return new String[0]; }
}
