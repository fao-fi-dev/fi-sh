/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.properties;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlRootElement(name="stringProperty")
final public class StringProperty extends BasicProperty<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6533300062665669835L;

	/**
	 * Class constructor
	 *
	 */
	public StringProperty() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param value
	 */
	public StringProperty(String name, String value) {
		super(name, value);
	}
}
