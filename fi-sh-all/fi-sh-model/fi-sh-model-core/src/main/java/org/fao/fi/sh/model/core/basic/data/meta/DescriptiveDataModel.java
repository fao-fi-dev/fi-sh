/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
@XmlRootElement(name="descriptiveDataModel")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for descriptive Data models")
public class DescriptiveDataModel extends DataModel<DataMetadata, DescriptiveDataModelProperty> {
	/**
	 * Class constructor
	 *
	 */
	public DescriptiveDataModel() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param metadata
	 * @param properties
	 */
	public DescriptiveDataModel(DataMetadata metadata, List<DescriptiveDataModelProperty> properties) {
		super(metadata, properties); // TODO Auto-generated constructor block
	}
}