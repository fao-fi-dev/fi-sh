/**
 * (c) 2015 FAO / UN (project: fis-persistence-mybatis-support)
 */
package org.fao.fi.sh.model.core.spi.persistence;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Apr 2015
 */
public interface KeyedData<ID> extends Data {
}