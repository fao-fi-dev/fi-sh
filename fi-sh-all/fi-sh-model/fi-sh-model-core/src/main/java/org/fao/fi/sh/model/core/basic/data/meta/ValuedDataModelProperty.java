/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
@XmlRootElement(name="valuedDataProperty")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for valued Data properties specifications")
public class ValuedDataModelProperty extends DataModelProperty {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2051648045052757725L;
	
	@XmlElement(name="value")
	@ApiModelProperty(position=1, value="The property value", required=false)
	private String value;

	/**
	 * Class constructor
	 *
	 */
	public ValuedDataModelProperty() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param value
	 * @param name
	 * @param type
	 * @param description
	 * @param key
	 * @param nullable
	 */
	public ValuedDataModelProperty(String value, String name, String type, String description, boolean key, boolean nullable) {
		super(name, type, description, key, nullable); // TODO Auto-generated constructor block
		this.value = value;
	}

	/**
	 * @return the 'value' value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * @param value the 'value' value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.value == null ) ? 0 : this.value.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		ValuedDataModelProperty other = (ValuedDataModelProperty) obj;
		if(this.value == null) {
			if(other.value != null) return false;
		} else if(!this.value.equals(other.value)) return false;
		return true;
	}
}
