/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.mappings;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Apr 2015
 */
@XmlType(name="mappingTarget")
@XmlAccessorType(XmlAccessType.FIELD)
public class MappingTarget<REFERENCE> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7629806813238106524L;

	@XmlAttribute(name="target") protected String _target;
	@XmlAttribute(name="version") protected String _version;
	@XmlAttribute(name="score") protected double _score;

	@XmlElement(name="targetReferenceModel") protected REFERENCE _targetReferenceModel;
	
	/**
	 * Class constructor
	 *
	 */
	public MappingTarget() {
	}

	/**
	 * Class constructor
	 *
	 * @param target
	 * @param version
	 * @param score
	 * @param targetReferenceModel
	 */
	public MappingTarget(String target, String version, double score, REFERENCE targetReferenceModel) {
		super();
		this._target = target;
		this._version = version;
		this._score = score;
		this._targetReferenceModel = targetReferenceModel;
	}

	/**
	 * @return the 'target' value
	 */
	public String getTarget() {
		return this._target;
	}

	/**
	 * @param target the 'target' value to set
	 */
	public void setTarget(String target) {
		this._target = target;
	}

	/**
	 * @return the 'version' value
	 */
	public String getVersion() {
		return this._version;
	}

	/**
	 * @param version the 'version' value to set
	 */
	public void setVersion(String version) {
		this._version = version;
	}

	/**
	 * @return the 'score' value
	 */
	public double getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public void setScore(double score) {
		this._score = score;
	}

	/**
	 * @return the 'targetReferenceModel' value
	 */
	public REFERENCE getTargetReferenceModel() {
		return this._targetReferenceModel;
	}

	/**
	 * @param targetReferenceModel the 'targetReferenceModel' value to set
	 */
	public void setTargetReferenceModel(REFERENCE targetReferenceModel) {
		this._targetReferenceModel = targetReferenceModel;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(this._score);
		result = prime * result + (int) ( temp ^ ( temp >>> 32 ) );
		result = prime * result + ( ( this._target == null ) ? 0 : this._target.hashCode() );
		result = prime * result + ( ( this._targetReferenceModel == null ) ? 0 : this._targetReferenceModel.hashCode() );
		result = prime * result + ( ( this._version == null ) ? 0 : this._version.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		MappingTarget other = (MappingTarget) obj;
		if(Double.doubleToLongBits(this._score) != Double.doubleToLongBits(other._score)) return false;
		if(this._target == null) {
			if(other._target != null) return false;
		} else if(!this._target.equals(other._target)) return false;
		if(this._targetReferenceModel == null) {
			if(other._targetReferenceModel != null) return false;
		} else if(!this._targetReferenceModel.equals(other._targetReferenceModel)) return false;
		if(this._version == null) {
			if(other._version != null) return false;
		} else if(!this._version.equals(other._version)) return false;
		return true;
	}
}
