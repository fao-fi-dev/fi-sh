/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.upload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.validation.BusinessValidationResult;
import org.fao.fi.sh.model.core.spi.Form;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2015
 */
@XmlRootElement(name="formUploadResult")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for form upload results")
public class FormUploadResult<FORM extends Form> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9216471158739942510L;

//	@XmlElement(name="form", nillable=false)
	@XmlAnyElement
	private FORM form;
	
	@XmlElementWrapper(name="uploadValidationResults")
	@XmlElement(name="uploadValidationResult")
	private List<UploadValidationResult> uploadValidationResults;
	
	@XmlElementWrapper(name="businessValidationResults")
	@XmlElement(name="businessValidationResult")
	private List<BusinessValidationResult> businessValidationResults;
	
	/**
	 * Class constructor
	 *
	 */
	public FormUploadResult() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param form
	 */
	public FormUploadResult(FORM form) {
		this(form, new ArrayList<UploadValidationResult>(), new ArrayList<BusinessValidationResult>());
	}

	/**
	 * Class constructor
	 *
	 * @param form
	 * @param uploadValidationResults
	 * @param businessValidationResults
	 */
	public FormUploadResult(FORM form, List<UploadValidationResult> uploadValidationResults, List<BusinessValidationResult> businessValidationResults) {
		super();
		this.form = form;
		this.uploadValidationResults = uploadValidationResults;
		this.businessValidationResults = businessValidationResults;
	}

	/**
	 * @return the 'form' value
	 */
	public FORM getForm() {
		return this.form;
	}

	/**
	 * @param form the 'form' value to set
	 */
	public void setForm(FORM form) {
		this.form = form;
	}

	/**
	 * @return the 'uploadValidationResults' value
	 */
	public List<UploadValidationResult> getUploadValidationResults() {
		return this.uploadValidationResults;
	}

	/**
	 * @param uploadValidationResults the 'uploadValidationResults' value to set
	 */
	public void setUploadValidationResults(List<UploadValidationResult> uploadValidationResults) {
		this.uploadValidationResults = uploadValidationResults;
	}
	
	/**
	 * @return the 'businessValidationResults' value
	 */
	public List<BusinessValidationResult> getBusinessValidationResults() {
		return this.businessValidationResults;
	}

	/**
	 * @param businessValidationResults the 'businessValidationResults' value to set
	 */
	public void setBusinessValidationResults(List<BusinessValidationResult> businessValidationResults) {
		this.businessValidationResults = businessValidationResults;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.businessValidationResults == null ) ? 0 : this.businessValidationResults.hashCode() );
		result = prime * result + ( ( this.form == null ) ? 0 : this.form.hashCode() );
		result = prime * result + ( ( this.uploadValidationResults == null ) ? 0 : this.uploadValidationResults.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		FormUploadResult other = (FormUploadResult) obj;
		if(this.businessValidationResults == null) {
			if(other.businessValidationResults != null) return false;
		} else if(!this.businessValidationResults.equals(other.businessValidationResults)) return false;
		if(this.form == null) {
			if(other.form != null) return false;
		} else if(!this.form.equals(other.form)) return false;
		if(this.uploadValidationResults == null) {
			if(other.uploadValidationResults != null) return false;
		} else if(!this.uploadValidationResults.equals(other.uploadValidationResults)) return false;
		return true;
	}
}
