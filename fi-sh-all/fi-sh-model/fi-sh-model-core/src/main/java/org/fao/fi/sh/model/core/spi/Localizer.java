/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.io.Serializable;


/**
 * Common interface for all entities that provide references to a language and to a country code (for localization purposes).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface Localizer<ID> extends Serializable, LanguageLocalizer, CountryLocalizer {
	ID getParentId();
	void setParentId(ID id);
}