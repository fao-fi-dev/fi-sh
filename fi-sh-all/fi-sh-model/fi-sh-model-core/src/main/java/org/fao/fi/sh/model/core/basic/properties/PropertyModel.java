/**
 * (c) 2015 FAO / UN (project: fis-model-common)
 */
package org.fao.fi.sh.model.core.basic.properties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlType(name="propertyModel")
@XmlAccessorType(XmlAccessType.FIELD)
public class PropertyModel<VALUE extends Serializable, MODEL extends BasicProperty<VALUE>, SELF extends PropertyModel<VALUE, MODEL, SELF>> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5580378624687226726L;
	
	static final public String ID_PROPERTY = "id"; 
	
	@XmlElementWrapper(name="modelProperties")
	@XmlElement(name="modelProperty")
	protected List<MODEL> _properties;

	/**
	 * Class constructor
	 *
	 */
	public PropertyModel() {
		this(new ArrayList<MODEL>());
	}

	/**
	 * Class constructor
	 *
	 * @param properties
	 */
	public PropertyModel(List<MODEL> properties) {
		super();
		this.setProperties(properties);
	}
	
	@SuppressWarnings("unchecked")
	public SELF addProperty(MODEL property) {
		if(property == null) throw new IllegalArgumentException("The property cannot be null");
		if(property.getName() == null) throw new IllegalArgumentException("The property cannot have a null name");
		
		this._properties.add(property);
		
		return (SELF)this;
	}
	
	final public MODEL findProperty(String name) {
		if(name == null) throw new IllegalArgumentException("The property cannot have a null name");
		
		for(MODEL in : _properties) {
			if(in == null) throw new IllegalArgumentException("The property cannot be null");
			
			if(name.equals(in.getName()))
				return in;
		}
		
		return null;
	}
	
	final public boolean hasProperty(String name) {
		return findProperty(name) != null;
	}

	/**
	 * @return the 'properties' value
	 */
	final public List<MODEL> getProperties() {
		return this._properties;
	}

	/**
	 * @param properties the 'properties' value to set
	 */
	final public void setProperties(List<MODEL> properties) {
		if(properties == null) throw new IllegalArgumentException("Backing properties list cannot be null");
		
		this._properties = properties;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this._properties == null ) ? 0 : this._properties.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		PropertyModel other = (PropertyModel) obj;
		if(this._properties == null) {
			if(other._properties != null) return false;
		} else if(!this._properties.equals(other._properties)) return false;
		return true;
	}
}
