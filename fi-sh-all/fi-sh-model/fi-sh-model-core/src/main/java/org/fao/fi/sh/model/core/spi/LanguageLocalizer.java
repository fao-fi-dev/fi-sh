/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

/**
 * Common interface for all entities that provide references to a language code (for localization purposes).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface LanguageLocalizer extends Named, Described {
	String getSysLangI18nId();
	void setSysLangI18nId(String sysLangI18nId);
}
