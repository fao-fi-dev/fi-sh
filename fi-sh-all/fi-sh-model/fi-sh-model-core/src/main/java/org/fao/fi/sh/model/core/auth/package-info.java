@javax.xml.bind.annotation.XmlSchema(namespace = "http://auth.core.model.sh.fi.fao.org", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package org.fao.fi.sh.model.core.auth;