/**
 * (c) 2015 FAO / UN (project: fis-model-common)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Apr 2015
 */
@XmlRootElement(name="dataMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for Data metadata")
public class DataMetadata implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6609666015767415677L;
	
	@ApiModelProperty(position=1, required=true, value="The Data name")
	@XmlElement private String name;
	
	@ApiModelProperty(position=2, required=false, value="The Data description")
	@XmlElement private String description;
	
	/**
	 * Class constructor
	 *
	 */
	public DataMetadata() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param description
	 */
	public DataMetadata(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the 'description' value
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description the 'description' value to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.description == null ) ? 0 : this.description.hashCode() );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		DataMetadata other = (DataMetadata) obj;
		if(this.description == null) {
			if(other.description != null) return false;
		} else if(!this.description.equals(other.description)) return false;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		return true;
	}
}