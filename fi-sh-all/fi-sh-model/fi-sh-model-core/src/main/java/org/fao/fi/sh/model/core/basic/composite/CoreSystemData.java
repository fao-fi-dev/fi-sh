/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import javax.xml.bind.annotation.XmlElement;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Apr 2015
 */
abstract public class CoreSystemData extends CoreData<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7406383392030659389L;
	
	@XmlElement(name="id", required=true, nillable=false)
	protected String id;

	/**
	 * @return the 'id' value
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @param id the 'id' value to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
