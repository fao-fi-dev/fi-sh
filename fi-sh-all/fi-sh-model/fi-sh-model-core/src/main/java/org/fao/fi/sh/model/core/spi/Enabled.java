/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;


/**
 * Common interface for all entities having a serializable code.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface Enabled {
	Boolean getEnabled();
	void setEnabled(Boolean enabled);
}
