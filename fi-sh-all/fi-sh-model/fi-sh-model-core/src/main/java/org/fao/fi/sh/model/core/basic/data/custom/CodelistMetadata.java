/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.custom;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.data.meta.DataMetadata;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Apr 2015
 */
@XmlRootElement(name="codelistMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for Codelist metadata")
public class CodelistMetadata extends DataMetadata {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6611278119109259900L;
	
	@ApiModelProperty(position=10, required=true, value="The number of entries in the codelist")
	@XmlAttribute(name="totalEntries") private int totalEntries;
	
	@ApiModelProperty(position=11, required=true, value="The number of enabled entries in the codelist")
	@XmlAttribute(name="enabledEntries") private int enabledEntries;

	/**
	 * Class constructor
	 *
	 */
	public CodelistMetadata() {
		super(); // TODO Auto-generated constructor block
	}
	
	/**
	 * Class constructor
	 *
	 * @param name
	 * @param description
	 * @param totalEntries
	 * @param enabledEntries
	 */
	public CodelistMetadata(String name, String description, int totalEntries, int enabledEntries) {
		super(name, description);
		this.totalEntries = totalEntries;
		this.enabledEntries = enabledEntries;
	}

	/**
	 * @return the 'totalEntries' value
	 */
	public int getTotalEntries() {
		return this.totalEntries;
	}

	/**
	 * @param totalEntries the 'totalEntries' value to set
	 */
	public void setTotalEntries(int totalEntries) {
		this.totalEntries = totalEntries;
	}

	/**
	 * @return the 'enabledEntries' value
	 */
	public int getEnabledEntries() {
		return this.enabledEntries;
	}

	/**
	 * @param enabledEntries the 'enabledEntries' value to set
	 */
	public void setEnabledEntries(int enabledEntries) {
		this.enabledEntries = enabledEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.enabledEntries;
		result = prime * result + this.totalEntries;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		CodelistMetadata other = (CodelistMetadata) obj;
		if(this.enabledEntries != other.enabledEntries) return false;
		if(this.totalEntries != other.totalEntries) return false;
		return true;
	}
	
	
}