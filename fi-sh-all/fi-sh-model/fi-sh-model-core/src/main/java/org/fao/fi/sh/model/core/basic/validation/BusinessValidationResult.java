/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.validation;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.spi.Severity;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
@XmlRootElement(name="businessValidationResult")
@XmlAccessorType(XmlAccessType.FIELD)
final public class BusinessValidationResult implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6658524157680515079L;
	
	@XmlAttribute(name="severity")
	private Severity severity;

	@XmlElement(name="triggeredRule")
	private BusinessValidationRule triggeredRule;
	
	@XmlElement(name="message")
	private String message;
	
	/**
	 * Class constructor
	 *
	 */
	public BusinessValidationResult() {
	}

	/**
	 * Class constructor
	 *
	 * @param severity
	 * @param triggeredRule
	 * @param message
	 */
	public BusinessValidationResult(Severity severity, BusinessValidationRule triggeredRule, String message) {
		super();
		this.severity = severity;
		this.triggeredRule = triggeredRule;
		this.message = message;
	}

	/**
	 * @return the 'severity' value
	 */
	public Severity getSeverity() {
		return this.severity;
	}

	/**
	 * @param severity the 'severity' value to set
	 */
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	/**
	 * @return the 'triggeredRule' value
	 */
	public BusinessValidationRule getTriggeredRule() {
		return this.triggeredRule;
	}

	/**
	 * @param triggeredRule the 'triggeredRule' value to set
	 */
	public void setTriggeredRule(BusinessValidationRule triggeredRule) {
		this.triggeredRule = triggeredRule;
	}

	/**
	 * @return the 'message' value
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * @param message the 'message' value to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasWarnings() {
		return has(Severity.WARNING);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasErrors() {
		return has(Severity.ERROR);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasFatals() {
		return has(Severity.FATAL);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasFailed() {
		return hasErrors() || hasFatals();
	}
	
	@ApiModelProperty(hidden=true)
	public boolean has(Severity severity) {
		return this.severity.equals(severity);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.message == null ) ? 0 : this.message.hashCode() );
		result = prime * result + ( ( this.severity == null ) ? 0 : this.severity.hashCode() );
		result = prime * result + ( ( this.triggeredRule == null ) ? 0 : this.triggeredRule.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		BusinessValidationResult other = (BusinessValidationResult) obj;
		if(this.message == null) {
			if(other.message != null) return false;
		} else if(!this.message.equals(other.message)) return false;
		if(this.severity != other.severity) return false;
		if(this.triggeredRule == null) {
			if(other.triggeredRule != null) return false;
		} else if(!this.triggeredRule.equals(other.triggeredRule)) return false;
		return true;
	}
}
