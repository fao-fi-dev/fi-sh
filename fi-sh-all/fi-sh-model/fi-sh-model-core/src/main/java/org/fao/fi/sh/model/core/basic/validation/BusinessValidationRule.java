/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.validation;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.spi.Described;
import org.fao.fi.sh.model.core.spi.Form;
import org.fao.fi.sh.model.core.spi.Identified;
import org.fao.fi.sh.model.core.spi.Named;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
@XmlType(name="businessValidationRule")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class BusinessValidationRule implements Serializable, Identified<String>, Named, Described, Comparable<BusinessValidationRule> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4451762878555631660L;

	@XmlAttribute(name="id") protected String id;
	@XmlAttribute(name="priority") protected int priority;
	@XmlElement(name="name") protected String name;
	@XmlElement(name="description") protected String description;
	
	public BusinessValidationRule() { }
	
	abstract public boolean appliesTo(Form form);
	abstract public boolean canBeAppliedTo(Form form);
	abstract public boolean isOptionalFor(Form form);
	
	abstract public List<BusinessValidationResult> validate(Form form) throws Exception;

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	final public int compareTo(BusinessValidationRule o) {
		return priority - o.priority;
	}
	
	/**
	 * @return the 'id' value
	 */
	final public String getId() {
		return this.id;
	}
	/**
	 * @param id the 'id' value to set
	 */
	final public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the 'priority' value
	 */
	final public int getPriority() {
		return this.priority;
	}

	/**
	 * @param priority the 'priority' value to set
	 */
	final public void setPriority(int priority) {
		if(priority < 0) throw new IllegalArgumentException("Validation rules priorities must be >= 0 (currently: " + priority + ")");
		
		this.priority = priority;
	}

	/**
	 * @return the 'name' value
	 */
	final public String getName() {
		return this.name;
	}
	
	/**
	 * @param name the 'name' value to set
	 */
	final public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the 'description' value
	 */
	final public String getDescription() {
		return this.description;
	}
	
	/**
	 * @param description the 'description' value to set
	 */
	final public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.description == null ) ? 0 : this.description.hashCode() );
		result = prime * result + ( ( this.id == null ) ? 0 : this.id.hashCode() );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		BusinessValidationRule other = (BusinessValidationRule) obj;
		if(this.description == null) {
			if(other.description != null) return false;
		} else if(!this.description.equals(other.description)) return false;
		if(this.id == null) {
			if(other.id != null) return false;
		} else if(!this.id.equals(other.id)) return false;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		return true;
	}
}
