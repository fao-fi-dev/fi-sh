/**
 * (c) 2015 FAO / UN (project: fis-model-common)
 */
package org.fao.fi.sh.model.core.basic.properties;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlRootElement(name="stringPropertyModel")
final public class StringPropertyModel extends PropertyModel<String, StringProperty, StringPropertyModel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5580378624687226726L;

	/**
	 * Class constructor
	 *
	 */
	public StringPropertyModel() {
		super(new ArrayList<StringProperty>());
	}

	/**
	 * Class constructor
	 *
	 * @param properties
	 */
	public StringPropertyModel(List<StringProperty> properties) {
		super(properties); // TODO Auto-generated constructor block
	}
}