/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

/**
 * Common interface for all entities that provide references to a country code (for localization purposes).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface CountryLocalizer extends Named, Described {
	String getSysCountryI18nId();
	void setSysCountryI18nId(String sysCountryI18nId);
}
