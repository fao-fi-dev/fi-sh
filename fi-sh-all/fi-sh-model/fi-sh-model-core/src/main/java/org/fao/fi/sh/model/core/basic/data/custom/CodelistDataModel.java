/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.custom;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.data.meta.DataModel;
import org.fao.fi.sh.model.core.basic.data.meta.DescriptiveDataModelProperty;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Apr 2015
 */
@XmlRootElement(name="codelistDataModel")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for Codelist Data models")
public class CodelistDataModel extends DataModel<CodelistMetadata, DescriptiveDataModelProperty> {

	/**
	 * Class constructor
	 *
	 */
	public CodelistDataModel() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param metadata
	 * @param properties
	 */
	public CodelistDataModel(CodelistMetadata metadata, List<DescriptiveDataModelProperty> properties) {
		super(metadata, properties); // TODO Auto-generated constructor block
	}
}