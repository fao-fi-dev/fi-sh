/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.mappings;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.basic.properties.StringPropertyModel;
import org.fao.fi.sh.model.core.spi.Mapped;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlRootElement(name="stringPropertyModelMapping")
public class StringPropertyModelMapping<ID extends Serializable, CODELIST extends CoreCodelist<ID, ?> & Mapped> extends Mapping<ID, CODELIST, StringPropertyModel, StringPropertyModelMappingTarget> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4180707137628306184L;

	/**
	 * Class constructor
	 *
	 */
	public StringPropertyModelMapping() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param version
	 * @param entry
	 */
	public StringPropertyModelMapping(String source, String version, CODELIST entry) {
		super(source, version, entry); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param version
	 * @param entry
	 * @param targets
	 */
	public StringPropertyModelMapping(String source, String version, CODELIST entry, List<StringPropertyModelMappingTarget> targets) {
		super(source, version, entry, targets); // TODO Auto-generated constructor block
	}
}
