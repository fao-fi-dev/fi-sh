/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.upload;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.spi.Severity;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2015
 */
@XmlRootElement(name="uploadValidationMessage")
@XmlAccessorType(XmlAccessType.FIELD)
public class UploadValidationMessage implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7573914047424259918L;

	@XmlAttribute(name="severity")
	private Severity severity;
	
	@XmlElement(name="message")
	private String message;
	
	/**
	 * Class constructor
	 *
	 */
	public UploadValidationMessage() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param severity
	 * @param message
	 */
	public UploadValidationMessage(Severity severity, String message) {
		super();
		this.severity = severity;
		this.message = message;
	}

	/**
	 * @return the 'severity' value
	 */
	public Severity getSeverity() {
		return this.severity;
	}

	/**
	 * @param severity the 'severity' value to set
	 */
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}

	/**
	 * @return the 'message' value
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * @param message the 'message' value to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.message == null ) ? 0 : this.message.hashCode() );
		result = prime * result + ( ( this.severity == null ) ? 0 : this.severity.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		UploadValidationMessage other = (UploadValidationMessage) obj;
		if(this.message == null) {
			if(other.message != null) return false;
		} else if(!this.message.equals(other.message)) return false;
		if(this.severity != other.severity) return false;
		return true;
	}
}
