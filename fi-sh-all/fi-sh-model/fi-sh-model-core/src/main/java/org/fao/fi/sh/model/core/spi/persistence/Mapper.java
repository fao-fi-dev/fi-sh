/**
 * (c); 2015 FAO / UN (project: fis-persistence-mybatis-support);
 */
package org.fao.fi.sh.model.core.spi.persistence;

import java.util.List;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Apr 2015
 */
public interface Mapper<DATA extends Data, EXAMPLE extends Example<DATA>> {
	int countByExample(EXAMPLE example);
	int deleteByExample(EXAMPLE example);
	int insert(DATA data);
	int insertSelective(DATA data);
	List<DATA> selectByExample(EXAMPLE example);
	int updateByExample(DATA data, EXAMPLE example);
	int updateByExampleSelective(DATA data, EXAMPLE example);
}
