/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.io.Serializable;
import java.util.List;

/**
 * Common interface for all entities that are localized.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 26 Mar 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 26 Mar 2015
 */
public interface Localized<LOCALIZER extends LanguageLocalizer> extends Serializable, Named, Described {
	List<LOCALIZER> getLocalization();
	void setLocalization(List<LOCALIZER> localization);
}