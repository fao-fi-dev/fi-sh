/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.spi.UpdatableCommentable;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Base class for updatable and commentable data.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
abstract public class CoreUpdatableData extends AbstractJAXBSerializableData implements UpdatableCommentable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4286175703386424015L;

	@XmlElement(name = "updaterId", required = true, nillable = false)
	@ApiModelProperty(position = 100, required = true, value = "The updater identifier")
	@ModelPropertyMetadata(isKey = false, name = "updaterId", type = "String", isNullable = false, description = "The updater identifier")
	protected String updaterId;

	@XmlElement(name = "updateDate", required = true, nillable = false)
	@ApiModelProperty(position = 101, required = true, value = "The date of last update for the data")
	@ModelPropertyMetadata(isKey = false, name = "updateDate", type = "Date", isNullable = false, description = "The date of last update for the data")
	protected Date updateDate;

	@XmlElement(name = "comment", required = false, nillable = true)
	@ApiModelProperty(position = 102, required = false, value = "A free text comment")
	@ModelPropertyMetadata(isKey = false, name = "comment", type = "String", isNullable = true, description = "A free text comment")
	protected String comment;
	
	/**
	 * Class constructor
	 *
	 */
	public CoreUpdatableData() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param updaterId
	 * @param updateDate
	 * @param comment
	 */
	public CoreUpdatableData(String updaterId, Date updateDate, String comment) {
		super();
		this.updaterId = updaterId;
		this.updateDate = updateDate;
		this.comment = comment;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Updatable#getUpdaterId()
	 */
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Updatable#getUpdaterId()
	 */
	final public String getUpdaterId() {
		return this.updaterId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Updatable#setUpdaterId(java.lang.String)
	 */
	final public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Updatable#getUpdateDate()
	 */
	final public Date getUpdateDate() {
		return this.updateDate;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Updatable#setUpdateDate(java.util.Date)
	 */
	final public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Commentable#getComment()
	 */
	final public String getComment() {
		return this.comment;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Commentable#setComment(java.lang.String)
	 */
	final public void setComment(String comment) {
		this.comment = comment;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.comment == null ) ? 0 : this.comment.hashCode() );
		result = prime * result + ( ( this.updateDate == null ) ? 0 : this.updateDate.hashCode() );
		result = prime * result + ( ( this.updaterId == null ) ? 0 : this.updaterId.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		CoreUpdatableData other = (CoreUpdatableData) obj;
		if(this.comment == null) {
			if(other.comment != null) return false;
		} else if(!this.comment.equals(other.comment)) return false;
		if(this.updateDate == null) {
			if(other.updateDate != null) return false;
		} else if(!this.updateDate.equals(other.updateDate)) return false;
		if(this.updaterId == null) {
			if(other.updaterId != null) return false;
		} else if(!this.updaterId.equals(other.updaterId)) return false;
		return true;
	}
}