/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import javax.xml.bind.annotation.XmlEnum;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 Apr 2015
 */
@XmlEnum
public enum Severity {
	DEBUG("debug"),
	INFO("info"),
	WARNING("warning"),
	ERROR("error"),
	FATAL("fatal");
	
	private String type;
	
	private Severity(String type) {
		this.type = type;
	}
	/**
	 * @return the 'type' value
	 */
	public String getType() {
		return this.type;
	}
}
