/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.mappings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreCodelist;
import org.fao.fi.sh.model.core.spi.Mapped;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlType(name="mapping")
@XmlAccessorType(XmlAccessType.FIELD)
public class Mapping<ID extends Serializable, CODELIST extends CoreCodelist<ID, ?> & Mapped, REFERENCE, MAPPING_TARGET extends MappingTarget<REFERENCE>> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -3107951599731362340L;
	
	@XmlAttribute(name="source") protected String _source;
	@XmlAttribute(name="version") protected String _version;
	 
	@XmlElement(name="codelistEntry") 
	protected CODELIST entry;
	
	@XmlElementWrapper(name="mappingTargets")
	@XmlElement(name="mappingTarget")
	protected List<MAPPING_TARGET> targets;
	
	/**
	 * Class constructor
	 *
	 */
	public Mapping() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param version
	 * @param entry
	 * @param targets
	 */
	public Mapping(String source, String version, CODELIST entry) {
		this(source, version, entry, new ArrayList<MAPPING_TARGET>());
	}
	
	/**
	 * Class constructor
	 *
	 * @param source
	 * @param version
	 * @param entry
	 * @param targets
	 */
	public Mapping(String source, String version, CODELIST entry, List<MAPPING_TARGET> targets) {
		super();
		this._source = source;
		this._version = version;
		this.entry = entry;
		this.targets = targets;
	}

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}

	/**
	 * @return the 'version' value
	 */
	public String getVersion() {
		return this._version;
	}

	/**
	 * @param version the 'version' value to set
	 */
	public void setVersion(String version) {
		this._version = version;
	}

	/**
	 * @return the 'entry' value
	 */
	public CODELIST getEntry() {
		return this.entry;
	}

	/**
	 * @param entry the 'entry' value to set
	 */
	public void setEntry(CODELIST entry) {
		this.entry = entry;
	}

	/**
	 * @return the 'targets' value
	 */
	public List<MAPPING_TARGET> getTargets() {
		return this.targets;
	}

	/**
	 * @param targets the 'targets' value to set
	 */
	public void setTargets(List<MAPPING_TARGET> targets) {
		this.targets = targets;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.entry == null ) ? 0 : this.entry.hashCode() );
		result = prime * result + ( ( this._source == null ) ? 0 : this._source.hashCode() );
		result = prime * result + ( ( this.targets == null ) ? 0 : this.targets.hashCode() );
		result = prime * result + ( ( this._version == null ) ? 0 : this._version.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		Mapping other = (Mapping) obj;
		if(this.entry == null) {
			if(other.entry != null) return false;
		} else if(!this.entry.equals(other.entry)) return false;
		if(this._source == null) {
			if(other._source != null) return false;
		} else if(!this._source.equals(other._source)) return false;
		if(this.targets == null) {
			if(other.targets != null) return false;
		} else if(!this.targets.equals(other.targets)) return false;
		if(this._version == null) {
			if(other._version != null) return false;
		} else if(!this._version.equals(other._version)) return false;
		return true;
	}
}
