/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.upload;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.spi.Severity;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2015
 */
@XmlRootElement(name="uploadValidationResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class UploadValidationResult implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1250032821506718222L;

	@XmlAttribute(name="rowNum")
	private int rowNum;
	
	@XmlAttribute(name="colNum")
	private int colNum;
	
	@XmlElementWrapper(name="validationMessages")
	@XmlElement(name="validationMessage")
	private List<UploadValidationMessage> validationMessages;
	
	/**
	 * Class constructor
	 *
	 */
	public UploadValidationResult() {
		// TODO Auto-generated constructor block
	}
	
	/**
	 * Class constructor
	 *
	 * @param rowNum
	 * @param colNum
	 * @param validationMessages
	 */
	public UploadValidationResult(int rowNum, int colNum, List<UploadValidationMessage> validationMessages) {
		super();
		this.rowNum = rowNum;
		this.colNum = colNum;
		this.validationMessages = validationMessages;
	}
	
	/**
	 * Class constructor
	 *
	 * @param rowNum
	 * @param colNum
	 * @param validationMessages
	 */
	public UploadValidationResult(int rowNum, int colNum, UploadValidationMessage validationMessage) {
		super();
		this.rowNum = rowNum;
		this.colNum = colNum;
		this.validationMessages = new ArrayList<UploadValidationMessage>();
		
		this.validationMessages.add(validationMessage);
	}

	/**
	 * @return the 'rowNum' value
	 */
	public int getRowNum() {
		return this.rowNum;
	}

	/**
	 * @param rowNum the 'rowNum' value to set
	 */
	public void setRowNum(int rowNum) {
		this.rowNum = rowNum;
	}

	/**
	 * @return the 'colNum' value
	 */
	public int getColNum() {
		return this.colNum;
	}

	/**
	 * @param colNum the 'colNum' value to set
	 */
	public void setColNum(int colNum) {
		this.colNum = colNum;
	}

	/**
	 * @return the 'validationMessages' value
	 */
	public List<UploadValidationMessage> getValidationMessages() {
		return this.validationMessages;
	}

	/**
	 * @param validationMessages the 'validationMessages' value to set
	 */
	public void setValidationMessages(List<UploadValidationMessage> validationMessages) {
		this.validationMessages = validationMessages;
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasWarnings() {
		return has(Severity.WARNING);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasErrors() {
		return has(Severity.ERROR);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasFatals() {
		return has(Severity.FATAL);
	}
	
	@ApiModelProperty(hidden=true)
	public boolean hasFailed() {
		return hasErrors() || hasFatals();
	}
	
	@ApiModelProperty(hidden=true)
	public boolean has(Severity severity) {
		if(validationMessages != null) {
			for(UploadValidationMessage in : validationMessages) { 
				if(severity.equals(in.getSeverity()))
					return true;
			}
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.colNum;
		result = prime * result + this.rowNum;
		result = prime * result + ( ( this.validationMessages == null ) ? 0 : this.validationMessages.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		UploadValidationResult other = (UploadValidationResult) obj;
		if(this.colNum != other.colNum) return false;
		if(this.rowNum != other.rowNum) return false;
		if(this.validationMessages == null) {
			if(other.validationMessages != null) return false;
		} else if(!this.validationMessages.equals(other.validationMessages)) return false;
		return true;
	}
}
