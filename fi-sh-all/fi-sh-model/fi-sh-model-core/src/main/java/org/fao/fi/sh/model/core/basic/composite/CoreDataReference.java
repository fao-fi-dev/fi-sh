/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;


/**
 * Base class for core data reference entities (tables whose name is in the form DT_%_TO_%).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
abstract public class CoreDataReference extends CoreUpdatableData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4286175703386424015L;
}