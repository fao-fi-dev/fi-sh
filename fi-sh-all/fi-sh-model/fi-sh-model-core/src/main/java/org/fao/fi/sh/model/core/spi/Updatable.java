/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.util.Date;

/**
 * Common interface for all entities whose updates should be tracked.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface Updatable {
	String getUpdaterId();
	void setUpdaterId(String updaterId);

	Date getUpdateDate();
	void setUpdateDate(Date updateDate);
}
