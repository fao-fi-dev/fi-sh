/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
abstract public class CoreLocalizedCodelist<ID extends Serializable, CODE extends Serializable, LOCALIZER extends CoreCodelistLocalizer<ID>> extends CoreCodelist<ID, CODE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4777127937081312848L;
	
	@XmlElementWrapper(name="localizations")
	@XmlElement(name="localization")
	protected List<LOCALIZER> localization = new ArrayList<LOCALIZER>();
	
	final protected void setLocalization(List<LOCALIZER> localization) {
		this.localization = localization == null ? new ArrayList<LOCALIZER>() : localization;
	}
	
	final protected List<LOCALIZER> getLocalization() {
		return this.localization == null ? new ArrayList<LOCALIZER>() : localization;
	}
	
	final public String getLocalizedName(String language) {
		return this.getLocalizedName(language, null);
	}
	
	final public String getLocalizedName(String language, String country) {
		Optional<LOCALIZER> localizer = this.localize(language, country);
		
		return localizer.isPresent() ? localizer.get().getName() : this.getName();
	}
	
	final public String getLocalizedDescription(String language) {
		return this.getLocalizedDescription(language, null);
	}
	
	final public String getLocalizedDescription(String language, String country) {
		Optional<LOCALIZER> localizer = this.localize(language, country);
		
		return localizer.isPresent() ? localizer.get().getDescription() : this.getDescription();
	}
	
	final private Optional<LOCALIZER> localize(String language, String country) {
		Stream<LOCALIZER> byLanguage = 
			localization.
				stream().
					filter(L -> language.equals(L.getSysLangI18nId())).
						sorted((L1, L2) -> L1.getSysCountryI18nId() == null ? -1 : L2.getSysCountryI18nId() == null ? 1 : L1.getSysCountryI18nId().compareTo(L2.getSysCountryI18nId()));
		
		Stream<LOCALIZER> byCountry = 
			byLanguage.
				filter(L -> country == null || country.equals(L.getSysCountryI18nId()));
		
		try {
			return ( country == null || !byCountry.findFirst().isPresent() ? byLanguage : byCountry ).findFirst();
		} catch(NoSuchElementException NSEe) {
			return null;
		}
	}
}