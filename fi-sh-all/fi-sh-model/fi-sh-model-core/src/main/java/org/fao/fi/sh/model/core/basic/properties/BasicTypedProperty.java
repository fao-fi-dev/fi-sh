/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.properties;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.spi.TypedProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlType(name="typedProperty")
public class BasicTypedProperty<VALUE extends Serializable> extends BasicProperty<VALUE> implements TypedProperty<VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8333399910068981157L;
	
	@XmlAttribute(name="type") protected String type;
	@XmlAttribute(name="nullable") protected boolean isNullable;
	
	/**
	 * Class constructor
	 *
	 */
	public BasicTypedProperty() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param value
	 * @param type
	 * @param isNullable
	 */
	public BasicTypedProperty(String name, VALUE value, String type, boolean isNullable) {
		super(name, value);
		this.type = type;
		this.isNullable = isNullable;
	}

	/**
	 * @return the 'type' value
	 */
	final public String getType() {
		return this.type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	final public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the 'isNullable' value
	 */
	final public boolean isNullable() {
		return this.isNullable;
	}

	/**
	 * @param isNullable the 'isNullable' value to set
	 */
	final public void setNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( this.isNullable ? 1231 : 1237 );
		result = prime * result + ( ( this.type == null ) ? 0 : this.type.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		BasicTypedProperty other = (BasicTypedProperty) obj;
		if(this.isNullable != other.isNullable) return false;
		if(this.type == null) {
			if(other.type != null) return false;
		} else if(!this.type.equals(other.type)) return false;
		return true;
	}
}
