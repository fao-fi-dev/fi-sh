/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.io.Serializable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
public interface TypedProperty<VALUE extends Serializable> extends Property<VALUE> {
	String getType();
	void setType(String type);
	
	boolean isNullable();
	void setNullable(boolean isNullable);
}
