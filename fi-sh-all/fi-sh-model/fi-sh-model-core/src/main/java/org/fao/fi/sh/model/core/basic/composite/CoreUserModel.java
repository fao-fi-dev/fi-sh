/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import org.fao.fi.sh.model.core.auth.UserModel;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Apr 2015
 */
abstract public class CoreUserModel extends CoreSystemData implements UserModel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7406383392030659389L;
}
