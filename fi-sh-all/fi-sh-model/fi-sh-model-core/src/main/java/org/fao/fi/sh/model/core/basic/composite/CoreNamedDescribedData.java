/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.spi.Described;
import org.fao.fi.sh.model.core.spi.Named;

import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Base class for updatable and commentable data.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
abstract public class CoreNamedDescribedData<ID> extends CoreData<ID> implements Named, Described {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4286175703386424015L;

	@XmlElement(name = "name", required = true, nillable = false)
	@ApiModelProperty(position = 50, required = true, value = "The name assigned to this data")
	@ModelPropertyMetadata(isKey = false, name = "name", type = "String", isNullable = false, description = "The name assigned to this data")
	protected String name;
	
	@XmlElement(name = "description", required = false, nillable = true)
	@ApiModelProperty(position = 51, required = false, value = "The description assigned to this data")
	@ModelPropertyMetadata(isKey = false, name = "description", type = "String", isNullable = true, description = "The description assigned to this data")
	protected String description;

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Named#getName()
	 */
	final public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Named#setName(java.lang.String)
	 */
	final public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Described#getDescription()
	 */
	final public String getDescription() {
		return this.description;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Described#setDescription(java.lang.String)
	 */
	final public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.description == null ) ? 0 : this.description.hashCode() );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		CoreNamedDescribedData other = (CoreNamedDescribedData) obj;
		if(this.description == null) {
			if(other.description != null) return false;
		} else if(!this.description.equals(other.description)) return false;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		return true;
	}
}