/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

/**
 * Common interface for all entities having a name (free text).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
public interface Named {
	String getName();
	void setName(String name);
}
