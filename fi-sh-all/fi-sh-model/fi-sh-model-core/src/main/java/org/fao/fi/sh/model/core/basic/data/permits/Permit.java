/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.permits;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.basic.composite.CoreData;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Apr 2015
 */
@XmlType(name="permit")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="permit", description="The common Permit model")
abstract public class Permit<ID extends Serializable> extends CoreData<ID> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 6572405965327426624L;
	
	@XmlElement(name="permitNumber", required=true, nillable=false)
	@ApiModelProperty(position=10, value="The permit number", required=true)
	private String permitNumber;

	@XmlElement(name="applicationDate", required=true, nillable=false)
	@ApiModelProperty(position=11, value="The permit application date", required=true)
	private Date applicationDate;
	
	@XmlElement(name="permitDate", required=true, nillable=false)
	@ApiModelProperty(position=12, value="The permit issuing date", required=true)
	private Date permitDate;
	
	@XmlElement(name="dateFrom", required=true, nillable=false)
	@ApiModelProperty(position=13, value="The permit validity start", required=true)
	private Date dateFrom;
	
	@XmlElement(name="dateTo", required=false, nillable=true)
	@ApiModelProperty(position=14, value="The permit validity end", required=false)
	private Date dateTo;
		
	@XmlElement(name="specialConditions", required=false, nillable=true)
	@ApiModelProperty(position=15, value="The permit special conditions", required=false)
	private String specialConditions;

	/**
	 * Class constructor
	 *
	 */
	public Permit() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param permitNumber
	 * @param applicationDate
	 * @param permitDate
	 * @param dateFrom
	 * @param dateTo
	 * @param specialConditions
	 */
	public Permit(String permitNumber, Date requestDate, Date permitDate, Date dateFrom, Date dateTo, String specialConditions) {
		super();
		this.permitNumber = permitNumber;
		this.applicationDate = requestDate;
		this.permitDate = permitDate;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.specialConditions = specialConditions;
	}

	/**
	 * @return the 'permitNumber' value
	 */
	final public String getPermitNumber() {
		return this.permitNumber;
	}

	/**
	 * @param permitNumber the 'permitNumber' value to set
	 */
	final public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}

	/**
	 * @return the 'applicationDate' value
	 */
	final public Date getApplicationDate() {
		return this.applicationDate;
	}

	/**
	 * @param applicationDate the 'applicationDate' value to set
	 */
	final public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return the 'permitDate' value
	 */
	final public Date getPermitDate() {
		return this.permitDate;
	}

	/**
	 * @param permitDate the 'permitDate' value to set
	 */
	final public void setPermitDate(Date permitDate) {
		this.permitDate = permitDate;
	}

	/**
	 * @return the 'dateFrom' value
	 */
	final public Date getDateFrom() {
		return this.dateFrom;
	}

	/**
	 * @param dateFrom the 'dateFrom' value to set
	 */
	final public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the 'dateTo' value
	 */
	final public Date getDateTo() {
		return this.dateTo;
	}

	/**
	 * @param dateTo the 'dateTo' value to set
	 */
	final public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the 'specialConditions' value
	 */
	final public String getSpecialConditions() {
		return this.specialConditions;
	}

	/**
	 * @param specialConditions the 'specialConditions' value to set
	 */
	final public void setSpecialConditions(String specialConditions) {
		this.specialConditions = specialConditions;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.dateFrom == null ) ? 0 : this.dateFrom.hashCode() );
		result = prime * result + ( ( this.dateTo == null ) ? 0 : this.dateTo.hashCode() );
		result = prime * result + ( ( this.permitDate == null ) ? 0 : this.permitDate.hashCode() );
		result = prime * result + ( ( this.permitNumber == null ) ? 0 : this.permitNumber.hashCode() );
		result = prime * result + ( ( this.applicationDate == null ) ? 0 : this.applicationDate.hashCode() );
		result = prime * result + ( ( this.specialConditions == null ) ? 0 : this.specialConditions.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		Permit other = (Permit) obj;
		if(this.dateFrom == null) {
			if(other.dateFrom != null) return false;
		} else if(!this.dateFrom.equals(other.dateFrom)) return false;
		if(this.dateTo == null) {
			if(other.dateTo != null) return false;
		} else if(!this.dateTo.equals(other.dateTo)) return false;
		if(this.permitDate == null) {
			if(other.permitDate != null) return false;
		} else if(!this.permitDate.equals(other.permitDate)) return false;
		if(this.permitNumber == null) {
			if(other.permitNumber != null) return false;
		} else if(!this.permitNumber.equals(other.permitNumber)) return false;
		if(this.applicationDate == null) {
			if(other.applicationDate != null) return false;
		} else if(!this.applicationDate.equals(other.applicationDate)) return false;
		if(this.specialConditions == null) {
			if(other.specialConditions != null) return false;
		} else if(!this.specialConditions.equals(other.specialConditions)) return false;
		return true;
	}
}
