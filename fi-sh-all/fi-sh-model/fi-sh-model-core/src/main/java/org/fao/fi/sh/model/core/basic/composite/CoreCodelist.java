/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.spi.Described;
import org.fao.fi.sh.model.core.spi.Enabled;
import org.fao.fi.sh.model.core.spi.Localizable;
import org.fao.fi.sh.model.core.spi.Named;
import org.fao.fi.sh.model.core.spi.persistence.IdentifiedData;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;


/**
 * Base class for core codelist entities (tables prefixed by CL_).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
@ApiModel(value="codelist", description="The abstract container for codelist data")
abstract public class CoreCodelist<CODELIST_ID, CODELIST_CODE> extends CoreNamedDescribedData<CODELIST_ID> implements IdentifiedData<CODELIST_ID>, Localizable, Enabled, Named, Described {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4777127937081312848L;

	@XmlElement(name = "enabled", required = true, nillable = false)
	@ApiModelProperty(position = 2, required = true, value = "Tells whether this codelis entry is enabled or not")
	@ModelPropertyMetadata(isKey = false, name = "enabled", type = "Boolean", isNullable = false, description = "Tells whether this codelis entry is enabled or not")
	protected Boolean enabled;

	@XmlElement(name = "code", required = true, nillable = false)
	@ApiModelProperty(position = 3, required = true, value = "The codelist entry code")
	@ModelPropertyMetadata(isKey = false, name = "code", type = "String", isNullable = false, description = "The codelist entry code")
	protected CODELIST_CODE code;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Enabled#getEnabled()
	 */
	final public Boolean getEnabled() {
		return this.enabled;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Enabled#setEnabled(java.lang.Boolean)
	 */
	final public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Coded#getCode()
	 */
	public CODELIST_CODE getCode() {
		return this.code;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Coded#setCode(java.io.Serializable)
	 */
	public void setCode(CODELIST_CODE code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.code == null ) ? 0 : this.code.hashCode() );
		result = prime * result + ( ( this.enabled == null ) ? 0 : this.enabled.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		CoreCodelist other = (CoreCodelist) obj;
		if(this.code == null) {
			if(other.code != null) return false;
		} else if(!this.code.equals(other.code)) return false;
		if(this.enabled == null) {
			if(other.enabled != null) return false;
		} else if(!this.enabled.equals(other.enabled)) return false;
		return true;
	}
}