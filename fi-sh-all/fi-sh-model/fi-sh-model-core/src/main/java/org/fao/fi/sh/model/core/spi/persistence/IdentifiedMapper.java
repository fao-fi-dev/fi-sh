/**
 * (c); 2015 FAO / UN (project: fis-persistence-mybatis-support);
 */
package org.fao.fi.sh.model.core.spi.persistence;

import java.io.Serializable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 Apr 2015
 */
public interface IdentifiedMapper<ID extends Serializable, DATA extends IdentifiedData<ID>, EXAMPLE extends Example<DATA>> extends KeyedMapper<ID, DATA, EXAMPLE> {
}
