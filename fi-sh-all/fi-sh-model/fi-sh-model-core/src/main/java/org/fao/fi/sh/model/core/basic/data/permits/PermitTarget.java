/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.permits;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Apr 2015
 */
public interface PermitTarget<TARGET> {
}