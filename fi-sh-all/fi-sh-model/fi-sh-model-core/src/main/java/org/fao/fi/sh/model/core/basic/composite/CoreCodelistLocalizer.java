/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.composite;

import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.sh.model.core.annotations.ModelPropertyMetadata;
import org.fao.fi.sh.model.core.spi.Described;
import org.fao.fi.sh.model.core.spi.Localizer;
import org.fao.fi.sh.model.core.spi.Named;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Base class for codelist entities localizer (tables prefixed by CL_%_I18N).
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 mar 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since 22 mar 2015
 */
@ApiModel(value="codelistLocalizer", description="Localization data for a codelis entry")
abstract public class CoreCodelistLocalizer<CODELIST_ID> extends CoreUpdatableData implements Localizer<CODELIST_ID>, Named, Described {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8804530781530570978L;

	@XmlElement(name="parentId", required=true, nillable=false)
	@ApiModelProperty(required = true, value = "The reference to the localized codelist entry")
	@ModelPropertyMetadata(isKey = false, name = "parentId", type = "Integer", isNullable = false, description = "The reference to the localized codelist entry")
	protected CODELIST_ID parentId;

	@XmlElement(name = "sysLangI18nId", required = true, nillable = false)
	@ApiModelProperty(required = true, value = "The localization language")
	@ModelPropertyMetadata(isKey = false, name = "sysLangI18nId", type = "String", isNullable = false, description = "The localization language")
	protected String sysLangI18nId;

	@XmlElement(name = "sysCountryI18nId", required = false, nillable = true)
	@ApiModelProperty(required = false, value = "The localization country")
	@ModelPropertyMetadata(isKey = false, name = "sysCountryI18nId", type = "String", isNullable = true, description = "The localization country")
	protected String sysCountryI18nId;

	@XmlElement(name = "name", required = true, nillable = false)
	@ApiModelProperty(required = true, value = "The localized name")
	@ModelPropertyMetadata(isKey = false, name = "name", type = "String", isNullable = false, description = "The localized name")
	protected String name;

	@XmlElement(name = "description", required = true, nillable = false)
	@ApiModelProperty(required = true, value = "The localized description")
	@ModelPropertyMetadata(isKey = false, name = "description", type = "String", isNullable = false, description = "The localized description")
	protected String description;

	/**
	 * Class constructor
	 *
	 */
	public CoreCodelistLocalizer() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param parentId
	 * @param sysLangI18nId
	 * @param sysCountryI18nId
	 * @param name
	 * @param description
	 */
	public CoreCodelistLocalizer(CODELIST_ID parentId, String sysLangI18nId, String sysCountryI18nId, String name, String description) {
		super();
		this.parentId = parentId;
		this.sysLangI18nId = sysLangI18nId;
		this.sysCountryI18nId = sysCountryI18nId;
		this.name = name;
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Localizer#getParentId()
	 */
	public CODELIST_ID getParentId() {
		return this.parentId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Localizer#setParentId(java.io.Serializable)
	 */
	public void setParentId(CODELIST_ID parentId) {
		this.parentId = parentId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.LanguageLocalizer#getI18nLangId()
	 */
	final public String getSysLangI18nId() {
		return this.sysLangI18nId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.LanguageLocalizer#setI18nLangId(java.lang.String)
	 */
	final public void setSysLangI18nId(String i18nLangId) {
		this.sysLangI18nId = i18nLangId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.CountryLocalizer#getI18nCountryId()
	 */
	final public String getSysCountryI18nId() {
		return this.sysCountryI18nId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.CountryLocalizer#setI18nCountryId(java.lang.String)
	 */
	final public void setSysCountryI18nId(String i18nCountryId) {
		this.sysCountryI18nId = i18nCountryId;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Named#getName()
	 */
	final public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Named#setName(java.lang.String)
	 */
	final public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Described#getDescription()
	 */
	final public String getDescription() {
		return this.description;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Described#setDescription(java.lang.String)
	 */
	final public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( this.parentId == null ) ? 0 : this.parentId.hashCode() );
		result = prime * result + ( ( this.description == null ) ? 0 : this.description.hashCode() );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		result = prime * result + ( ( this.sysCountryI18nId == null ) ? 0 : this.sysCountryI18nId.hashCode() );
		result = prime * result + ( ( this.sysLangI18nId == null ) ? 0 : this.sysLangI18nId.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(!super.equals(obj)) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		CoreCodelistLocalizer other = (CoreCodelistLocalizer) obj;
		if(this.parentId == null) {
			if(other.parentId != null) return false;
		} else if(!this.parentId.equals(other.parentId)) return false;
		if(this.description == null) {
			if(other.description != null) return false;
		} else if(!this.description.equals(other.description)) return false;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		if(this.sysCountryI18nId == null) {
			if(other.sysCountryI18nId != null) return false;
		} else if(!this.sysCountryI18nId.equals(other.sysCountryI18nId)) return false;
		if(this.sysLangI18nId == null) {
			if(other.sysLangI18nId != null) return false;
		} else if(!this.sysLangI18nId.equals(other.sysLangI18nId)) return false;
		return true;
	}
}