/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.mappings;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.basic.properties.StringPropertyModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlRootElement(name="stringPropertyModelMappingTarget")
public class StringPropertyModelMappingTarget extends MappingTarget<StringPropertyModel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2162127143984609634L;

	/**
	 * Class constructor
	 *
	 */
	public StringPropertyModelMappingTarget() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param target
	 * @param version
	 * @param score
	 * @param targetReferenceModel
	 */
	public StringPropertyModelMappingTarget(String target, String version, double score, StringPropertyModel targetReferenceModel) {
		super(target, version, score, targetReferenceModel); 
	}
}
