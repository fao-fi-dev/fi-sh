/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.model.core.spi.Property;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Apr 2015
 */
@XmlType(name="property")
@XmlAccessorType(XmlAccessType.FIELD)
public class BasicProperty<VALUE> implements Property<VALUE> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2132847958391469768L;
	
	@XmlAttribute(name="name") protected String name;
	@XmlElement(name="value") protected VALUE value;
	
	/**
	 * Class constructor
	 *
	 */
	public BasicProperty() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param value
	 */
	public BasicProperty(String name, VALUE value) {
		super();
		this.name = name;
		this.value = value;
	}

	/**
	 * @return the 'name' value
	 */
	final public String getName() {
		return this.name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	final public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the 'value' value
	 */
	final public VALUE getValue() {
		return this.value;
	}

	/**
	 * @param value the 'value' value to set
	 */
	final public void setValue(VALUE value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		result = prime * result + ( ( this.value == null ) ? 0 : this.value.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		BasicProperty other = (BasicProperty) obj;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		if(this.value == null) {
			if(other.value != null) return false;
		} else if(!this.value.equals(other.value)) return false;
		return true;
	}
}
