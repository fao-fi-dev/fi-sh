/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.wordnik.swagger.annotations.ApiModel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 Apr 2015
 */
@XmlRootElement(name="DescriptiveDataModelProperty")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for descriptive Data properties specifications")
public class DescriptiveDataModelProperty extends DataModelProperty {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 55981908328048072L;

	/**
	 * Class constructor
	 *
	 */
	public DescriptiveDataModelProperty() {
		// TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param type
	 * @param description
	 * @param key
	 * @param nullable
	 */
	public DescriptiveDataModelProperty(String name, String type, String description, boolean key, boolean nullable) {
		super(name, type, description, key, nullable); // TODO Auto-generated constructor block
	}
}
