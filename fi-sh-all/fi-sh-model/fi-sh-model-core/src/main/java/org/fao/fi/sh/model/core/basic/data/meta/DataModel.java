/**
 * (c) 2015 FAO / UN (project: fis-model-common)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Apr 2015
 */
@XmlType(name="abstractDataModel")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for Data models")
abstract public class DataModel<META extends DataMetadata, PROPERTY extends DataModelProperty> {
	@ApiModelProperty(position=1, required=true, value="The Data metadata")
	@XmlElement(name="metadata") private META metadata;
	
	@ApiModelProperty(position=2, required=true, value="The Data properties")
	@XmlElementWrapper(name="properties")
	@XmlElement(name="property")
	protected List<PROPERTY> properties;
	
	/**
	 * Class constructor
	 *
	 */
	public DataModel() {
	}
	
	/**
	 * Class constructor
	 *
	 * @param metadata
	 * @param properties
	 */
	public DataModel(META metadata, List<PROPERTY> properties) {
		super();
		this.metadata = metadata;
		this.properties = properties;
	}
	
	/**
	 * @return the 'metadata' value
	 */
	public META getMetadata() {
		return this.metadata;
	}
	
	/**
	 * @param metadata the 'metadata' value to set
	 */
	public void setMetadata(META metadata) {
		this.metadata = metadata;
	}
	
	/**
	 * @return the 'properties' value
	 */
	public List<PROPERTY> getProperties() {
		return this.properties;
	}
	
	/**
	 * @param properties the 'properties' value to set
	 */
	public void setProperties(List<PROPERTY> properties) {
		this.properties = properties;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.metadata == null ) ? 0 : this.metadata.hashCode() );
		result = prime * result + ( ( this.properties == null ) ? 0 : this.properties.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		@SuppressWarnings("rawtypes")
		DataModel other = (DataModel) obj;
		if(this.metadata == null) {
			if(other.metadata != null) return false;
		} else if(!this.metadata.equals(other.metadata)) return false;
		if(this.properties == null) {
			if(other.properties != null) return false;
		} else if(!this.properties.equals(other.properties)) return false;
		return true;
	}
}
