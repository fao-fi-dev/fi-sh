/**
 * (c) 2015 FAO / UN (project: fis-web-common)
 */
package org.fao.fi.sh.model.core.basic.data.meta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Apr 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Apr 2015
 */
@XmlType(name="dataProperty")
@XmlAccessorType(XmlAccessType.FIELD)
@ApiModel(value="The container for Data properties specifications")
abstract public class DataModelProperty implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 7104323810144388097L;
	
	@ApiModelProperty(position=1, value="The property name", required=true)
	@XmlElement private String name;
	
	@ApiModelProperty(position=2, value="The property type", required=true)
	@XmlElement	private String type;
	
	@ApiModelProperty(position=3, value="The property description", required=true)
	@XmlElement private String description;
	
	@ApiModelProperty(position=4, value="Whether this property is a key or not", required=true)
	@XmlAttribute(name="isKey") private boolean isKey;
	
	@ApiModelProperty(position=5, value="Whether this property is nullable or not", required=true)
	@XmlAttribute(name="isNullable") private boolean isNullable;
	
	/**
	 * Class constructor
	 *
	 */
	public DataModelProperty() {
		super(); // TODO Auto-generated constructor block
	}

	/**
	 * Class constructor
	 *
	 * @param name
	 * @param type
	 * @param description
	 * @param key
	 * @param nullable
	 */
	public DataModelProperty(String name, String type, String description, boolean key, boolean nullable) {
		super();
		this.name = name;
		this.type = type;
		this.description = description;
		this.isKey = key;
		this.isNullable = nullable;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the 'type' value
	 */
	public String getType() {
		return this.type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the 'description' value
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description the 'description' value to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the 'nullable' value
	 */
	public boolean isNullable() {
		return this.isNullable;
	}

	/**
	 * @param nullable the 'nullable' value to set
	 */
	public void setIsNullable(boolean isNullable) {
		this.isNullable = isNullable;
	}

	/**
	 * @return the 'isKey' value
	 */
	public boolean isKey() {
		return this.isKey;
	}

	/**
	 * @param isKey the 'isKey' value to set
	 */
	public void setKey(boolean isKey) {
		this.isKey = isKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( this.description == null ) ? 0 : this.description.hashCode() );
		result = prime * result + ( this.isKey ? 1231 : 1237 );
		result = prime * result + ( this.isNullable ? 1231 : 1237 );
		result = prime * result + ( ( this.name == null ) ? 0 : this.name.hashCode() );
		result = prime * result + ( ( this.type == null ) ? 0 : this.type.hashCode() );
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		DataModelProperty other = (DataModelProperty) obj;
		if(this.description == null) {
			if(other.description != null) return false;
		} else if(!this.description.equals(other.description)) return false;
		if(this.isKey != other.isKey) return false;
		if(this.isNullable != other.isNullable) return false;
		if(this.name == null) {
			if(other.name != null) return false;
		} else if(!this.name.equals(other.name)) return false;
		if(this.type == null) {
			if(other.type != null) return false;
		} else if(!this.type.equals(other.type)) return false;
		return true;
	}
}
