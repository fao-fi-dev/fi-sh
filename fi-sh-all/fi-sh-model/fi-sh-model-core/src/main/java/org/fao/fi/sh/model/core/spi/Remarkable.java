/**
 * (c) 2015 FAO / UN (project: fis-model-core)
 */
package org.fao.fi.sh.model.core.spi;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2015
 */
public interface Remarkable {
	Boolean getRemarkable();
	void setRemarkable(Boolean remarkable);
	
	String getRemarks();
	void setRemarks(String remarks);
}
