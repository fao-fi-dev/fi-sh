/**
 * (c) 2015 FAO / UN (project: fi-sh-model-core)
 */
package org.fao.fi.sh.model.core.spi;

import java.util.Date;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 May 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 May 2015
 */
public interface Creatable {
	String getCreatorId();
	void setCreatorId(String creatorId);
	
	Date getCreationDate();
	void setCreationDate(Date creationDate);
}
